window.jQuery = window.$ = require('jquery');
let hide = true;
$(document).ready(function () {
    //handle newsletter signup
    $('form#newsletter-signup :input[type=submit]').on('click', (function (e) {
        e.preventDefault();
        var action = $('form#newsletter-signup').attr('action');
        $.ajax({
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: action,
            data: {// change data to this object
                _token: $('meta[name="csrf-token"]').attr('content'),
                email: $('input[name=subscriber-email]').val(),
                doi: $('input[name=newsletter-doi-check]').val()
            },
            dataType: "text",
            success: function (data) {
                //success
                if (data == '200') {
                    $('.modal').modal('close');
                } else {
                    //dump controller errors out to html of errors content
                    $('form div errors').removeClass('hide');
                }
            }
        });
    }));
});
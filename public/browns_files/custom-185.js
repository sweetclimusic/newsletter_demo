﻿var co_name = 'JoeBrowns';
var def_full_size = '';
var def_samp_size = '';
var pageId = 'WC-';
var buyBtn_type = 'css';  // or 'img'
var postcodeCountries = 'UK,GB,JE,GY,US,CA,CI';  // override by copying to custom.js if needed
var CIcheck = { 'match':'(gy|je)[ ]*([0-9])', 'country':'CI', 'notCI':'UK' }; // override by copying to custom.js if needed
var checkData_immediate = true;  // true = error check immediately e.g. onblur, false = error check only after clicking submit
var checkData_errorPlacement = 'below'; // choices are "popup" or "below" - this dictates position of validation error messages

customSelectDoAll = false; // override of common.js var
customBeforeUnload = custBeforeUnload;
doUnloadPopup = false;  // override

isResponsiveSite = true;
customResize = custResize;
responsiveLimits['designed'] = { max:10000, min:990,   ovr:960 };
responsiveLimits['desktop'] =  { max:959,   min:802,   ovr:990 };
responsiveLimits['pad'] =      { max:801,   min:701,   ovr:801 };
responsiveLimits['phone'] =    { max:700,   min:1,     ovr:320 };
/*
responsiveLimits['designed'] = { max:10000, min:641,  ovr:1030 };
responsiveLimits['desktop'] =  { max:10001, min:10002, ovr:1030 };
responsiveLimits['pad'] =      { max:10003, min:10004, ovr:768 };
responsiveLimits['phone'] =    { max:640,   min:1,     ovr:320 };
*/
//responsiveLimits['designed'] = { max:10000, min:769,  ovr:1030 };
//responsiveLimits['desktop'] =  { max:10001, min:10002, ovr:1030 };
//responsiveLimits['pad'] =      { max:768, min:641, ovr:768 };
//responsiveLimits['phone'] =    { max:640,   min:1,     ovr:320 };

phone_menu_mode = 'expand';  // 'link' = follow onclick link, no mouseover
                           // 'expand' = dont follow link, just expand
                           // 'pad' = expand menu on first click, follow link on second
responsiveWidth = responsiveLimits['designed'].ovr;
responsivePageId = '';    // use this to identify a page to the responsiveStylesheetChanged function

addLog('rscss='+getResponsiveStyle());
addLoadEvent(xxx);
function xxx() {
addLog('rsxxx='+getResponsiveStyle());
}

function canZoom(mode) {
  var res = true;
  var canzoom = 'designed,desktop,pad';
  if (responsiveStyle > '' && !canzoom.match(responsiveStyle)) { res = false; }
  return res;
}

var onlyPostLoadVisibleImages = true;
alertConfig = {
 defaultHeaders : false,
 cssButtons : true,
 buttonExt : 'png',
 buttonAlt : false,
 buttonAlign: 'right',
 buttonWidth: 'auto',
 useIcons : false,
 leftFillHeader : false,
 printButton : false,
 hideHead : true,
 removePadding : false,
 clickCoverToClose : true,
 spacingtop : 20,
 noclose : false,
 noscroll : false,
 nocover : false,
 forcesquarecorners : false,
 closeMsg : '<span class="escCloseMsg">Hit escape to close this message</span>',
 resets : { hideHead : true, removePadding : false, clickCoverToClose : true, noclose : false,
            noscroll : false, nocover : false, forcesquarecorners : false }
}
var miniBasketConfig = { // settings for toggleBasketPopup (if used) values for an image to drop the basket down on ipad etc.
 popupdiv : 'basketPopup',
 minibasketdiv : 'ajaxBasket',
 img : 'miniBasketToggle',
 alink : 'miniBasketMainA',
 altshow : 'show basket',
 althide : 'hide basket',
 srcshow : 'assets/images/mini_basket_down.png',
 srchide : 'assets/images/mini_basket_up.png'
}
// to show num items on basket mobile
//var basketTotalItems;
// -------------------- web site functions
function menuFixup() {
  var to = document.getElementById('menu');
  var tol = document.getElementById('lmenu')
  var tos = document.getElementById('sitemapmenu')
  var top = document.getElementById('phoneMenu')
  var from = document.getElementById('menu_holder')
  var froml = document.getElementById('lmenu_holder')
  try {
  if (to && from) {
    to.innerHTML = from.innerHTML;
  }
  if (tol && (from || froml)) {
    if (froml) {
      tol.innerHTML = froml.innerHTML;
    } else {
      var pm = tol.getAttribute('data-preset_menu');
      if (!pm) { tol.innerHTML = from.innerHTML; }
    }
  }
  if (tos && from) { tos.innerHTML = from.innerHTML;}
  if (top && from) { top.innerHTML = from.innerHTML;}
  if (from) { from.innerHTML = ''; }
  if (froml) { froml.innerHTML = ''; }
//  if (to) {  // replace all spaces in <a> with nbsp to stop wrapping
//    var els = to.getElementsByTagName('A');
//    for (var i=0; i<els.length; i++) {
//      try { els[i].innerHTML = els[i].innerHTML.replace(/ /ig,'&nbsp;'); }
//      catch(err) { els[i].innerHTML = 'THERE IS AN ERROR ON THIS LINE'; }
//    }
//  }
  } catch(err) {}

  // manipulate top menu
  lm = to;
  if (lm) {
    // assign div ids to the popup divs inside each li and assign touch handlers if mobile
    var lia = lm.getElementsByTagName("LI");
    for (var i=0; i < lia.length; i++ ) {
      if (lia[i].id && lia[i].id.indexOf('pos') > -1) {
        topMenus[lia[i].id] = 0;
        var da = lia[i].getElementsByTagName("DIV");
        if (da && da.length > 0) {
          da[0].id = 'div_'+lia[i].id;  // auto assign ids to popup menu div - assumed to be first div in the <li>
        }
        if (is_mob) {  // replace all 'li' mouseover handlers with touchhandlers so menuOpen logic works
          if (lia[i].onmouseover) {
            lia[i].ontouchstart = lia[i].onmouseover;
            lia[i].onmouseover=null;
            lia[i].onmouseout=null;
          }
        }
      }
    }
  }

  // manipulate left menu
  lm = tol;
  if (lm) {
    // assign div ids to the popup divs inside each li and remove mouse handlers
    var lia = lm.getElementsByTagName("LI");
    for (var i=0; i < lia.length; i++ ) {
      if (lia[i].id && lia[i].id.indexOf('pos') > -1) {
        lia[i].id = lia[i].id + '_l';  // make id unique
        topMenus[lia[i].id] = 0;
        var da = lia[i].getElementsByTagName("DIV");
        if (da && da.length > 0) {
          da[0].id = 'div_'+lia[i].id;  // auto assign ids to popup menu div - assumed to be first div in the <li>
        }
        if (is_mob) {
          if (lia[i].onmouseover) {
            lia[i].onmouseover=null;
            lia[i].onmouseout=null;
          }
        }
      }
    }
  }

  // manipulate phone menu
  lm = top;
  if (lm) {
    var lia = lm.getElementsByTagName("LI");
    for (var i=0; i < lia.length; i++ ) {
      if (lia[i].id && lia[i].id.indexOf('pos') > -1) {
        lia[i].id = lia[i].id + '_p';  // make id unique
        topMenus[lia[i].id] = 0;
        var da = lia[i].getElementsByTagName("DIV");
        if (da && da.length > 0) {
          da[0].id = 'div_'+lia[i].id;  // auto assign ids to popup menu div - assumed to be first div in the <li>
        }

        if (phone_menu_mode == 'link' || phone_menu_mode == 'expand') {
          lia[i].onmouseover=null;  // remove mouse handlers
          lia[i].onmouseout=null;
        }
        else if (is_mob) {  // replace all 'li' mouseover handlers with touchhandlers so menuOpen logic works
          if (lia[i].onmouseover) {
            lia[i].ontouchstart = lia[i].onmouseover;
            lia[i].onmouseover=null;
            lia[i].onmouseout=null;
          }
        }
        else { // replace mouseover with click for desktop testing
          if (phone_menu_mode != 'pad') { lia[i].onclick = lia[i].onmouseover; }
          lia[i].onmouseover=null;
          lia[i].onmouseout=null;
        }
      }
    }
  }
  if (window.tidySitemap) { tidySitemap(); }
}

function saveWallp(el) {
  if (typeof(el) == 'string') {
   id = el;
  } else {
    var id = el.getAttribute('data-id');
    id = id.replace('-extra','');
  }
  setXCookie('wallp',id);
}
function setWallp(foo) {
  var wid = '';
  if(!foo){
    if (window.crumbGroup && (crumbGroup != '<elucid_crumbGroup>')) { wid = crumbGroup; }
    if (!wid && thisPageId && thisPageId != '<elucid_pageid_placeholder>') { wid = thisPageId; }
    if (!wid) { wid = getXCookie('wallp'); }
    if (!wid) { wid = pageId; }
  } else {
    wid = foo;
  }
  if (wid) {
    // pass e.g. MC- into setwallp to open mens menu
    pageId = wid.toUpperCase();
    var menu = document.getElementById('menu');
    if (menu) {
      var els = menu.getElementsByTagName('li');
      for (var i=0; i<els.length; i++) {
        var did = els[i].getAttribute('data-id');
        if (did) { did = did.toUpperCase(); }
        if (did == pageId) {
//          if (pageId != 'home') {
//            els[i].style.backgroundImage = 'url('+thisUrl+'assets/images/menu-pointer.jpg)';
//          }
          var aas = els[i].getElementsByTagName('a');
          for (var j=0; j<aas.length; j++) {
            var clas = aas[j].className;
            if (clas == 'toponly') {
              aas[j].className = 'toponlySelected';
            }
          }
        }
      }
    }
    var menu = document.getElementById('phoneMenu');
    if (menu) {
      var els = menu.getElementsByTagName('li');
      for (var i=0; i<els.length; i++) {
        var did = els[i].getAttribute('data-id');
        var didID = els[i].getAttribute('id');
        if (did == pageId) {
          el = document.getElementById('div_' + didID);
          el.style.display = 'block';
          el.menuopen = true;
        }
      }
    }
  }
}

function tidyFunc() {
// if (!pageIsLoaded) { return } // menu wont be ready yet
  // manage left menu - its a copy of the main menu
  // - remove the mouseover handlers
  var lm = document.getElementById('leftMenu');
  if (lm) {
    var lia = lm.getElementsByTagName("LI");
    for (var i=0; i < lia.length; i++ ) {
      lia[i].onmouseover=null;
      lia[i].onmouseout=null;
    }
    // make visible all menus with data-id = pageid or data-id = pageid+'extra'
    // pageid can have a _??? section, the _??? allows for different background but same menu
    // remove the _??? to get the correct menu
    var pid = pageId.toUpperCase();
    //alert(pid);
    if (pid.indexOf('_') > 0) { pid = pid.substr(0,pid.indexOf('_')); }
    var lia = lm.getElementsByTagName("LI");
    for (var i=0; i < lia.length; i++ ) {
      var id = lia[i].getAttribute('data-id');
      if (id) { id = id.toUpperCase(); }
      if (id == pid) { lia[i].style.display = 'block'; }
      if (id == pid+'-extra') { lia[i].style.display = 'block'; }
    }
    // highlight the selected menu item and sub menu item
    var pida = pid.split('||');
    var lia = lm.getElementsByTagName("DIV");
    for (var i=0; i < lia.length; i++ ) {
      var sub = (lia[i].getAttribute('class') || lia[i].getAttribute('className'));
      if (sub == 'sub' || 1==1) {
        var id = lia[i].getAttribute('data-id');
        if (id) { id = id.toUpperCase(); }
        if (id && (pid.match(id) > '')) {
          lia[i].style.display = 'block';
          var el = lia[i].parentNode;
          var anc = el.getElementsByTagName("A");
          for (var j=0; j < anc.length; j++ ) {
            var main = anc[j].getAttribute('data-main');
            var href = anc[j].href.toUpperCase();
            for (var k=0; k < pida.length; k++ ) {
              if (href.match(new RegExp(pida[k],'i')) > '') {
//alert(sub+'---'+href+'---'+pida[k]+'---'+main+'---'+anc[j].className);
                if (main == 'true') {
                  anc[j].setAttribute('className',anc[j].className + ' selected');
                  anc[j].setAttribute('class',anc[j].className + ' selected');
                } else {
                  anc[j].setAttribute('className','subSelected');
                  anc[j].setAttribute('class','subSelected');
                }
              }
            }
          }
        }
      }
    }
  }
}

function resetMenuHeight() {  // used in tabbed info pages
/*
  var lm = document.getElementById('leftMenu');
  if (lm) {
    lm.style.height = 'auto';
    var el = document.getElementById('innerContainer');
    if (el) {
      // make sure images on the page have a specified height, otherwise this will calc the wrong height
      var ht = el.offsetHeight;
      ht = (ht > 100 ? ht - 10 : 100); // -10 to match innerContainer padding
      lm.style.height = ht + 'px';
    }
  }
*/
}

var menuTimer = null;
var menuTId = '';
var menuCaller = null;
var menuOpen = false;
var menuWasOpen = false;
var topMenus = new Array();

function touchDocCustom(event) {
// called by touching anywhere from touchDoc in common.js

  if (menuOpen && (menuTId && !popupInDiv(event, menuTId, true))) {
    if (responsiveStyle == 'phone' && (phone_menu_mode == 'expand' || phone_menu_mode == 'pad') ) {
     // dont let touch somewhere else close an expanding menu
    } else {
      hideMenu(event);
    }
  }
  // menu timer opens the menu before mobClick is called so remember state when onTouch happened
  menuWasOpen = (menuTId && document.getElementById('div_'+menuTId).menuopen) || false;

  if (window.miniBasketConfig) {
    var div = document.getElementById(miniBasketConfig.popupdiv);
    if (div && div.style.display == 'block') { hideBasketPopup(event); }
  }
}
function onMenuTimer() {
  clearTimeout(menuTimer);
  var wofs = isIE6() ? 2 : 8;
  if (menuTId) {
    var el = document.getElementById('div_'+menuTId);
    if (el) {
      if (el.style.display == 'block') { return; }
      el.style.left = '-10000px';
      el.style.zIndex = '20000';
      el.style.display = 'block';
      el.style.overflow = 'visible';
      el.style.height = 'auto';
      var pos = cssStyle(el,'position');
      if (pos == 'absolute') {
        var elr = document.getElementById('menuWidthRef');
        if (elr) { el.style.width = cssStyle(elr,'width');  } // reset for responsive logic
        var pos = el.getAttribute('data-align') || 'BL';
        if (pos == undefined) { pos = 'BL'; } else { pos = pos.toUpperCase(); }
        if (!pos.match(/L|R/i)) { pos += 'L'; }
        if (!pos.match(/B|T/i)) { pos += 'B'; }
        var ol = (el.getAttribute('data-offsetLeft') || 0)-0;
        var ot = (el.getAttribute('data-offsetTop') || 0)-0;
        var mcb = getBounds(menuCaller);
        if (pos.match('L')) { var x = mcb.x-0 + ol; }
        if (pos.match('R')) { var x = mcb.x-0+mcb.w + ol - el.offsetWidth; }
        if (pos.match('B')) { var y = (mcb.y-0 + mcb.h + ot); }
        if (pos.match('T')) { var y = (mcb.y-0 + ot); }

        x = xOnPage(x,el.offsetWidth);
        setZIndex('menu');
        el.trueleft = x;
        el.style.left = x+'px';
        el.style.top = y+'px';
      } else {
        el.trueleft = 0;
        el.style.left = '0px';
        el.style.top = '0px';
      }
      menuOpen = true;
      el.menuopen = true;
//      rolldown(el,null);
      var els = el.getElementsByTagName('span');
      for (var i=0; i<els.length; i++) {
        if (els[i].className = 'menucover') {
          els[i].style.width = menuCaller.offsetWidth - wofs + 'px';
        }
      }
    }
  }
  updateMenuPrompts();
}
function mobClick(caller,event) {
  if (!is_mob) {  // touchhandler set menuWasOpen for mob
//document.title = caller.parentNode.id+' | '+menuWasOpen+' | '+menuTId;
    var tid = caller.parentNode.id;
    menuWasOpen = (tid && document.getElementById('div_'+tid).menuopen) || false;
  }

  if (responsiveStyle == 'phone') {
    if (phone_menu_mode == 'link') {
      return true;
    }
    if (phone_menu_mode == 'expand') {
      expandMenu(caller,event);
      return false;
    }
    if (phone_menu_mode == 'pad') {
      if (menuWasOpen) {
        return true;
      } else {
        if (!is_mob) { expandMenu(caller,event); }  // else touchhandler will open it
        return false;
      }
    }
  } else {
    return menuWasOpen;
  }
  return true;
}

function expandMenu(caller,event) {
  menuCaller = caller.parentNode;
  menuTId = menuCaller.id;
  //alert(menuTId);
//alert(menuTId+' '+menuWasOpen);
  menuWasOpen = (menuTId && document.getElementById('div_'+menuTId).menuopen) || false;
  if (menuWasOpen) {
    var el = document.getElementById('div_'+menuTId);
    if (el) {
      el.style.display = 'none';
      el.menuopen = false;
      menuOpen = false;
    }
  } else {
    onMenuTimer();
  }
  updateMenuPrompts();
}

function updateMenuPrompts() {
  if (!menuTId) { return; }
  // update the background of any <a> with a class of 'expand'
  var el = document.getElementById('div_'+menuTId);
  var els = menuCaller && el ? menuCaller.getElementsByTagName("A") : '';
  var elsParent;
  for (var i=0; i<els.length; i++) {
    if (els[i].className.indexOf('expand') > -1) {
      if (el.menuopen) {
        els[i].style.background = 'url('+thisUrl+'assets/images/minus.png) 100% 8px no-repeat transparent';
		 if(is_mob){
			elsParent = els[i].parentNode;
		 	elsParent.style.background = '#fdfdfd';
		 }
      } else {
        els[i].style.background = 'url(https://1039206484.rsc.cdn77.org/assets/images/plus.png) 100% 8px no-repeat transparent';
		   if(is_mob){
		elsParent = els[i].parentNode;
		elsParent.style.background = '#ffffff';
		   }
      }
    }
  }
}

function delayMenu(caller,event) {
//  menuFixup function now handles ontouchstart initialisation
  if (caller.id == menuTId && menuOpen) { return; }
  clearTimeout(menuTimer);
  if (responsiveStyle != 'phone' || phone_menu_mode != 'pad') { hideMenu(event); }
  menuTId = caller.id;
  menuCaller = caller;
  menuTimer = setTimeout( 'onMenuTimer()', 200 );
}

function hideMenu(ev) {
  clearTimeout(menuTimer);
  if (!ev) var ev = window.event;
	 var t = ev.relatedTarget || ev.toElement;

  for (var menu in topMenus) {
    var el = document.getElementById('div_'+menu);
    if (el && el.menuopen == true) {
      if (menu != menuTId || (menuTId && !popupInDiv(ev, menuTId, true, t))) {
        var pos = cssStyle(el,'position');
        if (pos == 'absolute' || is_mob) {  // dont close non-absolute menus in phone mode
          el.style.display = 'none';
          el.menuopen = false;
          menuOpen = false;
//          if (menu == menuTId) { menuTId = ''; }
        }
      }
    }
  }
//  updateMenuPrompts();
}

function set_site_currency(sel) {
  location = thisUrl + 'changecurrency.asp?SC='+sel.value+'&from='+location.href;
}

function printbigpic() {
  printDiv('zoomImg'+bigPicImgId);
}
var bigPicAlert = null;
var bigPicImg = null;
var bigPicImgId = 0;
var bigPicTitle = '';
var bigPicRootSrc = '';

function addDrag(el) {
  var img = document.getElementById('zoomImg'+bigPicImgId);
  var b = true;
  if (img && window.DragHandler) { var xx = DragHandler.attach(img,img.parentNode,b); }// make it scrollable
}

function bigPicShowAlt(zoompath,altpath) {
  var el = document.getElementById('zoomImg'+bigPicImgId);
  if (el) {
    el.src = zoompath;
  }
}

function bigPicAltImages(src,w,h) {
  var html = '';
  if (window.num_alt > 0) {

    var tmp = '<li style="float:left;">';
    tmp += '<a style="float:left; margin-right:10px;" onclick="bigPicShowAlt(\'[xx_zoompath]\',\'[xx_altpath]\'); return false;" >';
    tmp += '<img style="float:left; border:1px solid #a4a4a4;" src="[xx_altpath]" alt="" onerror="this.style.display = \'none\';" id="thumbz[xx_alt_no]" />';
    tmp += '</a>';
    tmp += '</li>';

    if (bigPicRootSrc) { src = bigPicRootSrc; }

    for (var i=0; i<num_alt-0+1; i++) {
      var tmp1 = tmp;
      tmp1 = tmp1.replace(/\[xx[_ ]zoompath\]/ig, src.replace('alt','zoom').replace('large','zoom')+alt_ext[i]);
      tmp1 = tmp1.replace(/\[xx[_ ]altpath\]/ig, src.replace('zoom','alt').replace('large','alt')+alt_ext[i]);
      tmp1 = tmp1.replace(/\[xx[_ ]alt_no\]/ig, i);
      html += tmp1;
    }
    html = '<ul class="bigPicAlt">'+html+'</ul>';
  }
  return html;
}

function bigPicLoaded() {
  closeAlertX(bigPicAlert);
  alertConfig.clickCoverToClose = true;
  alertConfig.hideHead = bigPicTitle == '';
  alertConfig.removePadding = true;
  alertConfig.forcesquarecorners = true;
//alert('style="float:left; width:'+bigPicImg.width+'px; height='+bigPicImg.height+'px;" ');
  bigPicImgId++;
  alertPostProcess = addDrag;
  var s = '<img id="zoomImg'+bigPicImgId+'" class="bigPic" style="width:'+bigPicImg.width+'px; height:'+bigPicImg.height+'px;" src="'+bigPicImg.src+'" alt="" />';
//  s += bigPicAltImages(bigPicImg.src, bigPicImg.width, bigPicImg.height);
  alertX(s,'',0,bigPicTitle,'',{});
}

function bigPicError() {
  closeAlertX(bigPicAlert);
  alertConfig.clickCoverToClose = true;
  alertX(getTranslatedText('tt_bigPic1','It is not possible to show this image.'),'','','','',{});
}

function getbigPicSrc(img) {
  var el = document.getElementById(img);
  if (el) {
    var src = el.src;
    if (!src) { src = el.style.backgroundImage.replace('url(','').replace(')',''); }
    var src = nonPHPImageName(src);
//       if (im.match('coming_soon') || im.match('blank.gif')) { return false; }  // dont show if its a coming soon image
    var dz = el.getAttribute('data-zoom')
    if (dz) {
      dz = dz.split('->');  // format = largefolder->zoomfolder
      if (dz.length == 2) { src = src.replace(dz[0],dz[1]); }
    }
  } else {
    var src = img;
  }
  return src;
}
function getbigPicTitle(title) {
  var bigPicTitle = (title ? title : '');
  if (title) {
    var el = document.getElementById(title);
    if (el) { bigPicTitle = el.innerHTML; }
  }
  return bigPicTitle;
}

function bigPic(img, title) {

  var src = getbigPicSrc(img);
  var bigPicTitle = getbigPicTitle(title);

  bigPicRootSrc = el.getAttribute('rootsrc');  // original src without alt-image suffix

  closeAlertX(bigPicAlert);
  bigPicAlert = alertX(getTranslatedText('tt_bigPic2','Loading Image. Please wait...'));

  bigPicImg = new Image();
  bigPicImg.onload = bigPicLoaded;
  bigPicImg.onerror = bigPicError;
  bigPicImg.src = src;
//  if (bigPicImg.offsetWidth > 0) { bigPicImg.onload = null; bigPicLoaded(bigPicImg); }

//  alertX('<div style="float:left; xwidth:510px; xheight:'+h+'px; xoverflow:auto;"><img src="'+img+'" alt="" />','','','','',{});
}

function imageView(img, title) {
  var w = screenWidth();
  var h = screenHeight();
  var src = getbigPicSrc(img);
  var title = getbigPicTitle(title);
 	var a = thisUrl+"imageview.asp?what="+encodeURIComponent(src+'.jpg')+'&title='+encodeURIComponent(title);

 	var b = "_blank";
 	var c = "width="+(w-20)+",height="+(h-20)+",resizable,scrollbars=yes"
  var w = window.open(a,b,c);
  try {
    w.focus();
   w.moveTo(0, 0)
  }
  catch(err)
  {}
}

//--------------------------------------
function explode(div, classs, endfunc, fromcust) {
  showModalCover();
  div.style.left = '-2000px';
  explodeStage = 1; // exploding
  var too = { x:div.origleft, y:div.offsetTop, w:div.offsetWidth, h:div.offsetHeight };
  if (fromcust) { var from = fromcust; } else { var from = { el:div }; }
  transition( from, too, { speed:1000, classname:classs, grow:'Y', onend:endfunc, action:'h,w' } );
//  transition( from, too, { speed:700, classname:'bigImage', movediv:'Y', onend:exploded, action:'fi' } );
}
//--------------------------------------
function initSizeChart() {
  var id = 'sizeChartDiv';
  var div = createDynamicPopup(id);
  var sct = document.getElementById('sizechartHost');
  if (sct) { sct = sct.innerHTML; } else { sct = 'Size Chart Not Found'; }

  var s = '';
  s+='<div id="sizeChartHead">Size Chart Help</div>';
  s+='<img id="sizeChartCloseBtn" src="assets/images/headerbtn.jpg"  alt="close" title="close" />';
  s+='<div id="sctable">'+sct+'</div>';
  div.innerHTML = s;
  var el = document.getElementById('sizeChartHead');
  if (el) { dhSize = DragHandler.attach(el,div); } // make it draggable on the header
  return div;
}
function showSizeChart() {
  var div = initSizeChart();
  if (div) {
    popupInit(hideSizeChart,'sizeChartCloseBtn');

    div.style.display = 'block';
    var posd = getBounds(div);
    var x = xOnPage(0,posd.w,true);
    var y = yOnPage(0,posd.h,true);
    div.style.left = x + 'px';
    div.style.top = y + 'px';
//    showShadowBox(div);
  }
}
function hideSizeChart() {
  var el = document.getElementById('sizeChartDiv');
  hideShadowBox();
  if (el) { el.style.display = 'none'; }
}
//--------------------

// ds - smooth scroll to div -  usage
// element = outer = usually, document.body
// to = how far, so, position of div to scroll to
// duration = how long to take for scroll
// example:: var howfar = document.getElementById(partAnchor).offsetTop;
//  scrollToPos(document.body, howfar, 300);
function scrollToPos(element, to, duration, lastTop) {
  if (duration < 0) return;
  var difference = to - element.scrollTop;
  var perTick = difference / duration * 10;
  setTimeout(function() {
    element.scrollTop = element.scrollTop + perTick;
    if (element.scrollTop === to || element.scrollTop == lastTop) return;
    scrollToPos(element, to, duration - 10, element.scrollTop);
  }, 10);
}

//--------------------------------------
function doProductBookmark() {
var url = location.href;
var title = document.title;
if (window.sidebar) // firefox
	{ window.sidebar.addPanel(title, url, ""); }
else if(window.opera && window.print) // opera
 {	var elem = document.createElement('a');
  	elem.setAttribute('href',url);
  	elem.setAttribute('title',title);
  	elem.setAttribute('rel','sidebar');
  	elem.click();
 }
else if(document.all && window.external)// ie
 {	window.external.AddFavorite(url, title); }
else
 { alertX('Bookmark could not be created\r\nPlease use your browsers menu to add a bookmark'); }
}
//--------------------------------------
//
function scok() {
  var res = false;
  var pwel = document.getElementById('sctot');
  if (pwel) {
    var opel = document.getElementById('opswd');
    if (opel) {
      var scpw = opel.value.toUpperCase();
      var pwht = 0;
      for (var i=0; i < scpw.length; i++) {
        pwht += scpw.charCodeAt(i) * (1+Math.pow(i*256,2));
      }
      if (pwht == pwel.value) res = true;
//  alert(pwht+' '+pwel.value);
    }
  }
  if (!res) {
    if (scpw == '') { alertX(getTranslatedText('tt_checkdata_oldpw','Please enter your current password')); }
    else { alertX(getTranslatedText('tt_checkdata_matchpw','The password you entered does not match your current password')); }
  }
  return res;
}
//
function searchOk(el) {
  if (typeof(el) == 'string') { el = document.getElementById(el); }
  var val = el.value;
  var def = el.getAttribute('data-def');
  var dv = getTranslatedText('tt_srch_def_value','Search');

  if ((val == '' || val == dv || val == def)) { return false; }
  if (trim(val).length < 3) { alertX('<br>&nbsp;&nbsp;'+getTranslatedText('tt_srch','Please enter at least 3 characters to search for')+'&nbsp;&nbsp;<br>'); return false; }
  else {
    // dont let the form post. I want all the data in the querystring
    val = escape(trim(val.replace(/:/g,'')));
    val = val.replace(/[`~!@#$^&*()_|+\-=÷¿?;:'",.<>\{\}\[\]\\\/]/gi, '');

    if (delim404 == '*') {
      //SSL
      //var s = thisUrl.replace('https:','http:')+'prodpage.asp?type=search&action=Search&'+el+'='+val;
      var s = thisUrl.replace('https:',def_proto)+'prodpage.asp?type=search&action=Search&'+el+'='+val;
      document.location = s;
    } else {
      //SSL
      // JP - added searchterm so JB can track searches in Google Analytics
      //var s = thisUrl.replace('https:','http:')+'product'+delim404+'t'+delim404+val+'?searchterm='+val;
      var s = thisUrl.replace('https:',def_proto)+'product'+delim404+'t'+delim404+val+'?searchterm='+val;
      document.location = s;
    }
    popupClose('','');  // close any open popups
    typeAheadLive = false;
  }
  return false;
}
function setNumber(num) {
	var n = document.getElementById("r_updateNo");
	n.value = num;
	return true;
}

function makeTable(fmt, s, tclass) {
// fmt is an array of columns defined as ['width','th justify','td justify','head text']
// justify is C, L or R
  var justy = new Array; justy['L']='Left'; justy['C'] = 'center'; justy['R'] = 'right';
//  if (tclass) { tclass = ' class="'+tclass+'"'; } else { tclass='';}
  if (tclass) {} else {tclass='';}

  var dohead = false;  // set to true if text supplied in the fmt array
  var totw = 0;
  var cols = fmt.length;
  for (var i=0; i<cols; i++) {
    var col = fmt[i];
    if (col[3]) { dohead = true; }
    totw += col[0]-0;
  }

  res = '<table cellspacing="0" cellpadding="4" border="0" width="'+totw+'" '+tclass+'>';
  if (dohead) {
    res += '<tr>';
    for (var i=0; i<fmt.length; i++) {
      var col = fmt[i];
      var val = (col[3] ? col[3] : '&nbsp;');
      res += '<th width="'+col[0]+'" align="'+justy[col[1]]+'">'+val+'</th>';
    }
    res += '</tr>';
  }

  var a = s.split('|');
  var col = 0;
  for (var i=0; i<a.length-1; i++) {
    col++;
    if (col == 1) { res += '<tr>'; }
    var val = (a[i] ? a[i] : '&nbsp;');
    res += '<td width="'+fmt[col-1][0]+'" align="'+justy[fmt[col-1][2]]+'">'+val+'</td>';
    if (col == cols) { res += '</tr>'; col = 0; }
  }

  res += '</table>';
  return res;
}

function showHideTabs(div_id) {
  showHide(div_id);
//  showHideJump(div_id);
}
function showHideJump(div_id) {
  location = '#s'+div_id;
}
function showHide(div_id, tid, bid, toggle) {
  tid = tid || 'p';
  bid = bid || 's';
  toggle = toggle || false;
  var wason = false;
  var ids = new Array('a','b','c','d','e','f','g');
  for (var i=0; i<ids.length; i++) {
   	var el = document.getElementById(bid+ids[i]);
    if (el) { el.style.display = 'none'; }
   	el = document.getElementById(tid+ids[i]);
    if (el) { if (ids[i] == div_id) { wason = el.className.indexOf('on') > -1; } el.className = el.className.replace('on','off'); }
  }

	// show the requested div
  if (toggle && wason) {
    // already switched it off
  } else {
   	var el = document.getElementById(bid+div_id);
    if (el) { el.style.display = 'block'; }
   	el = document.getElementById(tid+div_id);
    if (el) {el.className = el.className.replace('off','on'); }
  }

  resetMenuHeight();
}

function displayResizeTip() {
addLog('displayResizeTip pageisloaded='+pageIsLoaded+' responsivestyle='+responsiveStyle+' screenwidth='+screenWidth());
//return;
  if (showResizeTip && pageIsLoaded) {
    var wid = screenWidth();
    var div = createDynamicPopup('resizetip',false,'resizetip');
    if (div) {
      div.style.display = 'block';
      div.innerHTML = wid+' '+(window.innerWidth || 0)+' '+responsiveOverride +' '+ responsiveStyle;
    }
  }
}

var resizeDoTimer = null;
var resizeTimerOn = false;
var resizeLastWid = -1;
var lastResponsiveStyle = '';
function custBeforeUnload() {
  if (doUnloadPopup) {
    alertConfig.hideHead = true;
    alertConfig.noclose = true;
    alertConfig.nocover = true;
    alertX(getTranslatedText('tt_unload','Loading. Please wait...'));
  }
  var goobox = document.getElementById(':1.container');
  if (!goobox) { setXCookie('translateon','N'); } // no bar at the top so switch translate off
}

function getResponsiveStyle() {

  if (window.matchMedia && is_mob) {
    for (var ix in responsiveLimits) {
      var s = '(min-width:'+responsiveLimits[ix].min+'px) and (max-width:'+responsiveLimits[ix].max+'px)';
      var match = window.matchMedia(s);
      if (match.matches) { return ix; }
    }
  } else {
    var wid = screenWidth();
    for (var ix in responsiveLimits) {
      if (wid >= responsiveLimits[ix].min && wid <= responsiveLimits[ix].max) { return ix; }
    }
  }
  return responsiveStyle;
}

var resp_custom_css_load_needed = true;  // use for an initial kick if custom css is in use

function custResize() {
  if (isResponsiveSite) {
    resizeTimerOn = true;
    clearTimeout(resizeDoTimer);

addLog('custResize pageisloaded='+pageIsLoaded+' responsivestyle='+responsiveStyle+' screenwidth='+screenWidth());
    var style = getResponsiveStyle();//styleFromWidth();

//    if (wid != resizeLastWid) {  // dont bother if it hasnt really changed size
    if (window.resp_custom_css && resp_custom_css_load_needed) {
      resp_custom_css_load_needed = false;
      responsiveStyle = '';  // force load custom css even if its the default style
    }
addLog(responsiveStyle+' | '+style);

    if (responsiveStyle != style) {
      if (responsiveOverride == '') { // dont change if overridden
        onResponsiveStylesheetChange(responsiveStyle,style);
        setStylesheet(style,'respcss');
      }
      lastResponsiveStyle = responsiveStyle;
      responsiveStyle = style;
      showSizeSwitches();
      onResponsiveStylesheetChanged(lastResponsiveStyle,style);
    }

addLog('custResize pageisloaded='+pageIsLoaded+' responsivestyle='+responsiveStyle+' screenwidth='+screenWidth());
    var wid = screenWidth();
    if (responsiveOverride == '') { // dont change if overridden
      responsiveWidth = (wid <= responsiveLimits['designed'].ovr ? wid : responsiveLimits['designed'].ovr);
    } else {
      responsiveWidth = responsiveLimits[responsiveOverride].ovr;
    }
    responsiveMultiplier = roundDP((responsiveWidth) / responsiveLimits['designed'].ovr,4);

    displayResizeTip();
    // use a timer to stop too many calls to the onresize functions
    resizeDoTimer = setTimeout( 'onResizeDoTimer()', 300 );
  }
}

function respOvr(style) {
  var rstyle = responsiveLimits[style];
  if (rstyle != undefined || style == 'def') {
    if (style == 'def') {
      var wid = screenWidth();
      responsiveWidth = wid;
      responsiveOverride = '';  // back to default behaviour
      deleteCookie('respovr','/','');
    } else {
      responsiveOverride = style;
      responsiveWidth = responsiveLimits[responsiveOverride].ovr;
      setXCookie('respovr',responsiveOverride,'','/','');
    }
    setStylesheet(responsiveOverride || responsiveStyle,'respcss');

    showSizeSwitches();
    custResize();
  }
}

function getrespovr() {
  var ovr = getXCookie('respovr');
  if (ovr) {
    responsiveOverride = ovr;
    var wid = screenWidth();
    for (style in responsiveLimits) {
      if (wid >= responsiveLimits[style].min && wid <= responsiveLimits[style].max) {
        if (responsiveStyle != style) {
          onResponsiveStylesheetChange(responsiveStyle,style);
          setStylesheet(ovr,'respcss');
          responsiveStyle = style;
          showSizeSwitches();
        }
        break;
      }
    }
  } else {
    custResize();
  }
}

function closeAllSubMenus() {

  for (var menu in topMenus) {
    var el = document.getElementById('div_'+menu);
    if (el && el.menuopen == true) {
      el.style.width = '';
      el.style.display = 'none';
      el.menuopen = false;
      menuOpen = false;
    }
  }
}

// do all js responsive switching logic in this function to keep it all in one place
function responsiveSwitchOk(id) {
  res = true;
  // this logic = always true unless current settings say not ok
  if (isResponsiveSite) {
    switch (id)  {
//      case 'imageTrans' : if (responsiveStyle == 'phone') { res = false; }; break;
      //case 'zoomer' : if (responsiveStyle == 'phone' || responsiveStyle == 'pad' || is_mob) { res = false; }; break;
      case 'zoomer' : if (responsiveStyle == 'phone') { res = false; }; break;
      case 'spRelPopup' : if (responsiveStyle == 'phone' || responsiveStyle == 'pad') { res = false; }; break;
      case 'topMenuFirstClick' : if (responsiveStyle != 'phone') { res = false; }; break;
      case 'popupbasket' : if (is_mob) { res = false; }; break;
      case 'homepage_remote_links' : if (responsiveStyle == 'phone') { res = false; }; break;
      case 'use_data_src' : if (responsiveStyle == 'phone') { res = false; }; break;
      case 'alertXPrint' : if (is_mob) { res = false; }; break;
      //case 'feefo' : if (responsiveStyle == 'phone') { res = false; }; break;
      case 'homescroller' : if (responsiveStyle == 'phone') { res = false; }; break;
    }
  }
  return res;
}

function onResizeDoTimer() {
  clearTimeout(resizeDoTimer);
  resizeTimerOn = false;
  var wid = screenWidth();
  if (wid != resizeLastWid) {  // dont bother if it hasnt really changed size
    resizeLastWid = wid;
//    if (window.pager_exists === true) { showPager(); }  // pp page
    if (responsivePageId == 'prodpagef') { redrawProdpage('resize'); }  // complete pp redraw
    if (window.tidyParts) { tidyParts(); }  // pp page - recalc item heights in case any items wrapped around
    if (window.refreshScrollers) { refreshScrollers(null,true); } // anywhere
    if (window.reloadVideo) { reloadVideo(); }  // videos page

    initSearchBox(); // switch ID of search box on resize / device change

    if (responsivePageId == 'showpartf' && window.showpartResize) { showpartResize('customjs'); }     // in showpartf.js
    applyZoom('');
  }
}

function onResponsiveStylesheetChange(from,too) {
  // before stylesheet change
  if (from == 'phone' || too == 'phone') {
    // make sure all sub menus are closed
    closeAllSubMenus();
    closeAllResponsiveMenus(from,'init');
  }

//  if (animate) { animate = (too != 'phone'); }
}

function responsiveKick() {
  // call this from any page to run any javascript responsive changes
  onResponsiveStylesheetChanged(lastResponsiveStyle,responsiveStyle);
}

function onResponsiveStylesheetChanged(from,too) {
  // after stylesheet change

  // this will be called whenever a page width changes responsive size
  // OR if a page calls the responsiveKick function ('from' will be '')
  //
  // I have tried to keep all responsive change code here so it is not spread around the site

  // set responsivePageId = 'something' to establish a page id

  if (window.prodpageRespChange) { prodpageRespChange(from,too,'customjs'); }

  if (responsivePageId == 'showpartf' && window.showpartRespChange) {
    showpartRespChange(from, too, 'customjs');     // in showpartf.js
  }

  if (window.basketRespChange) { basketRespChange(from, too, 'customjs'); }

}

function closeAllResponsiveMenus(respStyle,init) {
  var el = document.getElementById(respStyle+'Menu');
  if (el) {
    var els = el.getElementsByTagName('A');
    for (var i=0; i<els.length; i++) {
      switchResponsiveMenu(respStyle, els[i], init || 'close');
    }
    closeAllSubMenus();
  }
}

function switchResponsiveMenu(respStyle,anc,mode) {
// mode = '' = toggle, mode = 'close' = close; anc is the <a> link that called this
  var div = anc.getAttribute('data-div');  // data div is the div to hide/show
  var el = document.getElementById(div);
  if (el) {
    var res = 'block';
    if (mode == 'close' || (mode == '' && el.style.display == 'block')) { res = 'none'; }
    if (mode == '') { closeAllResponsiveMenus(respStyle); }
    if (mode == 'init') { res = ''; }
    if (cssStyle(div,'position') == 'absolute') {
      var b = getBounds(anc);
      el.style.top = (b.y+anc.offsetHeight) + 'px';
    }
    el.style.display = res;
    setZIndex(el);
    anc.innerHTML = ( res == 'block' ? anc.getAttribute('data-open') : anc.getAttribute('data-closed') );
  }
}

function activeResponsiveStyle() {
  return responsiveOverride || responsiveStyle;
}
function respMoveContent(idToMove, respIds) {
// function = move the contents of 'idToMove' into the correct respId
// eg.
// <div id="respSrc_buybutton">
// </div>
// <div id="respMove_buybutton_desktopbuy">
// </div>
// <div id="respMove_buybutton_phonebuy">
// </div>
// usage: respMoveContent('respSrc_buybutton', { 'default':'respMove_buybutton_dektopbuy', 'phone':'respMove_buybutton_phonebuy' });

  var id = undefined;
  if (is_mob) { id = respIds['mob' + (activeResponsiveStyle())]; }
  if (id == undefined) { id = respIds[activeResponsiveStyle()]; }
  if ((id == undefined) && is_mob) { id = respIds['mobdefault']; }
  if (id == undefined) { id = respIds['default']; }

  return moveContent(idToMove, id, true);
}

function moveContent(idToMove, idTo, clear) {
  var content = document.getElementById(idToMove);
  if (!content) { return; }  // probably not ready yet
  var src = content.parentNode;
  var dest = document.getElementById(idTo);

//alert('respMove content='+content.id+' src='+src.id+' dest='+dest.id);

  if (src != dest && src && dest) {
    var tmp = src.removeChild(content);
    if (clear) {
      src.innerHTML = '<div></div>';
      dest.innerHTML = '';
    }
    dest.appendChild(tmp);
  }
  return (dest ? dest.id : '');
}

function showSizeSwitches() {
  // force the display of override switches according to this size - not css setting for this size
  var el = document.getElementById('responsiveOverride');
  if (el) {
    var disp = 'none';

    var style = responsiveOverride || responsiveStyle;
//document.title = responsiveStyle+' | '+responsiveOverride;
    var nodes = el.childNodes;
    for (var j = 0; j < nodes.length; j++) {
      var c = nodes[j].onclick+' ';
      if (c.match('respOvr')) { // its a size control link
        var s = nodes[j].getAttribute('data-oksizes');  // list of sizes that this link is visible for
        var d = 'none';

//alert(responsiveStyle+' | '+responsiveOverride+' | '+style+' | '+c.match(style));//+' | '+s);
        if (s == 'all' || (s && s.match(responsiveStyle))) {
          if (c.match('def')) {
            if (responsiveOverride) { d = 'block'; }
          } else {
            if (!c.match(style)) { d = 'block'; }
          }
        }
        nodes[j].style.display = d;
        if (d == 'block') { disp = 'block'; }
      }
    }
    el.style.display = disp;  // display of div containing switches
  }
}

function newsPopup() {
  if (responsiveStyle == 'phone') { return; }
  if (cookiesOn && window.showNewsPrompt != undefined) {
    var nlp_done = getXCookie('nlpdone') || '';
    if (nlp_done != 'Y') {
      var nlp_count = getXCookie('nlpcount') || 0;
      if (location.href.match(/https/ig)) {  // secure page = assume they are logging in or registering
        nlp_done = 'Y';
      }

      if (nlp_done != 'Y') {
        nlp_count++;

        var nv = getXCookie('vcount') || 0;
        if (nv <= 1) {  // first visit
          if (nlp_count > 4) {
            nlp_done = 'Y';
            showNewsPrompt();
          }
        } else {
          nlp_done = 'Y';
        }
      }
    }
  }
  if (nlp_done == 'Y') {
    setXCookie('nlpdone',nlp_done,730,'/','');
  } else {
    setXCookie('nlpcount',nlp_count);
  }
}

var newsAlertX = null;
function submitNewsPromptCallback(state, status, res) {
  if (state == 4) {
    // switch thanks div to visible
    var el = document.getElementById('nlp1');
    if (el) { el.style.display = 'none'; }
    var el = document.getElementById('nlp2');
    if (el) { el.style.display = 'block'; }
  }
}
function submitNewsPrompt(id) {
  if (!pageIsLoaded) return false;
  var ele = document.getElementById(id);
  if (checkID(ele)) {
    var url = thisUrl + 'request.asp?submit=newsletter&ajax=Y&email='+ele.value;
    callAjax(url, submitNewsPromptCallback);
    var el = document.getElementById('nlpMessage1');  // show 'saving...' message
    if (el) { el.style.display = 'block'; }
  }
  return false;
}
function showNewsPromptFunc(res) {
  if (res == 'submit') {
    return false;
  }
}
function showNewsPromptCallback(state, status, res) {
  if (state == 4) {
    alertX(res,showNewsPromptFunc,0,'','',{});
  }
}
function showNewsPrompt() {
  loadFileAjax('newspopup', showNewsPromptCallback);
}

if (window.svrTZO == undefined) { var svrTZO = 0; }
var clientTZO = new Date().getTimezoneOffset();

function showCountdown(cd) {
// eg. call
// var countdown1 = { div_id:'timer', end_time:'January 31 2013 16:55', end_msg:'Offer Ended' }
// showCountdown(countdown1);
// eg. styles
//.countdown_holder { position:absolute; top:600px; left:800px; padding:0 0px 5px 0; background:yellow; border:10px ridge red; }
//.countdown_digits { float:left; clear:left; width:100%; }
//.countdown_digit { float:left; width:50px; font-size:20px; text-align:center;
//  color:red; border:1px solid #ccc; margin:10px 10px 0 10px;}
//.countdown_texts { float:left; clear:left; width:100%;}
//.countdown_text { float:left; width:50px; font-size:10px; text-align:center; color:#999; margin:0 11px; }
//.countdown_ended { float:left; width:100%; font-size:20px; text-align:center; padding:20px 0; color:red;}

  var el = document.getElementById(cd.div_id);
//alert(el);
  if (el) {
    if (cd.end_time != undefined) { el.end_time = cd.end_time; }
    if (cd.end_msg != undefined) { el.end_msg = cd.end_msg; }
    if (cd.classroot != undefined) { el.classroot = cd.classroot; }
    if (el.classroot == undefined) { el.classroot = 'countdown'; }

    if (el.className != el.classroot + '_holder') { el.className = el.classroot + '_holder'; }

    var now = new Date();
    if (el.end_time && el.end_time.indexOf(':') == -1) { el.end_time += ' 23:59:59'; }
    var then = new Date(el.end_time);
    var offset = clientTZO - svrTZO; //now.getTimezoneOffset(); //mins
    var diff=then-now-(offset*60000);
    if (diff < 0) {
      s = '<div class="'+el.classroot+'_ended">'+el.end_msg+'</div>';
      el.innerHTML = s;
      clearInterval(el.timer);
    } else {
      var ms, sec, min, hr, days, s;
      diff=(diff-(ms=diff%1000))/1000;
      diff=(diff-(sec=diff%60))/60;
      diff=(diff-(min=diff%60))/60;
      days=(diff-(hr=diff%24))/24;

      text = getTranslatedText('tt_timer','days,hours,minutes,seconds').split(',');

      s = '<div class="'+el.classroot+'_digits">';
      s+= '<div class="'+el.classroot+'_digit">'+days+'</div>';
      s+= '<div class="'+el.classroot+'_digit">'+hr+'</div>';
      s+= '<div class="'+el.classroot+'_digit">'+min+'</div>';
      s+= '<div class="'+el.classroot+'_digit">'+sec+'</div>';
      s+= '</div>';
      s+= '<div class="'+el.classroot+'_texts">';
      s+= '<div class="'+el.classroot+'_text">'+text[0]+'</div>';
      s+= '<div class="'+el.classroot+'_text">'+text[1]+'</div>';
      s+= '<div class="'+el.classroot+'_text">'+text[2]+'</div>';
      s+= '<div class="'+el.classroot+'_text">'+text[3]+'</div>';
      s+= '</div>';

      el.innerHTML = s;
      if (el.timer == undefined) {
        el.timer = setInterval('showCountdown( { div_id:\''+cd.div_id+'\' } )',1000);
      }
    }
  }
}

function customMiniBasketFunc() {
  // used by mini basket to do any client custom bits
  // el will be the element with id of basketPopupFlag which may or may not be useful
//  var s = el.innerHTML;
//  s = s.replace('\[xx_country_code\]',country_code);

}

var translateOn = false;
var translateC = 0;
var translateLoaded = false;
function doTranslateLoaded() {
  if (window.google && window.google.translate && window.google.translate.TranslateElement && google.translate.TranslateElement.InlineLayout ) {
    new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.VERTICAL}, 'google_translate_element');
    var el = document.getElementById('translateLoading');
    if (el) { el.style.display = 'none'; }
    document.getElementById('google_translate_element').style.display = 'block';
   } else if (translateC < 100) {
      translateC++;
      setTimeout(doTranslateLoaded,100);
   }
}

function enableTranslate(mode) {
  if (translateOn) {
    deleteCookie('translateon');
    translateOn = false;
    var el = document.getElementById('translatePrompt');
    if (el) { el.style.display = 'block'; }
    document.getElementById('google_translate_element').style.display = 'none';
    return;
  }
  if (!translateLoaded) {
    translateOn = true;
    translateLoaded = true;
    var el = document.getElementById('translateLoading');
    if (el) { el.style.display = 'block'; }
    var el = document.getElementById('translatePrompt');
    if (el) { el.style.display = 'none'; }
    addScript('https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit',doTranslateLoaded);
  } else {
    doTranslateLoaded();
  }
  setXCookie('translateon','Y');
}
if (getXCookie('translateon') == 'Y') {
  addLoadEvent(enableTranslate,20);
}

function deliveryPopup() {
  helpPopup('include_delivery',0,0,getTranslatedText('tt_deliv_info','Delivery Information'));
}

function checkDeliveryPopup() {
  try { // can except in a window.open popup
    var s = window.location.href.toLowerCase();
    if (s.indexOf('showdelivery=') > -1 && s.indexOf('delivery'+delim404+'1') == -1) {    deliveryPopup();   }
  } catch(err) {}
}

function validateDMY(num) {
  // validate a three part date
  var res = { ok:true, err:'' };
  var eld = document.getElementById('dobD'+num);
  var elm = document.getElementById('dobM'+num);
  var ely = document.getElementById('dobY'+num);
  if (!eld || !elm || !ely) { return res; }
  var d = eld.value-0;
  var m = elm.value-0;
  var y = ely.value-0;
  // dob and dobtxt are optional hidden fields which will contain yyyy-mm-dd and dd-mon-yyyy if they exist
  var dv = document.getElementById('dob'+num);
  if (dv) { dv.value = ''; }
  var dvt = document.getElementById('dobtxt'+num);
  if (dvt) { dvt.value = ''; }
  if (d && m && y) {
//    var months = ['','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var monthss = ',Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec';
    var months = months; // dont translate cos it might be used in the db --- getTranslatedText('tt_short_months',monthss);
    try { months = months.split(','); } catch (err) { months = monthss.split(','); }
    var days = ['',31,29,31,30,31,30,31,31,30,31,30,31];
    var leap = (y % 4 == 0) && ( !(y % 100 == 0) || (y % 400 == 0));
    if (d > days[m] || (m == 2 && d == 29 && !leap)) {
      res.err = 'bad day'
      res.ok = false;
    }
    if (dv) { // yyyy-mm-dd
      dv.value = y+'-'+(m <= 9 ? '0'+m : m)+'-'+(d <= 9 ? '0'+d : d);
    }
    if (dvt) {  // dd-jan-yyyy for cheetahmail
      dvt.value = (d <= 9 ? '0'+d : d)+'-'+(months[m])+'-'+y;
    }
  } else
  if ( d || m || y) {
    res.err = 'part date';
    res.ok = false;
  }
  return res;
}

function prepPosterPage(countdown1) {
  showCountdown(countdown1);

  // process all prices
  var rate = window.currency_mult ? currency_mult : 1;
  var lm = document.getElementById('centerContainer');
  var lia = lm.getElementsByTagName("span");
  for (var i=0; i < lia.length; i++ ) {
    if (lia[i].className.indexOf('price') > -1) {
      lia[i].innerHTML = lia[i].innerHTML.replace(/\[([0-9.]*)\]/ig,function(x,y) { return window.currency_symbol+roundDP((y-0)* rate,2) } )
    }
  }

  applyZoom('centerContainer');
}


function applyZoom(div) {
  div = div || 'outerContainer';
  var lm = document.getElementById(div);
  if (lm) {
    var lia = lm.getElementsByTagName("DIV");
    for (var i=0; i < lia.length; i++ ) {
      if (lia[i].className.indexOf('applyzoom') > -1) {
        lia[i].style.zoom = (100*responsiveMultiplier)+'%';
      }
    }
  }
}

/* 
var snowC = 0;
function dosnow() {
return;
  if (snowC == 0) { addScript(thisUrl+'assets/js/snowflakes'+(window.linkver ? linkver : '')+'.js'); }
  if (window.initsnow) {
    initsnow();
  } else if (snowC < 10) {
    snowC++;
    setTimeout(dosnow,200);
  }
}
*/

// the following fixes <br> showing in address textareas in saved addresses
function br2nl(str) {
  return str.replace(/<br\s*\/?>/mg,"\n");;
}

function stripBR(iNum) {
  var el = document.getElementById('address'+iNum);
  if(el)el.value = br2nl(el.value);
}

// set number of items shown next to basket on mobile DSDS
function updateMobileBasketTotal(howMany) {
  // set to allow number to instantly increase by
 
  if(!howMany){ var howMany = 0; }
  // set number of items total in basket for mobile
  var elb = document.getElementById('basket_itemcount_mobile');
  var elm = document.getElementById('miniBasketMainA');
  if((elb) && (window.vouc_data_qty) && (window.vouc_data_qty>0)){
    var bsktTotal = parseInt(window.vouc_data_qty)+parseInt(howMany);
    elb.innerHTML = bsktTotal;
	elb.classList.add('itemsInBasket');  
  } else if((elm)&&(!window.vouc_data_qty)&&(howMany==1)) {
    // after first part added to basket
    elb.innerHTML = '1';
	elb.classList.add('itemsInBasket');  
  } else {
    // if viewing basket, remove count
    if((elb)&&(elm)){ elb.innerHTML = '0';   elb.classList.remove('itemsInBasket');   }
  }
}

// reset feefo table max-height on homepage to match when all hp box is loaded
// this function is called at the end of getJBBlogCallback(); in home1.txt
function feefoHeight(){
  var sm = document.getElementById('sm_reviews_hp');
  var feefoDiv = document.getElementById('feeforows_hp');
  if((sm)&&(feefoDiv)){
    var outerHeight = sm.offsetHeight;
    var newHeight = outerHeight - 481;
    feefoDiv.style.maxHeight = newHeight+'px';
  }
}

// switch on intellisense (search drop down) by calling initSearchBox()
var searchBoxId = 'searchwhat';  // the id of the search input box - now defined in initSearchBox() and ID switched based on device / screen size
addLoadEvent(initSearchBox,500);
//addLoadEvent(doCustomSelects,1); // get it done as early as possible
//addLoadEvent(doCustomSelects,4000);  // do it again incase new selectors have been created
addLoadEvent(doCustomCheckBoxAndRadio,1); // get it done as early as possible
//addLoadEvent(doCustomCheckBoxes,4000);  // do it again incase new selectors have been created
//preLoadImages += 'assets/images/cselDD_swatch.png,assets/images/errbox3.png,assets/images/search-dd-back.png,assets/images/menu-dd-back.png,';
preLoadImages += 'assets/images/errbox3.png,';
//stopRightClick();
//addLoadEvent(templateSwitch);
addLoadEvent(preLoad);
getrespovr();
addLoadEvent(showSizeSwitches)
//setFont();
//addLoadEvent(setHeight);
//addLoadEvent(newsPopup,6000);
//addLoadEvent(showNewsPrompt);
//addLoadEvent(checkDeliveryPopup,1);
addLoadEvent(displayResizeTip,5000);
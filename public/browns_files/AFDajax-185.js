// darren notes:
// counting results on number of select options - if only one, skip user click and show
// hidden "field names label" on phone for better UX and layout
// used placeholder as well as current method of prepopulating field
// to ensure value there if blur on phone
// clear fields if "enter address manually" clicked
// added onclick to reg to show address fields otherwise
// backend finds empty fields
// JP - added new function - country field value on change
//-------------------------------------------------------------------------
//  AJAX stuff
//-------------------------------------------------------------------------
var AFD_http = '';
var iNum = '';
var AFD_staticDivId = '';  // optionally pass staticDivId to AFD_select_address to make it appear in the div rather than popup
var AFD_alertX_id = null;
var AFD_use_alertX = true;

function AFD_keypress(event, i, staticDivId) {
  event = event || window.event;
  if (event.keyCode == 13) {
    iNum = i;
    AFD_select_address(i, staticDivId);
    return false;
  }
  return true;
}

function AFD_select_address(i, staticDivId) {
  AFD_http = AFD_getHTTP();
  iNum = i;
  AFD_staticDivId = staticDivId;
  clearField('house'+iNum); clearField('postcodefind'+iNum);
// new code uses postcodefind as the field entered and postcode as the address field
// coded to handle either way.
// var pc = document.getElementById("postcode"+iNum).value;
  pcE = document.getElementById("postcodefind"+iNum);
  if (pcE != null)
    { var pc = document.getElementById("postcodefind"+iNum).value; }
  else
    { var pc = document.getElementById("postcode"+iNum).value; }

  if (trim(pc) == '' || pc.match(/postcode/i)) { return false; }
  var el = document.getElementById("house"+iNum);
  var hs = ( el ? el.value : '');
  if (hs == 'House no/Name') { hs = ''; }
  hs = trim(hs);
  hs = hs.replace(/'/g, "");
  //alert(pc+hs);
  var iURL = thisUrl+"AFDajax_JB.asp?postcode="+pc+"&house="+hs+"&num="+iNum
//alert(iURL);
  var fixer = new Date();
  var iURL = iURL+"&when="+fixer.getTime();
  AFD_http.open("GET", iURL, true);
  AFD_pauseComp(200);
  AFD_http.onreadystatechange = function(){ AFD_show_selector(iNum); };
  AFD_http.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2005 00:00:00 GMT");
  AFD_http.send(null);
}

function AFD_showAddressFields(iNum) {
  showDiv('addressTable'+iNum);
  hideDiv('showAddress'+iNum);
  hideDiv('AFDAjaxTable'+iNum);
  hideDiv('addFind');
}


// JP - check postcode for countries without postcode system
function AFD_checkPostcode(i,val) {
  var el = val.value;
  var ele = document.getElementById('postcode'+i);

  if (el && ele) {
    // Country in the list, set PC to N/A
    if (el == "IR" || el == "AE" || el == "SA" || el == "ZA" || el == "HK" || el == "QA" ) {
      ele.value = "N/A"
      ele.readOnly = true;
      ele.style.opacity = "0.6";
    // Country not in the list, but postcode previously set to N/A. Reset postcode
    } else if ( el != "IR" && ele.value == "N/A" || el != "AE" && ele.value == "N/A"  || el != "SA" && ele.value == "N/A"  || el != "ZA" && ele.value == "N/A"  || el != "HK" && ele.value == "N/A"  || el != "QA" && ele.value == "N/A") {
      ele.value = "";
      ele.readOnly = false;
      ele.style.opacity = "1";
    // Country not in the list and postcode is blank
    } else if (el != "IR" && ele.value != "" || el != "AE" && ele.value != ""  || el != "SA" && ele.value != ""  || el != "ZA" && ele.value != ""  || el != "HK" && ele.value != ""  || el != "QA" && ele.value != "" ) {
      ele.readOnly = false;
      ele.style.opacity = "1";
    }
  }
}

function AFD_show_selector(iNum) {

  showRes = false;
  var el = (AFD_staticDivId) ? document.getElementById(AFD_staticDivId+iNum) : null;
  var pc = document.getElementById('postcodefind'+iNum);

  if (pc && pc.value) {
      var el_p = document.getElementById('postcode'+iNum);

      // Only set postcode if country supports it
      if (el_p && el_p.value != "N/A") {
        document.getElementById('postcode'+iNum).value = pc.value;
      }
  }

  if (AFD_http.readyState == 1) {
    if (el) {
      el.innerHTML = 'Looking up addresses...';
      el.style.display = 'block';
    } else {
      AFD_alertX_id = alertX(res,'',0,'Address Selector','',{});
    }
  }

  if (AFD_http.readyState == 4) {
    res = AFD_http.responseText;

    var resa = res.split('#####'); // count#####selector
    //alert(resa[0]);
    if (resa[0]-0 == 0) {
      var hse = document.getElementById('house'+iNum);
      if (hse && hse.value) {  // house was entered and nothing found - auto try again with no house
        hse.value = '';
        AFD_select_address(iNum, AFD_staticDivId);
        return;
      }
      AFD_showAddressFields(iNum);
    }

    if (el) {
      el.innerHTML = resa[1];
      el.style.display = 'block';
    } else {
      closeAlertX(AFD_alertX_id,'');
      AFD_alertX_id = alertX(res,'',0,'Address Selector','',{});
    }

    var sel = document.getElementById('AFD_address');
    if (sel) {
       sel.selectedIndex = 0;
       if (resa[0]-0 == 1) {  // only one address -- auto select it
         // DSDS copy last select option value to first select option value
         // so when only one result, first selection option value is same as last select option value
         sel.options[sel.options.length - 2].value = sel.options[sel.options.length - 1].value;
         AFD_showAddress(iNum); return;
       }

      if (is_mob) {
        sel.onclick = null;
        var att = sel.getAttribute('data-onchange');
        sel.onchange= new Function(att);
        sel.size = 1;
      } else {
        sel.onchange = null;
        var att = sel.getAttribute('data-onclick');
        sel.onclick= new Function(att);
        sel.size = 8;
        if (sel.size > resa[0]-0) { sel.size = resa[0]-0; }
      }

      showDiv('AFDAjaxTable');

      sel.focus();
    }

  }
}

function AFD_getHTTP() {
  var xhttp;
   try {   // The following "try" blocks get the XMLHTTP object for various browsers�
      xhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
      try {
        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (e2) {
 // This block handles Mozilla/Firefox browsers...
     try {
       xhttp = new XMLHttpRequest();
     } catch (e3) {
       xhttp = false;
     }
      }
    }
  return xhttp; // Return the XMLHTTP object
}

function AFD_getAddressKey(event,iNum) {
  if (event) {
    if (event.keyCode == 13) { AFD_showAddress(iNum); }
    if (event.keyCode == 27) { AFD_CloseDiv(); }
  }
}

function AFD_set(field, value) {
  var el = document.getElementById(field);
  if (el) { el.value = value; }
}

function AFD_showAddress(i) {
   if (i == undefined) { i = iNum; }
   var a = '';
   var el = document.getElementById("AFD_address");
   if (el) { a = el.value; }
   if (a != '')
   {
     var x = /\t/;
     a = a.replace(x,'');
     a = trim(a);
     a = a.replace(/, /g,',');
     var b = a.split(",");
     if (b.length > 2) { var c = b[b.length-2]; } else { var c = ''; }
     if (b.length > 1) { var d = b[b.length-1]; } else { var d = ''; }
     var idx = c.indexOf('Not Found');
     if (idx>0) c = '';
     b.length = b.length-2;
     a = b.join('\n');

     // set selected country to be UK as they are using address finder
     // check to see if UK or GB to be used
     var elc = document.getElementById('country'+i);
     var countryVal;
     if (elc){
       elc.selectedIndex = -1
       elc.value = 'UK';
       if (elc.selectedIndex == -1 )  {
         elc.value = 'GB';
       }
     }

     AFD_set("address"+i,a);
     AFD_set("street"+i,a);
     AFD_set("city"+i,c);
     AFD_set("town"+i,c);
     AFD_set("county"+i,'');
     AFD_set("postcode"+i,d);
     AFD_set("postcodefind"+i,d);
     var elp = document.getElementById('postcode'+i);
     if(elp){
       elp.readOnly = false;
       elp.style.opacity = "1";
     }

   }
   AFD_CloseDiv();
   document.getElementById('addressTable'+i).style.display = 'block';
   document.getElementById('showAddress'+i).style.display = 'none';
//   var county = document.getElementById('county'+i);
//   if(county)county.value='';
}

function AFD_pauseComp(millis) {
   var date = new Date();
   var curDate = null;
   do { curDate = new Date(); }
   while(curDate-date < millis);
}

function AFD_CloseDiv()	{
  var el = (AFD_staticDivId) ? document.getElementById(AFD_staticDivId+iNum) : null;
  if (el) {
    el.innerHTML = '';
    el.style.display = 'none';
  } else
  if (AFD_use_alertX) {
    closeAlertX(AFD_alertX_id,'');
  } else {
   if (document.getElementById("addrList"+iNum) != null) { document.getElementById("addrList"+iNum).style.display='none'; }
   if (document.getElementById("addrSel"+iNum) != null) { document.getElementById("addrSel"+iNum).style.display='none'; }
   el = document.getElementById('AFD_div');
   if (el) { el.style.display='none'; }
   hideShadowBox();
   showSelects();
  }
}

function AFD_findStore(event) {
  if (!AFD_http) { AFD_http = AFD_getHTTP(); }
  var pc = document.getElementById("postcode").value;

  var fixer = new Date();
  var iURL = thisUrl+"AFDajax.asp?action=nearest&from="+pc+"&ajax=y&when="+fixer.getTime();
//alert(iURL);
  AFD_http.open("GET", iURL, true);
  AFD_pauseComp(200);
  AFD_http.onreadystatechange = function(){ AFD_show_store(); };
  AFD_http.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2005 00:00:00 GMT");
  AFD_http.send(null);
}

function AFD_show_store() {
  showRes = false;
//alert(AFD_http.readyState);
  if (AFD_http.readyState == 1) {
    res = '<tr><td>Waiting for response................</td></tr>';
    showRes = true;
  }

  if (AFD_http.readyState == 4) {
//alert(AFD_http.responseText);
    res = AFD_http.responseText;
    showRes = true;
  }

  if (showRes) {
    var el = document.getElementById("storeFinderMid");
    if (el) { el.innerHTML = res; }
    alert(res);
  }
}
//-------------------------------------------------------------------------

function AFD_findStock(event) {
  if (!AFD_http) { AFD_http = AFD_getHTTP(); }
  var part = trim(document.getElementById("part").value);
  if ( (part == '') || (mx_stock[part] != undefined)) { return false; }

  var fixer = new Date();
  // sitecodes should be provided by asp and in store_finder.tmp as a js var
  part = part.replace("'",'');
  var iURL = thisUrl+"showPart_ajax.asp?part="+part+"&sitecodes="+sitecodes+"&template_suffix=_stock&when="+fixer.getTime();
//alert(iURL);
  AFD_http.open("GET", iURL, true);
  AFD_pauseComp(200);
  AFD_http.onreadystatechange = function(){ AFD_show_stock(); };
  AFD_http.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2005 00:00:00 GMT");
  AFD_http.send(null);
}

function AFD_show_stock() {
  showRes = false;

  if (AFD_http.readyState == 1) {
    var el = document.getElementById("stockScan");
    if (el) { el.style.display = 'block'; }
  }

  if (AFD_http.readyState == 4) {
    res = AFD_http.responseText;
    showRes = true;
    var el = document.getElementById("stockScan");
    if (el) { el.style.display = 'none'; }
  }

  if (showRes) {
    var el = document.getElementById("stockFinderStock");
    if (el) {
//alert(res);
      res = res.split('#####');

      if (res[0].indexOf('Error:') == 0) {
        alertX(res[1],'',300,'Please Note');
        return;
      }

      // stock/matrix vars and functions are in ecom.js
      var elt = document.getElementById("partBox"+stockBoxCount);
      stockBoxCount++;
      var div = document.createElement('div');
      div.setAttribute('id', 'partBox'+stockBoxCount);
      el.insertBefore(div,elt);

      div.innerHTML = res[0];
      if (res.length > 1) {
        var idx = trim(res[1]);
        eval(res[2]);
//alertX_BR(res[2]);  // to show data returned - stock array is generated by showpart_ajax.asp
        var matrix = (mx_data[idx] && mx_data[idx].feat && (mx_data[idx].feat.length > 0));

        if (matrix) {
          matrix_tip_override = 'click to show stock for this item'

          var func = null;
          switch(mx_data[idx].template)
          {
            case 'MTX_LARGE' : func = createMatrixTable; break;
            case 'DDL_LARGE' : func = createMatrixTable; break;
            case 'LIST' : func = createMatrixTable; break;
            case 'MTX_LARGE_TABLE' : func = createMatrixTable; break;
            case 'MTX_LARGE_ALPHA' : func = createMatrixAlphabet; break;
            case 'MTX_LARGE_BRA' : func = createMatrixBra; break;
          }
          func(idx,'matrixHead','matrixBody', '', 'Click a colour / size combination to see our available stock','STOCK');
//          createMatrixNormal(idx,'matrixHead','matrixBody','', 'Click a colour / size combination to see our available stock','STOCK');
        }

        createShopList(stockBoxCount, idx, matrix);
      }
    }
  }
}
// constants for scroller modes
var as_horizontal='h', as_vertical='v', as_loop='l', as_bounce='b', as_once='o', as_rewind='r';
var ipadscroll = true;  // true = move with finger rather than scrollbar behaviour
var iosscroll = true;  // true = allow native gesture scrolling
// IF IOSSCROLL IS TO BE USED --- DONT FORGET TO SET
//  -webkit-overflow-scrolling: touch;
// in the device css for the scrolling div eg hp_scroll class in phone.css
// ioscroll WILL HIDE hide the scrollbar

if (is_mob) {
  var regex=new RegExp('.*CPU [iPhone ]*OS ([0-9]+)_.*','i');
  var res=regex.exec(navigator.userAgent);
  if (res && res.length > 1) {
    if (res[1] && res[1]-0 < 5) { iosscroll = false; } // its ios and before version 5 so switch iosscroll off
  }
} else {
  iosscroll = false;
}
//iosscroll = false;
//alert(navigator.userAgent);

var registeredScrollers = [];
var scrollersSwitchedOn = false;
// ********
// If a scroller is not visible on the initial page, you will need to register it when it first becomes
// visible.
// ********
// you need to call registerScroller once for each scroller defined in the code
function registerScroller(id_root, data) {
//  if (window.resizeTimerOn == true && scrollersSwitchedOn) { return; }  // something calls this during resize so dump the call
  registeredScrollers[id_root] = data;
  if (scrollersSwitchedOn) {  // already run main switchon so switch this one on now.
    refreshScrollers(id_root,true);
  }
}

// switchOnScrollers is called automaticaly but can be called again to pick up any scrollers which
// were created during onload
function switchOnScrollers() {
  for (var ix in registeredScrollers) {
    var el = document.getElementById(ix);
    if (el) {  // the div exists
      if (el.scroll == undefined) { // it has not already been switched on
        makeScroller(ix,registeredScrollers[ix]);
      }
    }
  }
  scrollersSwitchedOn = true;
}

// this is called by window resize events but can be called at any time
// ds added scroll timer to queue calls and ensure alt images scroller refreshes correctly
var scrollTimer;
function refreshScrollers(id,reset) {
  if(scrollTimer) {
    window.clearTimeout(scrollTimer);
  }
  scrollTimer = window.setTimeout(function() {
    for (var ix in registeredScrollers) {
      if (id == undefined || id == ix) {
        var el = document.getElementById(ix);
        if (el) {  // the div exists
          if ((el.scroll != undefined) || reset) { // it has been switched on or its a forced reset
            if (reset) { makeScroller(ix,registeredScrollers[ix]); }  else { makeScroller(ix); }
          }
        }
      }
    }
  }, 500);
}

function stopScroller(id) {
  for (var ix in registeredScrollers) {
    if (id == undefined || id == ix || id == null) {
      var el = document.getElementById(ix);
      if (el) {  // the div exists
        if (el.scrollbar) {
          el.scrollbar._stopAutoScroll();
        }
      }
    }
  }
}

var o_w=0,o_h=0,n_w=0,n_h=0;
function makeScroller(id_root, options) {
  this.reset = function(el) {
    // this can be called multiple times without options for resetting after a window resize
    if (options && options != undefined) {
      // override width/height of the inner (scrolling) div
      options.width = options.width != undefined ? options.width : 0;
      options.height = options.height != undefined ? options.height : 0;
      // step size when left /right are clicked
      options.xstep = options.xstep != undefined ? options.xstep : false;
      // step size when left /right are clicked
      options.ystep = options.ystep != undefined ? options.ystep : false;
      // mouseover:true to use left/right button mouse over rather than click
      options.mouseover = options.mouseover != undefined ? options.mouseover : false;
      // autohide the controls if nothing to scroll
      options.autohide = options.autohide != undefined ? options.autohide : false;
      // autohidebuttons:true adds '_off' to the buttons class when it should be hidden - style as needed
      options.autohidebuttons = options.autohidebuttons != undefined ? options.autohidebuttons : false;
      // autosizehandles:true = make the scrollbar handle sizes proportional to the scroller
      options.autosizehandles = options.autosizehandles != undefined ? options.autosizehandles : false;
    		options.as_mode = options.as_mode != undefined ? options.as_mode : as_bounce;
    		options.as_direction = options.as_direction != undefined ? options.as_direction : as_horizontal;
    		options.as_step = options.as_step != undefined ? options.as_step : 10;
    		options.as_width = options.as_width != undefined ? options.as_width : 100;
    		options.as_delay = options.as_delay != undefined ? options.as_delay : 3000;
  		  options.autoscroll = options.autoscroll != undefined ? options.autoscroll : false;

      el.scroll = {
        self : this,
        id_root : id_root,
        elo : document.getElementById(id_root+'_outer'),
        el : el,
        eli : document.getElementById(id_root+'_inner'),
        eliwid : options.width,
        elihgt : options.height,
        options : options,
        horiz : false,
        elhc : document.getElementById(id_root+'_hctrl'),
        ell : document.getElementById(id_root+'_left'),
        elr : document.getElementById(id_root+'_right'),
        elhb : document.getElementById(id_root+'_hbar'),
        elhh : document.getElementById(id_root+'_hhandle'),

        vert : false,
        elvc : document.getElementById(id_root+'_vctrl'),
        elu : document.getElementById(id_root+'_up'),
        eld : document.getElementById(id_root+'_down'),
        elvb : document.getElementById(id_root+'_vbar'),
        elvh : document.getElementById(id_root+'_vhandle')
      }
    }

    if (!el.scroll) { return; }

    el.scroll.horiz = el.scroll.elhc != undefined;
    el.scroll.vert = el.scroll.elvc != undefined;

    el.scroll.eli.style.width = '';   // revert to css setting
    el.scroll.eli.style.height = '';
    el.scroll.el.style.position = 'relative';
    el.scroll.eli.style.position = 'absolute';
//    el.scroll.el.style.overflow = 'hidden';

    if (el.scroll.eli.offsetWidth > 0) {

      if (el.scroll.eliwid > 0) {
          el.scroll.eli.style.width = el.scroll.eliwid+'px';

      } else if (el.scroll.eliwid == 0) {
        // no passed width and left button or horiz bar in use
        // assume all contents are laid out as required and find the width
  //      el.scroll.eli.style.width = '';
        if (el.scroll.eli.hasChildNodes()) {
          for (i=0; i < el.scroll.eli.childNodes.length; i++) {
            if (el.scroll.eli.childNodes[i].offsetLeft != undefined) {
              if (typeof(el.scroll.eli.childNodes[i]) == 'object') {
                var m = cssStyle(el.scroll.eli.childNodes[i],'marginRight')-0;
                if (isNaN(m)) { m = 0; }
                // if you get an offsetLeft is null or ... error here then you need to specify a width or use visibility:hidden rather than display:none on the div containing the scroller
                var x = el.scroll.eli.childNodes[i].offsetLeft + el.scroll.eli.childNodes[i].offsetWidth + m ;
                if (x > el.scroll.eliwid) { el.scroll.eliwid = (x-0); }
              }
            }
          }
  //        if (el.scroll.eliwid == 0) { showDebug('scroller '+id_root+' calc width = 0 == probably not visible yet. move the call to registerScroller so it happens after it is visible'); }
          el.scroll.eli.style.width = (el.scroll.eliwid+1)+'px';
        }
      }
      if (el.scroll.elihgt > 0) {
          el.scroll.eli.style.height = el.scroll.elihgt+'px';

      } else if (el.scroll.elihgt == 0) {
        // no passed height and up button or vert bar in use
        // assume all contents are laid out as required and find the height
        if (el.scroll.eli.hasChildNodes()) {
          for (i=0; i < el.scroll.eli.childNodes.length; i++) {
            if (el.scroll.eli.childNodes[i].offsetTop != undefined) {
              if (typeof(el.scroll.eli.childNodes[i]) == 'object') {
                var m = cssStyle(el.scroll.eli.childNodes[i],'marginBottom')-0;
                if (isNaN(m)) { m = 0; }
                var x = el.scroll.eli.childNodes[i].offsetTop + el.scroll.eli.childNodes[i].offsetHeight + m ;
                if (x > el.scroll.elihgt) { el.scroll.elihgt = (x-0); }
              }
            }
          }
  //        if (el.scroll.elihgt == 0) { showDebug('scroller '+id_root+' calc height = 0 == probably not visible yet. move the call to registerScroller so it happens after it is visible'); }
          el.scroll.eli.style.height = (el.scroll.elihgt+1)+'px';
        }
      }
    }
  }


  var el = document.getElementById(id_root);
  if (!el) { return; }
  this.reset(el);
  if (!el.scroll) { return; }

  if (el.scroller == undefined) {
    el.scroller = new jsScroller(id_root, el, el.scroll.eliwid, el.scroll.elihgt);
    el.scrollbar = new jsScrollbar(el.scroller, null);
  } else {
    el.scroller.refresh(el.scroll.eliwid, el.scroll.elihgt);
    el.scrollbar.refresh();
  }
}

function jsScroller (id_root, el, w, h) {
	var self = this;

	this._setPos = function (x, y) {
//showDebug(' ..'+x+'..',true);
		if (x < this.viewableWidth - this.totalWidth)
			x = this.viewableWidth - this.totalWidth;
		if (x > 0) x = 0;
//showDebug(x+'.. ',true);
		if (y < this.viewableHeight - this.totalHeight)
			y = this.viewableHeight - this.totalHeight;
		if (y > 0) y = 0;
		this._x = x;
		this._y = y;
		with (o.style) {
			left = this._x +"px";
			top  = this._y +"px";
		}
//document.title = this._x;
	};

	this.refresh = function(wid,hgt) {
	  this._w = wid;
	  this._h = hgt;
   this.reset();
 };
	this.reset = function () {
  this.viewableWidth  = this.el.clientWidth;
	 this.viewableHeight = this.el.clientHeight;
		this.content = o;
 	this.totalWidth	 = (this._w ? this._w : o.offsetWidth);
 	this.totalHeight = (this._h ? this._h : o.offsetHeight);
		this._x = 0;
		this._y = 0;
		with (o.style) {	left = "0px";	top  = "0px";	}
	};
	this.scrollBy = function (x, y) {
		this._setPos(this._x + x, this._y + y);
	};
	this.scrollTo = function (x, y) {
		this._setPos(-x, -y);
	};
	this.stopScroll = function () {
		if (this.scrollTimer) window.clearInterval(this.scrollTimer);
	};
	this.startScroll = function (x, y) {
		this.stopScroll();
		this.scrollTimer = window.setInterval(
			function(){ self.scrollBy(x, y); }, 40
		);
	};
	this.swapContent = function (c, w, h) {
  o = document.getElementById(c.id+'_inner');
		if (w) this.viewableWidth  = w;
		if (h) this.viewableHeight = h;
		this.reset();
	};

 var o = el.scroll.eli;//document.getElementById(id_root+'_inner');

 this._w = w;
 this._h = h;
	this.el = el;
	this.scrollTimer = null;
	this.reset();
};

function stopEventPropogation(e) {
  if ('bubbles' in e) {   // all browsers except IE before version 9
    if (event.bubbles) { e.stopPropagation ();}
  } else {
    event.cancelBubble = true;
  }
}

var ccc = 0;
function jsScrollbar (s, ev) {
	var self = this;
	this.refresh = function () {
		this._y = 0;
  this._yMax = 0;
  this._vratio = 1;
		this._x = 0;
  this._xMax = 0;
  this._hratio = 1;

  if (this._vert && this._vTrack && this._vHandle) {
  		this._trackTop = 0;
		  this._trackHeight  = this._vTrack.clientHeight;
    if (this.autosizehandles === true && (this._src.totalHeight > 0)) {
      var rat = this._trackHeight * (this._src.viewableHeight / this._src.totalHeight);
      if (rat > this._trackHeight) { rat = this._trackHeight; }
      var bo = hborder(this._vHandle);
      this._vHandle.style.height = (rat-bo)+'px';
    }
  		this._handleHeight = this._vHandle.offsetHeight;
    this._vTrack.style.position = 'relative';
    this._vHandle.style.position = 'absolute';
  		with (this._vHandle.style) {	top  = "0px";	left = "0px"; }
  } else { // no trackbar
  		this._trackTop = 0;
		  this._trackHeight  = this._src.totalHeight;
  		this._handleHeight = 0;
  }
		if (this._trackHeight - this._handleHeight > 0)  {
  		this._vratio = (this._src.totalHeight - this._src.viewableHeight)/(this._trackHeight - this._handleHeight);
  } else {
  		this._vratio = (this._src.totalHeight - this._src.viewableHeight);
  }

  if (this._vratio == 0) { this._vratio = 1; }
		this._yMax = this._trackHeight - this._handleHeight;
//alert('x');
  if (this._horiz && this._hTrack && this._hHandle) {
//  		this._trackLeft = 0;
	  	this._trackWidth  = this._hTrack.clientWidth;
    if (this.autosizehandles === true && (this._src.totalWidth > 0)) {
      var rat = this._trackWidth * (this._src.viewableWidth / this._src.totalWidth);
      if (rat > this._trackWidth) { rat = this._trackWidth; }
      var bo = wborder(this._hHandle);
      this._hHandle.style.width = (rat-bo)+'px';
    }
  		this._handleWidth = this._hHandle.offsetWidth;
    this._hTrack.style.position = 'relative';
    this._hHandle.style.position = 'absolute';
  		with (this._hHandle.style) {	top  = "0px";	left = "0px"; }
  } else {  // no trackbar
//  		this._trackLeft = 0;
	  	this._trackWidth = this._src.totalWidth;
  		this._handleWidth = 0;
  }
		if (this._trackWidth - this._handleWidth > 0) {
  		this._hratio = (this._src.totalWidth - this._src.viewableWidth)/(this._trackWidth - this._handleWidth);
  } else {
  		this._hratio = (this._src.totalWidth - this._src.viewableWidth);
  }
  if (this._hratio == 0) { this._hratio = 1; }
		this._xMax = this._trackWidth - this._handleWidth;

  if (this._vert) {
  		if (this._src.totalHeight < this._src.viewableHeight) {
  			this._disabled = true;
  			if (this.autohide) {
    			if (this._vHandle) { this._vHandle.style.visibility = "hidden"; }
    			if (this._vctrl) { this._vctrl.style.visibility  = "hidden"; }
    	}
  		} else {
  			this._disabled = false;
  			if (this._vHandle) { this._vHandle.style.visibility = "visible"; }
  			if (this._vctrl) { this._vctrl.style.visibility  = "visible"; }
  		}
  }
  if (this._horiz) {
  		if (this._src.totalWidth < this._src.viewableWidth) {
  			this._disabled = true;
  			if (this.autohide) {
    			if (this._hHandle) { this._hHandle.style.visibility = "hidden"; }
    			if (this._hctrl) { this._hctrl.style.visibility  = "hidden"; }
     }
  		} else {
  			this._disabled = false;
  			if (this._hHandle) { this._hHandle.style.visibility = "visible"; }
  			if (this._hctrl) { this._hctrl.style.visibility  = "visible"; }
  		}
  }
		this.redraw();
 };
 
	this.reset = function () {
		//Arguments that were passed
		this.el = s.el;
		this._src    = s;
		this.autohide = this.el.scroll.options.autohide;
		this.autohidebuttons = this.el.scroll.options.autohidebuttons;
		this.autosizehandles = this.el.scroll.options.autosizehandles;
		this.mouseover = this.el.scroll.options.mouseover;
		this.id_root = this.el.scroll.id_root;
		this.eventHandler = ev ? ev : function () {};

  this._vert   = this.el.scroll.vert;
		this._vctrl  = this.el.scroll.elvc;
		this._up     = this.el.scroll.elu;
		this._down   = this.el.scroll.eld;
		this._vTrack = this.el.scroll.elvb;
		this._vHandle = this.el.scroll.elvh;

  this._horiz   = this.el.scroll.horiz;
		this._hctrl  = this.el.scroll.elhc;
		this._left   = this.el.scroll.ell;
		this._right  = this.el.scroll.elr;
		this._hTrack = this.el.scroll.elhb;
		this._hHandle = this.el.scroll.elhh;

		this._vscrollDist  = this.el.scroll.options.ystep ? this.el.scroll.options.ystep : 5;
		this._hscrollDist  = this.el.scroll.options.xstep ? this.el.scroll.options.xstep : 5;
		this._scrollTimer = null;
		this._selectFunc  = null;
		this._grabPoint   = null;
		this._grabX   = null;
		this._grabY   = null;
		this._initgrabX   = null;
		this._initgrabY   = null;
		this._tempTarget  = null;
		this._tempDistX   = 0;
		this._tempDistY   = 0;
		this._disabled    = false;
		this._autoScrollTimer = null;
		this._as_aborted = false;
		this._as_mode = this.el.scroll.options.as_mode;
		this._as_direction = this.el.scroll.options.as_direction;
		this._as_step = this.el.scroll.options.as_step;
		this._as_width = this.el.scroll.options.as_width;
		this._as_delay = this.el.scroll.options.as_delay;
		this._autoScroll = this.el.scroll.options.autoscroll;
		this._autoScrollSize = 0;
		this._as_inc = 1;
		this._is_over = false; // mouse is over outer div
  this._draggingTimer = null;

  this.refresh();

  if (this._vert) {
    if (this._vTrack && this._vHandle) {
    		this._vHandle.ondragstart  = function () {return false;};
    		this._vHandle.onmousedown = function () {return false;};
    		this._vHandle.ondragstart  = function () {return false;};
    		this._vHandle.onmousedown = function () {return false;};
    }
  		this._addEvent(this._src.content, "mousewheel", this._scrollbarWheel);

  		this._removeEvent(this._vctrl, "mousedown", this._scrollbarClick);
  		this._addEvent(this._vctrl, "mousedown", this._scrollbarClick);
  		this._addEvent(this._vctrl, "touchstart", this._scrollbarClick, true);
  		this._addEvent(this.el.scroll.eli, "touchstart", this._scrollbarClick);

    if (this._up) {
    		this._addEvent(this.el.scroll.elo, "touchstart", function() { self._up.style.display = 'none'; });
    }
    if (this._down) {
    		this._addEvent(this.el.scroll.elo, "touchstart", function() { self._down.style.display = 'none'; });
    }

    if (this.mouseover === true) {
      if (this._up) {
      		this._addEvent(this._up, "mouseover", this._mouseoverUp);
      		this._addEvent(this._up, "mouseout", this._stopScroll);
      }
      if (this._down) {
      		this._addEvent(this._down, "mouseover", this._mouseoverDown);
      		this._addEvent(this._down, "mouseout", this._stopScroll);
      }
    }
  }

  if (this._horiz) {
    if (this._hTrack && this._hHandle) {
    		this._hHandle.ondragstart  = function () {return false;};
    		this._hHandle.onmousedown = function () {return false;};
    		this._hHandle.ondragstart  = function () {return false;};
    		this._hHandle.onmousedown = function () {return false;};
    }
  		this._removeEvent(this._hctrl, "mousedown", this._scrollbarClick);
  		this._addEvent(this._hctrl, "mousedown", this._scrollbarClick);
  		this._addEvent(this._hctrl, "touchstart", this._scrollbarClick, true);
  		this._addEvent(this._hctrl, "mouseover", function() { self._is_over = true; });
  		this._addEvent(this._hctrl, "mouseout", function() { self._is_over = false; });
//  		this._addEvent(this.el.scroll.eli, "mousedown", this._scrollbarClick);  // to enable mouseover dragging
// note I havent stopped onclick happening to <a> links in the scrolling div yet

    if (this._left) {
    		this._addEvent(this.el.scroll.elo, "touchstart", function() { self._left.style.display = 'none'; });
    }
    if (this._right) {
    		this._addEvent(this.el.scroll.elo, "touchstart", function() { self._right.style.display = 'none'; });
    }

    if (this.mouseover === true) {
      if (this._left) {
      		this._addEvent(this._left, "mouseover", this._mouseoverLeft);
      		this._addEvent(this._left, "mouseout", this._stopScroll);
      }
      if (this._right) {
      		this._addEvent(this._right, "mouseover", this._mouseoverRight);
      		this._addEvent(this._right, "mouseout", this._stopScroll);
      }
    }
  }

  if (iosscroll) {
  		this._addEvent(this.el.scroll.eli, "touchmove", this._stopAutoScroll);
    if (this._hTrack && this._hHandle) {
      self._hTrack.style.display = 'none';   // this line hides the arrows if iosscroll
//      this.el.style.overflowX = "auto";
    }
    if (this._vTrack && this._vHandle) {
      self._vTrack.style.display = 'none';
//      this.el.style.overflowY = "auto";
    }
    this.el.style.overflow = "auto";
  } else {
  		this._addEvent(this.el.scroll.eli, "touchstart", this._scrollbarClick);
  }

		this._addEvent(this.el, "dragstart", function() { return false; }, true);
		this._addEvent(this.el, "mouseover", function() { self._is_over = true; });
		this._addEvent(this.el, "mouseout", function() { self._is_over = false; });
		this._addEvent(this.el, "mousedown", function() { self.as_aborted = true; });

		this._src.reset();

		this._moveContent();

  if (this._autoScroll) { this._startAutoScroll(); }
	};

 this._onAutoScroll = function () {
   if (self.as_aborted) {
     return;
   }
   if (self._is_over) {
     self._autoScrollTimer = window.setTimeout(function () {	self._onAutoScroll();	}, 300);
     return;
   }

 		var step = self._as_step;
   if (self._as_inc == -1 && self._as_mode == as_rewind) { // rewind quickly
     step = (self._as_direction == as_vertical) ? self._src.totalHeight / 10 : self._src.totalWidth / 10;
   }

 		self._autoScrollSize -= step;
 		if (self._autoScrollSize < 0) { step += self._autoScrollSize; }
   if (self._as_direction == as_vertical) { self.scrollBy(0,(step*self._as_inc)); }
                                     else { self.scrollBy((step*self._as_inc),0); }
   if (self._autoScrollSize > 0 && !self.as_aborted) {
     self._autoScrollTimer = window.setTimeout(function () {	self._onAutoScroll();	}, 40);
   } else if (!self.as_aborted) {

     self._startAutoScroll();
   }
 };
 this._startAutoScroll = function () {
   if (this.as_aborted) {return;}
   if (!animate) {return;}
   if (this._as_direction == as_vertical) {
     if (this._y >= this._yMax) { this._as_inc = -1; }
     if (this._y <= 0) { this._as_inc = 1; }
   } else {
     if (this._x >= this._xMax) { this._as_inc = -1; }
     if (this._x <= 0) { this._as_inc = 1; }
   }
   if (this._as_inc == -1 && this._as_mode == as_once) { return; } // hit the end so get out
   
   if (self._as_inc == -1 && self._as_mode == as_rewind) { // rewind quickly
   		this._autoScrollSize = (self._as_direction == as_vertical) ? self._src.totalHeight : self._src.totalWidth;
   } else {
   		this._autoScrollSize = self._as_width;
   }
   if (self._as_mode == as_bounce) { self.as_inc = -self.as_inc; }
   this._autoScrollTimer = window.setTimeout(function () {	self._onAutoScroll();	}, this._as_delay);
 };
 this._stopAutoScroll = function () {
   this.as_aborted = true;
 		if (self._autoScrollTimer) window.clearInterval(self._autoScrollTimer);
   if (iosscroll) {
   		self._removeEvent(self.el.scroll.eli, "touchmove", self._stopAutoScroll);
   }
 };
 this._ondraggingTimer = function () {
   dragging = false;
 };
	this._addEvent = function (o, t, f) {
		if (o.addEventListener) o.addEventListener(t, f, false);
		else if (o.attachEvent) o.attachEvent('on'+ t, f);
		else o['on'+ t] = f;
	};

	this._removeEvent = function (o, t, f) {
		if (o.removeEventListener) o.removeEventListener(t, f, false);
		else if (o.detachEvent) o.detachEvent('on'+ t, f);
		else o['on'+ t] = null;
	};

	this._mouseoverLeft = function (e) { self._startScroll(-self._hscrollDist,0);	};
	this._mouseoverRight = function (e) { self._startScroll(self._hscrollDist,0); };
	this._mouseoverUp = function (e) { self._startScroll(0,-self._vscrollDist);	};
	this._mouseoverDown = function (e) { self._startScroll(0,self._vscrollDist);	};
	
	this._scrollbarClick = function (e) {
		if (self._disabled) return false;
  self._stopAutoScroll();

  self.breakit= false;

		e = e ? e : event;
		if (!e.target) e.target = e.srcElement;
  if (!e.target && e.targetTouches) e.target = e.targetTouches[0].target;
//showDebug('target id '+e.target+' xxx',true);

  var target = e.target; cc = 0;
  if (target.id == undefined) { target.id = 'xx'; }
//showDebug('target id '+target+' '+target.id+' '+ccc+' '+(e.targetTouches ? e.targetTouches[0].target : '')+'<br>');
  if (target.id == '' || target.id.indexOf(self.id_root) < 0) {
   while (cc < 5 && target && target.id.indexOf(self.id_root) < 0) {
      cc++;
      target = target.parentNode;
    }
  }
  if (!target) { return; }

//ccc++;
//showDebug('target id '+target.id+' '+ccc,true);

		if (target.id == self.id_root+'_up') self._scrollUp(e);
		else if (target.id == self.id_root+'_down') self._scrollDown(e);
		else if (target.id == self.id_root+'_left') {
    if (self.mouseover) { return; } else {self._scrollLeft(e); }
  }
		else if (target.id == self.id_root+'_right') {
//showDebug(self.mouseover,true);
    if (self.mouseover) { return; } else {self._scrollRight(e); }
  }
		else if (target.id == self.id_root+'_vbar') self._scrollvTrack(e);
		else if (target.id == self.id_root+'_vhandle') self._scrollvHandle(e);
		else if (target.id == self.id_root+'_hbar') self._scrollhTrack(e);
		else if (target.id == self.id_root+'_hhandle') { self._scrollhHandle(e); }
		else if (target.id == self.id_root+'_inner') { self._scrollInner(e); }

  if (self.breakit) { return; }
  
		self._tempTarget = target;
		self._selectFunc = document.onselectstart;
		document.onselectstart = function () {return false;};

		self.eventHandler(target, "mousedown");
		self._addEvent(document, "mouseup", self._stopScroll, false);
		self._addEvent(document, "touchend", self._stopScroll, false);

		return false;
	};

	this._vscrollbarDrag = function (e) {
  if (!self._vTrack || !self._vHandle) { return; }
		e = e ? e : event;
		if (e.preventDefault) { e.preventDefault(); } // stop ios trying to scroll
		var t = parseInt(self._vHandle.style.top);
		var v = mouseY(e);// - self._trackTop;
		with (self._vHandle.style) {
			if (v >= self._trackHeight - self._handleHeight + self._grabPoint)
				top = self._trackHeight - self._handleHeight +"px";
			else if (v <= self._grabPoint) top = "0px";
			else top = v - self._grabPoint +"px";
			self._y = parseInt(top);
		}
//showDebug('top '+self._vHandle.style.top+' '+v+ '<br>');

		self._moveContent();
	};

	this._hscrollbarDrag = function (e) {
    
  if (!self._hTrack || !self._hHandle) { return; }
		e = e ? e : event;
		if (e.preventDefault) { e.preventDefault(); } // stop ios trying to scroll
		var l = parseInt(self._hHandle.style.left);
		var v = mouseX(e);// - self._trackLeft;
//ccc++;
		with (self._hHandle.style) {
			if (v >= self._trackWidth - self._handleWidth + self._grabPoint)
				left = self._trackWidth - self._handleWidth +"px";
			else if (v <= self._grabPoint) left = "0px";
			else left = v - self._grabPoint +"px";
//ccc++;
//showDebug(left+' '+v,true);
			self._x = parseInt(left);
		}

		self._moveContent();
	};

	this._fingerDrag = function (e) {  // only ever get here if touch events work
  
		e = e ? e : event;
		var x = mouseX(e);
		var y = mouseY(e);
  var h = Math.abs(self._initgrabX - x);
  var v = Math.abs(self._initgrabY - y);
  var end_h = false, end_v = false;
  var myinc = false;
  var inc = 0;

  if (self._hHandle) {
    
    var max = self._trackWidth - self._handleWidth;
    if (ipadscroll) {
      inc = (self._grabX - x) / self._hratio;
      if (inc < 1 && inc > 0) { inc = 1; myinc = true; }
      if (inc > -1 && inc < 0) { inc = -1; myinc = true; }
      inc = Math.round(inc);
//showDebug(inc+' ',true)
      if (Math.abs(inc) > 3) { inc = inc * 2; }
      var left = (inc) + self._hHandle.offsetLeft;
    } else {
      var left = self._grabX - x + self._hHandle.offsetLeft;
    }
//showDebug(left+' '+max+' -- ',true);
    if (left < 0)  { left = 0; end_h = true; }
    if (left > max) { left = max; end_h = true;  }
  		self._x = parseInt(left);
  		self._hHandle.style.left = left + 'px';
  		self._grabX = x;
  }
  if (self._vHandle) {
    var max = self._trackHeight - self._handleHeight;
    if (ipadscroll) {
      inc = (self._grabY - y) / self._vratio;
      if (inc < 1 && inc > 0) { inc = 1; myinc = true; }
      if (inc > -1 && inc < 0) { inc = -1; myinc = true; }
      inc = Math.round(inc);
//showDebug(inc+' ',true)
      if (Math.abs(inc) > 3) { inc = inc * 2; }
      var top = (inc) + self._vHandle.offsetTop;
    } else {
      var top = self._grabY - y + self._vHandle.offsetTop;
    }
//showDebug(top+' '+v+ ' '+ self._grabY+' '+max);
    if (top < 0)  { top = 0; end_v = true; }
    if (top > max) { top = max; end_v = true; }
  		self._y = parseInt(top);
  		self._vHandle.style.top = top + 'px';
  		self._grabY = y;
  }
//ccc++;
//showDebug(v+ ' '+ self._grabY+' '+max,true);
		if (e.preventDefault && !iosscroll) {
    // try to identify a main scroll gesture
    var allowDef = false;
    if (!self._hHandle && (h > v)) { allowDef = true; }
    if (!self._vHandle && (v > h)) { allowDef = true; }
    if ((!self._hHandle || (end_h && h > 10)) && (!self._vHandle || (end_v && v > 10))) { allowDef = true; }
//showDebug(allowDef+' '+h+' '+v+' '+end_h+' '+end_v,true);
    if (!allowDef) {
      e.preventDefault(); // stop ios trying to scroll
    }
  }
		if (inc != 0 ) { dragging = true; }
		if (self._draggingTimer) window.clearInterval(self._draggingTimer);

		self._moveContent();
	};

	this._scrollbarWheel = function (e) {
		e = e ? e : event;
		var dir = 0;
		if (e.wheelDelta >= 120) dir = -1;
		if (e.wheelDelta <= -120) dir = 1;

		self.scrollBy(0, dir * 20);
		e.returnValue = false;
	};

	this._startScroll = function (x, y) {
//		if ((x == -110 && this._hscrollDist > 20) || (y == 110 && this._vscrollDist > 20)) {
    // dont auto repeat for large steps - its too fast
//    self.breakit= true;
	//	  self.scrollBy(x,y);
 // } else {
  		this._tempDistX = x;
  		this._tempDistY = y;
  		this._scrollTimer = window.setInterval(function () {
  			self.scrollBy(self._tempDistX, self._tempDistY);
  		}, 40);
//  }
	};

	this._stopScroll = function (e) {
//ccc++;
//document.title = 'stop '+ccc;
//		e = e ? e : event;
//		if (!e.target) e.target = e.srcElement;

		self._removeEvent(document, "mousemove", self._vscrollbarDrag, false);
		self._removeEvent(document, "mousemove", self._hscrollbarDrag, false);
//		self._removeEvent(document, "mousemove", self._fingerDrag, false);
  if (!iosscroll) {
  		self._removeEvent(document, "touchmove", self._hscrollbarDrag, false);
  		self._removeEvent(document, "touchmove", self._vscrollbarDrag, false);
  		self._removeEvent(document, "touchmove", self._fingerDrag, false);
  }
		self._removeEvent(document, "mouseup", self._stopScroll, false);
		self._removeEvent(document, "mouseover", self._stopScroll, false);

		if (self._selectFunc) document.onselectstart = self._selectFunc;
		else document.onselectstart = function () { return true; };

		if (self._scrollTimer) window.clearInterval(self._scrollTimer);
		self.eventHandler (self._tempTarget, "mouseup");

  self._draggingTimer = window.setTimeout(function () {	self._ondraggingTimer();	}, 400);

//  if (e.target.id.indexOf(self.id_root) < 0) {
////    e.preventDefault();
//    e.returnValue = false;
//    return false;
//  }
	};
	this._scrollUp = function (e) {this._startScroll(0, -this._vscrollDist);};
	this._scrollDown = function (e) {this._startScroll(0, this._vscrollDist);};
	this._scrollLeft = function (e) {this._startScroll(-this._hscrollDist,0);};
	this._scrollRight = function (e) {this._startScroll(this._hscrollDist,0);};
	this._scrollvTrack = function (e) {
  if (!this._vTrack || !this._vHandle) { return; }
  var b = getBounds(this._vTrack);
		var curY = mouseY(e) - b.y;
		this._scroll(this._x, curY - this._handleHeight/2);
	};
	this._scrollhTrack = function (e) {
  if (!this._hTrack || !this._hHandle) { return; }
  var b = getBounds(this._hTrack);
		var curX = mouseX(e) - b.x;
		this._scroll(curX - this._handleWidth/2, this._y);
	};
	this._scrollvHandle = function (e) {
		var curY = mouseY(e);
		this._grabPoint = curY - this._vHandle.offsetTop;
		this._addEvent(document, "mousemove", this._vscrollbarDrag, false);
		this._addEvent(document, "touchmove", this._vscrollbarDrag, false);
	};
	this._scrollhHandle = function (e) {
		var curX = mouseX(e);
		this._grabPoint = curX - this._hHandle.offsetLeft;
//alert(curX+'cccc');
		this._addEvent(document, "mousemove", this._hscrollbarDrag, false);
		this._addEvent(document, "touchmove", this._hscrollbarDrag, false);
	};
	this._scrollInner = function (e) {
		var curX = mouseX(e);
		this._grabX = curX;
		this._initgrabX = curX;
		var curY = mouseY(e);
		this._grabY = curY;
		this._initgrabY = curY;
//alert(curX+'cccc');
		this._addEvent(document, "mousemove", this._fingerDrag, false);
		this._addEvent(document, "touchmove", this._fingerDrag, false);
	};

	this._scroll = function (x, y) {
 	if (y > this._trackHeight - this._handleHeight)	y = this._trackHeight - this._handleHeight;
		if (y < 0) y = 0;
  if (this._vTrack && this._vHandle) {	this._vHandle.style.top = y +"px"; }
		this._y = y;

		if (x > this._trackWidth - this._handleWidth)	x = this._trackWidth - this._handleWidth;
		if (x < 0) x = 0;
  if (this._hTrack && this._hHandle) {	this._hHandle.style.left = x +"px"; }
		this._x = x;

		this._moveContent();
	};
	this._moveContent = function () {
    
//showDebug(this._x+' '+this._y+' '+this._vratio+' '+this._hratio+' -- ',true);
		this._src.scrollTo(Math.round(this._x * this._hratio), Math.round(this._y * this._vratio));
  this.redraw();
	};
	this.redraw = function () {
    
	 function doClass(btn, is_on) {
    if (is_on) { btn.className = btn.className.replace(/_off/ig,''); }
      else { if (btn.className.indexOf('_off') == -1) { btn.className += '_off'; } }
	 }
  if (self.autohidebuttons) {
    if (self._left) { doClass(self._left, self._x > 0); }
    if (self._right) { doClass(self._right, self._x < self._xMax); }
    if (self._up) { doClass(self._up, self._y > 0); }
    if (self._down) { doClass(self._down, self._y < self._yMax); }
//showDebug(self._x+' '+self._y+' '+self._xMax+' '+self._yMax+'<br>',true);
  }
 };
	this.scrollBy = function (x, y) {
		this._scroll((-this._src._x + x)/this._hratio, (-this._src._y + y)/this._vratio);
	};
	this.scrollTo = function (x, y) {
		this._scroll(x/this._hratio, y/this._vratio);
	};
	this.swapContent = function (o, w, h) {
		this._removeEvent(this._src.content, "mousewheel", this._scrollbarWheel, false);
		this._src.swapContent(o, w, h);
		this.reset();
	};

	this.reset();
};

addLoadEvent(switchOnScrollers,600);
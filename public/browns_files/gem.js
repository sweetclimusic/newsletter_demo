var GEM_InUse = true;

function GEM_Add_data_fp(tmp1, part) {
//  var pr = (window.GEM_fp) ? roundDP(window.GEM_fp['x'+part.part],2) : '';
  var pr = roundDP(part.price,2);
  if (pr) { pr = '{"'+GEM_Country+'":'+pr+'}'; }
  tmp1 = tmp1.replace(/\[xx[_ ]data_fp\]/ig, pr);

  return tmp1;
}

addEvent(window, 'message', GEM_paymentCompleteMessage);

function GEM_completeCallBack(state, status, res) {
  if (state == 4) {
    GEM_complete_response = 'status='+status+' response='+res;
    if (status == 200 && res == 'notdone') {
      startGEMcompletetimer();
    }
    //if there was an error or res = 'ok' dont restart the timer
  }
}

var GEM_complete_response = 'order completion message has not been received';

function GEM_paymentCompleteMessage(evt) {
  // called from GEM iframe when the order is complete

  if (!evt.data || !evt.data.indexOf) return ;  //something else was sending messages and screwing up on indexOf

  var x = evt.data.split(' ') // expecting 'orderno complete'
  if (x.length == 2) {
    if (x[1] == 'complete') {
      GEM_completetimer_order = x[0];
      GEM_complete();
    }
  }
}

var GEM_completetimer = null;
function startGEMcompletetimer() {
  GEM_completetimer = setTimeout(GEM_complete,2000);
}
var GEM_completetimer_count = 0, GEM_completetimer_order = '';
function GEM_complete() {
  GEM_completetimer_count++;
  callAjax(thisUrl+'GEM_service.asp?ajax=y&svc=complete&order='+GEM_completetimer_order,GEM_completeCallBack);
  GEM_complete_response = 'order '+GEM_completetimer_order+' server notified '+GEM_completetimer_count+' times=';
}
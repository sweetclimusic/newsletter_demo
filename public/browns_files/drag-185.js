var DragHandler = {

	obj : null,

// added doScroll parameter. If true then o should be the content and oroot should be the scrolling container
	attach : function(o, oRoot, doScroll, minX, maxX, minY, maxY, bSwapHorzRef, bSwapVertRef, fXMapper, fYMapper)
	{
  if (is_mob) {
  		o.ontouchstart	= DragHandler.start;
  } else {
  		o.onmousedown	= DragHandler.start;
  }
		o.style.cursor = 'move';

		o.hmode			= bSwapHorzRef || doScroll ? false : true ;
		o.vmode			= bSwapVertRef || doScroll ? false : true ;

		o.root = oRoot && oRoot != null ? oRoot : o ;
  o.doScroll = doScroll;

		o.minX	= typeof minX != 'undefined' ? minX : null;
		o.minY	= typeof minY != 'undefined' ? minY : null;
		o.maxX	= typeof maxX != 'undefined' ? maxX : null;
		o.maxY	= typeof maxY != 'undefined' ? maxY : null;

		o.xMapper = fXMapper ? fXMapper : null;
		o.yMapper = fYMapper ? fYMapper : null;

		o.root.onDragStart	= new Function();
		o.root.onDragEnd	= new Function();
		o.root.onDrag		= new Function();
	},

	start : function(e)
	{
		var o = DragHandler.obj = this;
		e = DragHandler.fixE(e);

		if (o.hmode  && isNaN(parseInt(o.root.style.left  ))) o.root.style.left   = o.root.offsetLeft+"px";
		if (o.vmode  && isNaN(parseInt(o.root.style.top   ))) o.root.style.top    = o.root.offsetTop+"px";
		if (!o.hmode && isNaN(parseInt(o.root.style.right ))) o.root.style.right  = "0px";
		if (!o.vmode && isNaN(parseInt(o.root.style.bottom))) o.root.style.bottom = "0px";

		var y = parseInt(o.vmode ? o.root.style.top  : o.root.style.bottom);
		var x = parseInt(o.hmode ? o.root.style.left : o.root.style.right );
		o.root.onDragStart(x, y);

		o.lastMouseX	= mouseX(e);
		o.lastMouseY	= mouseY(e);

		if (o.hmode) {
			if (o.minX != null)	o.minMouseX	= o.lastMouseX - x + o.minX;
			if (o.maxX != null)	o.maxMouseX	= o.minMouseX + o.maxX - o.minX;
		} else {
			if (o.minX != null) o.maxMouseX = -o.minX + o.lastMouseX + x;
			if (o.maxX != null) o.minMouseX = -o.maxX + o.lastMouseX + x;
		}

		if (o.vmode) {
			if (o.minY != null)	o.minMouseY	= o.lastMouseY - y + o.minY;
			if (o.maxY != null)	o.maxMouseY	= o.minMouseY + o.maxY - o.minY;
		} else {
			if (o.minY != null) o.maxMouseY = -o.minY + o.lastMouseY + y;
			if (o.maxY != null) o.minMouseY = -o.maxY + o.lastMouseY + y;
		}

  if (is_mob) {
  		document.ontouchmove	= DragHandler.drag;
		  document.ontouchend		= DragHandler.end;
		} else {
  		document.onmousemove	= DragHandler.drag;
		  document.onmouseup		= DragHandler.end;
  }

		return false;
	},

	drag : function(e)
	{
		e = DragHandler.fixE(e);
		var o = DragHandler.obj;
  if (!o) return;

		var ey	= mouseY(e);
		var ex	= mouseX(e);

  if (o.doScroll) {
  		var y = parseInt(o.root.scrollTop);
		  var x = parseInt(o.root.scrollLeft);
  } else {
  		var y = parseInt(o.vmode ? o.root.style.top  : o.root.style.bottom);
		  var x = parseInt(o.hmode ? o.root.style.left : o.root.style.right );
		}
		var nx, ny;

		if (o.minX != null) ex = o.hmode ? Math.max(ex, o.minMouseX) : Math.min(ex, o.maxMouseX);
		if (o.maxX != null) ex = o.hmode ? Math.min(ex, o.maxMouseX) : Math.max(ex, o.minMouseX);
		if (o.minY != null) ey = o.vmode ? Math.max(ey, o.minMouseY) : Math.min(ey, o.maxMouseY);
		if (o.maxY != null) ey = o.vmode ? Math.min(ey, o.maxMouseY) : Math.max(ey, o.minMouseY);

		nx = x + ((ex - o.lastMouseX) * (o.hmode ? 1 : -1));
		ny = y + ((ey - o.lastMouseY) * (o.vmode ? 1 : -1));

		if (o.xMapper)		nx = o.xMapper(y)
		else if (o.yMapper)	ny = o.yMapper(x)

  if (o.doScroll) {
//document.title = o.root.scrollTop+' '+ny+' '+ey+' '+o.root.id+' '+ccc;
//    o.root.style.overflow = 'hidden';
// scrolltop doesnt work on older android - leave it for now.
  		o.root.scrollTop = ny;
  		o.root.scrollLeft = nx;
//    o.root.style.overflow = 'auto';
//alert(o.root.scrollTop+' '+ny+' '+ey+' '+o.root.id);
  } else {
  		o.root.style[o.hmode ? "left" : "right"] = nx + "px";
		  o.root.style[o.vmode ? "top" : "bottom"] = ny + "px";
		}
		o.lastMouseX	= ex;
		o.lastMouseY	= ey;

  if (o.root.shadow) {
    o.root.shadow.style.top = (ny+shadowOffset) + 'px';
    o.root.shadow.style.left = (nx+shadowOffset) + 'px';
  }

		o.root.onDrag(nx, ny);
		if (e.preventDefault) { e.preventDefault(); } // stop ios trying to scroll

		return false;
	},

	end : function()
	{
  if (is_mob) {
  		document.ontouchmove = null;
		  document.ontouchend  = null;
		} else {
  		document.onmousemove = null;
		  document.onmouseup   = null;
		}
		DragHandler.obj.root.onDragEnd(	parseInt(DragHandler.obj.root.style[DragHandler.obj.hmode ? "left" : "right"]),
									parseInt(DragHandler.obj.root.style[DragHandler.obj.vmode ? "top" : "bottom"]));
		DragHandler.obj = null;
	},

	fixE : function(e)
	{
		if (typeof e == 'undefined') e = window.event;
		return e;
	}
};
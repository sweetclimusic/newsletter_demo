//-------------------------------------------------------------------------
//  AJAX buy stuff
//-------------------------------------------------------------------------
var res = '';
var lightBoxTimeOut = 5000;  // milliseconds
var xpart;
var lbDiv = null;
var lbLocation = '';
var timer1 = null;
var listenerDone = false;
var safetoclose = false;
var oneclick = 0;
var oneclicktimer = null;
var lbError = false;
var ajaxRefresh = '';
var BUY_http = null;
var matrixPopupId = '';
var matrixAjaxRefresh = '';
var voucherLineNo = '';
var voucherName = '';
var voucherType = '';
var verb1 = 'tt_buy_adding'; var verb1e = 'Adding';
var verb2 = 'tt_buy_added'; var verb2e = 'Added';
var verb3 = 'tt_buy_to'; var verb3e = 'to your Basket';
var buyDisplay = 'lightbox'; // 'lightbox' or 'basket'
var replaceBasket = true; // true = ajax return will replace ajaxbasket.innerHTML

function startOneClick() {  clearTimeout(oneclicktimer); oneclick = 1; oneclicktimer = window.setTimeout("stopOneClick", 10000); }
function stopOneClick() {  clearTimeout(oneclicktimer); oneclick = 0; }

function timer1timer() {
  clearTimeout(timer1);
  closeAlertX(lb_alert);
}
//
var lbOrigWid = 0;
var lb_alert = null;
function showLightBox(myEvent, bodyid) {
  var elb = document.getElementById(bodyid);
  if (elb) {
    var idx = elb.getAttribute('data-idx');
    var colour = elb.getAttribute('data-colour') || '';
    var size   = elb.getAttribute('data-size') || '';
    var price = mx_data[idx].major;
    if (colour > -1 && size > -1) {
      var part = mx_data[idx].array[colour][size];
      var major = mx_data[idx].major;
    }
    if (colour == -99 && size == -99) {
      var part = mx_data[idx].major;
      var major = part;
    }

    if (colour) { colour = 'Colour: '+mx_data[idx].colours[colour].descr; }
    if (size) { size = 'Size: '+mx_data[idx].sizes[size].descr; }
    if (price) { price = mx_data[idx].major.price; }

    var tmp1 = getTemplate('lightbox_holder');
    tmp1 = tmp1.replace(/\[xx[_ ]major\]/ig, major.part);
    tmp1 = tmp1.replace(/\[xx[_ ]partimg\]/ig, major.part+part.colour);
    tmp1 = tmp1.replace(/\[xx[_ ]majortitle]/ig, major.title);
    tmp1 = tmp1.replace(/\[xx[_ ]part\]/ig, part.part);
    tmp1 = tmp1.replace(/\[xx[_ ]title]/ig, part.title);
    tmp1 = tmp1.replace(/\[xx[_ ]colour\]/ig, colour);
    tmp1 = tmp1.replace(/\[xx[_ ]size\]/ig, size);

    // Call Sub2 addItemtoBasket function after item successfully added to basket
    if(window.S2Tech_2Prompt) {
      S2Tech_addItemToBasket(part.part,part.title,price,'1');
    }

    lb_alert = alertX(tmp1,'',0,'','',{ });
  }
}

function showBasketDropDown() {
  showBasketPopup(null,'ajaxBasket',4000);
}

function showAjaxErrorBox(c)
{
// make sure lbDiv is not being used by lightbox
  lbError = true;
  clearTimeout(timer1);
  lbLocation = ''; // stop it refreshing the page
  ajaxRefresh = ''; // ditto
  popupClose('','');

//  c = '<ul class="vert" style="padding:10px">'+c+'</ul>';
  c = '<div style="float:left; padding:10px;">'+c+'</div>';
  alertX(c,'',0,'Please Note');
}

function expressBuy() {
  var el = document.getElementById('catCode');
  if (el) {
    if (trim(el.value) == '') { return false; }
    var part = el.value;
    part = part.replace("'",'');
    buyOneAjax(part,null,'','EXPRESS');
//    el.value = '';
  }
}

function buyOneAjax(part,e,masterpart,promo, bodyid) {
//alert(part+' - '+masterpart+' - '+promo);
  if (oneclick == 1) { return false; }

  if (!BUY_http) { BUY_http = getBuyAjax(); }

  var fixer = new Date();
  var iURL = thisUrl+"BUYajax.asp?part="+part+"&masterpart="+masterpart+"&quantity=1"+"&when="+fixer.getTime();
  var xpart = part;
  if (promo == 'PROMO')  {iURL= iURL+'&attr=promo'}
  if (promo == 'EXPRESS')  {iURL= iURL+'&express=express&template_suffix=_quickbuy'}

// now look for Gift Wrapping options
//alert('now look for Gift Wrapping options')
// try new way first
   var gw = (document.getElementById('gift_wrap_f') ? document.getElementById('gift_wrap_f').value : '');
   if (gw == 'Yes') {
     var gw_msg = document.getElementById('gift_msg_f').value;
     var gw_paper = document.getElementById('gift_paper_f').value;
     iURL = iURL+'&gift_wrap='+ajaxFilter(gw_paper)+'&gift_msg='+ajaxFilter(gw_msg);
   }

   if (gw == '') {  // new data didnt exist
     var gw;
     if (document.getElementById('gift_wrapYes')) {
        gw = document.getElementById('gift_wrapYes').checked;
     }
     if (gw) {
        var gw_msg = document.getElementById('gift_msg').value;
        var el = document.getElementById('wrapSelect');
        for (var i = 0; i < el.options.length; i++) {
           if (el.options[i].selected) { var gw_paper = el.options[i].value; }
        }
        iURL = iURL+'&gift_wrap='+ajaxFilter(gw_paper)+'&gift_msg='+ajaxFilter(gw_msg);
     }
   }

// now look for Personalisation options
//alert('now look for Personalisation options');
   var persoURL;
   var perso;
   var perso_date;
   var perso_string;
   var perso_number;
   var perso_string_option;
   var perso_number_option;
   persoURL = '';
   var ip;
   if (document.getElementById('personalisationYes')) {
      perso = document.getElementById('personalisationYes').checked;
   }
   if (perso) {
      var perso_number_count = document.getElementById('perso_number_count').value;
      var perso_string_count = document.getElementById('perso_string_count').value;
      persoURL = persoURL+'&pnc='+perso_number_count+'&psc='+perso_string_count
//alert('persoURL='+persoURL);
      for (ip = 1; ip <= perso_string_count; ip++) {
         if (document.getElementById('perso_string'+ip)) {
            perso_string = document.getElementById('perso_string'+ip).value;
            perso_string_option = document.getElementById('perso_string_option'+ip).value;
            persoURL = persoURL+'&ps'+ip+'='+perso_string+'&pso'+ip+'='+perso_string_option
         }
      }
      for (ip = 1; ip <= perso_number_count; ip++) {
         if (document.getElementById('perso_number'+ip)) {
            perso_number = document.getElementById('perso_number'+ip).value;
            perso_number_option = document.getElementById('perso_number_option'+ip).value;
            persoURL = persoURL+'&pn'+ip+'='+perso_number+'&pno'+ip+'='+perso_number_option
         }
      }
      if (document.getElementById('dDate')) {
         perso_date = document.getElementById('dDate').value;
         persoURL = persoURL+'&perso_date='+perso_date
      }
   }
   iURL = iURL+ persoURL

//alert(iURL);
//  try {
    BUY_http.open("GET", iURL, true);
//  } catch(err) { alert(err.description); return false; }

  lbLocation = '';  // just to make sure the lightBox doesnt try to call the server with location=
  if (!masterpart) { masterpart = part; }
  if (buyDisplay == 'lightbox') {
    showLightBox(e, bodyid);
  }

  startOneClick();
  pauseBuy(200);
  BUY_http.onreadystatechange = function(){ getBuyRes(); };
  BUY_http.setRequestHeader("Content-Type", "text/plain");
  BUY_http.send(null);
}
function buySomeAjax(x,part,e,rel_item, bodyid) {
  if (oneclick == 1) { return false; }
  startOneClick();
  if (!BUY_http) { BUY_http = getBuyAjax(); }
   // set rel_item to 'NOPRESEL' to stop the presel scan. --> Dont want related item buy to add presel item.
   if (x == '')  // '' means buy one - called from infopage and search via verysmall.tmp etc.
   { a = 1; rel_item='NOPRESEL'; }
   else
   { var el = document.getElementById('qty'+x); if (el) { a = el.value; } else { a = 1; } }
   b = parseInt(a);
   if (isNaN(b)) {
      showAjaxErrorBox(getTranslatedText('tt_buy_qty','ERROR - quantity entered - "[1]" - is INVALID',{'[1]':a}) );
      return false;
   }

  var xiURL = '';
  var yiURL = '';
  var fixer = new Date();
  var iURL = thisUrl+"BUYajax.asp?part="+part+"&quantity="+a+"&when="+fixer.getTime();
//  if (part==catalog) iURL = iURL+'&attr=catlg'

// now look for Gift Wrapping options
// try new way first
   if (window.gw_idx != undefined && document.getElementById('gift_wrap_f'+gw_idx)) {
     var gw = document.getElementById('gift_wrap_f'+gw_idx).value;
     if (gw == 'Yes') {
       var gw_msg = document.getElementById('gift_msg_f'+gw_idx).value;
       var gw_paper = document.getElementById('gift_paper_f'+gw_idx).value;
       iURL = iURL+'&gift_wrap='+ajaxFilter(gw_paper)+'&gift_msg='+ajaxFilter(gw_msg);
     }
   }
   if (window.gw_idx == undefined) {  // new data didnt exist
     if (document.getElementById('gift_wrapYes')) {
        gw = document.getElementById('gift_wrapYes').checked;
     }
     if (gw) {
        var gw_msg = document.getElementById('gift_msg').value;
        var el = document.getElementById('wrapSelect');
        for (var i = 0; i < el.options.length; i++) {
           if (el.options[i].selected) var gw_paper = el.options[i].value;
        }
        iURL = iURL+'&gift_wrap='+ajaxFilter(gw_paper)+'&gift_msg='+ajaxFilter(gw_msg);
     }
   }

// now look for Personalisation options
//alert('now look for Personalisation options');
   var perso;
   var perso_date;
   var perso_string;
   var perso_number;
   var perso_string_option;
   var perso_number_option;
   var persoURL = '';
   var ip;

// try new way first
   if (window.gw_idx != undefined && document.getElementById('perso_f'+gw_idx)) {
     var pe = document.getElementById('perso_f'+gw_idx).value;
     if (pe == 'Yes') {
       var data = document.getElementById('perso_data_f'+gw_idx).value;
       if (data == '') { data = 'var arrd=[]; var arrs=[]; var arrn=[]; '; }
       eval(data);
       var c = 0;
       for (var i in arrd) { if (c==0) { c++; persoURL += '&perso_date='+arrd[i]; } } // only one date allowed in asp
       var c = 0;
       for (var i in arrs) { c++; persoURL += '&ps'+c+'='+ajaxFilter(arrs[i])+'&pso'+c+'='+ajaxFilter(i); }
       persoURL += '&psc='+c;
       var c = 0;
       for (var i in arrn) { c++; persoURL += '&pn'+c+'='+ajaxFilter(arrn[i])+'&pno'+c+'='+ajaxFilter(i); }
       persoURL += '&pnc='+c;
     }
   }
   if (window.gw_idx == undefined) {  // new data didnt exist
     if (document.getElementById('personalisationYes')) {
        perso = document.getElementById('personalisationYes').checked;
     }
  //alert('perso='+perso);
     if (perso) {
        var perso_number_count = document.getElementById('perso_number_count').value;
        var perso_string_count = document.getElementById('perso_string_count').value;
        persoURL = persoURL+'&pnc='+perso_number_count+'&psc='+perso_string_count
  //alert('persoURL='+persoURL);
        for (ip = 1; ip <= perso_string_count; ip++) {
           if (document.getElementById('perso_string'+ip)) {
              perso_string = document.getElementById('perso_string'+ip).value;
              perso_string_option = document.getElementById('perso_string_option'+ip).value;
              persoURL = persoURL+'&ps'+ip+'='+perso_string+'&pso'+ip+'='+perso_string_option
           }
        }
        for (ip = 1; ip <= perso_number_count; ip++) {
           if (document.getElementById('perso_number'+ip)) {
              perso_number = document.getElementById('perso_number'+ip).value;
              perso_number_option = document.getElementById('perso_number_option'+ip).value;
              persoURL = persoURL+'&pn'+ip+'='+perso_number+'&pno'+ip+'='+perso_number_option
           }
        }
        if (document.getElementById('dDate')) {
           perso_date = document.getElementById('dDate').value;
           persoURL = persoURL+'&perso_date='+perso_date
        }
     }
   }
   iURL = iURL+ persoURL

  var xpart = part;
//  look for preSelectedRelatives and add to buying string if found
  if ( rel_item != 'NOPRESEL' && (document.getElementById('numPre'))) {
     numPre = parseInt(document.getElementById('numPre').value);
     xiURL = '&preParts=';
     for (var i=0; i<numPre; ++i) {
         j = i+1;
        if (document.getElementById('buyPre0'+j).checked!='') xiURL = xiURL+document.getElementById('partPre0'+j).value+'|';
     }
  }
//     } else alert('numPre not found');
  if (window.extracodes) {
    // showpartf method of passing the relatives ticked as 'extra' items to buy
    xiURL = '&preParts='+extracodes;
  }

  if (rel_item.substring(0,5) == 'popup')  {yiURL='&attr='+rel_item}
  if (rel_item == 'promo')  {iURL='&attr=promo'}
  if (rel_item.toUpperCase() == 'VOUCHER7') { iURL += '&attr='+voucherName+'~7~'+voucherLineNo+'&price=0'; }
  if (rel_item.toUpperCase() == 'VOUCHER6') { iURL += '&attr='+voucherName+'~6&price=0'; }

  iURL = iURL+xiURL+yiURL;
//alert(iURL);
  BUY_http.open("GET", iURL, true);
//alertX_BR(iURL);
  lbLocation = '';  // just to make sure the lightBox doesnt try to call the server with location=
  if (buyDisplay == 'lightbox') {
    showLightBox(e, bodyid);
  }
  if (buyDisplay == 'basket') {
    showBasketDropDown();
  }

  pauseBuy(200);
  BUY_http.onreadystatechange = function(){ getBuyRes(); };
  BUY_http.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2005 00:00:00 GMT");
  BUY_http.setRequestHeader("Content-Type", "text/plain");
  BUY_http.send(null);
  return false;  // make sure it doesnt submit the form
}

var buyLotsAlertX = null;
function buyLotsAjax(e,mes,noclose) {
  // quantities are held in main data array
  if (oneclick == 1) { return false; }
  startOneClick();
  if (!BUY_http) { BUY_http = getBuyAjax(); }

  var buystr = '';
  var buystr_c = -1;
  var buystr_qty = 0;
  for (var s=0; s<mx_data[0].sizes.length; s++) {
    for (var c=0; c<mx_data[0].colours.length; c++) {
      var part = mx_data[0].array[c][s];
      if (part.buyqty > 0) {
        buystr_c++;
        buystr_qty+=part.buyqty;
        buystr += '&Qty'+buystr_c+'='+part.buyqty+'&'+'Part'+buystr_c+'='+part.part.replace('+','%2B');
      }
    }
  }
  buystr_c++;
  buystr = 'mode=Lots&major='+mx_data[0].major.part.replace('+','%2B')+'&part_count='+buystr_c+buystr;

  var fixer = new Date();
  iURL = thisUrl+'buyAjax.asp?'+buystr+"&when="+fixer.getTime();

  if (mes) {
    mes = mes.replace(/\[xx[_ ]count\]/ig, buystr_qty);
    // make any alertConfig settings before calling this function
    buyLotsAlertX = alertX(mes);
  }

//alert(iURL);
//  try {
  BUY_http.open("GET", iURL, true);
//  } catch(err) { alert(err.description); return false; }

  lbLocation = '';  // just to make sure the lightBox doesnt try to call the server with location=
  if (buyDisplay == 'lightbox') {
    showLightBox('Lots', e, 'Lots', '');
  }

  startOneClick();
  pauseBuy(200);
  BUY_http.onreadystatechange = function(){ getBuyRes(); };
  BUY_http.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2005 00:00:00 GMT");
  BUY_http.setRequestHeader("Content-Type", "text/plain");
  BUY_http.send(null);
}


function ajaxFilter(s) {
  s = s.replace(/\n/g,'~');
  s = s.replace('#','');  // firefox and chrome will truncate the data at the point of a #
  s = s.replace('&','~and~');
  s = s.replace('�','~pnd~');
  return s;
}

function getBuyRes() {
  if (BUY_http.readyState == 4) {
    getBuyRes_core(BUY_http.readyState,BUY_http.status,BUY_http.responseText);
  } else {
    getBuyRes_core(BUY_http.readyState,-1,'');
  }
}

function getBuyRes_core(state, status, res) {
// _core function exists so I can call it from showpart_giftv.js
  if (state == 1) {
    res = 'Checking Stock........';
    if ( document.getElementById('lbTxtS'))
       { document.getElementById('lbTxtS').innerHTML = res;  }
  }

  if (state == 4) {
    if ( document.getElementById('lbTxtS')) { document.getElementById('lbTxtS').innerHTML = '';  }
//alertX_BR(res);
    vouc_data = '';
    // was the basket summary returned
    if (res.search('#-#-#-#-#') > -1) {
      aaa = res.split('#-#-#-#-#');
      vouc_data = aaa[1];
      res = aaa[0];
    }
    // is it an out of stock message
    // or it might now be the code for a colour / size selector if it was an express buy
    if (res.search('#####') > -1) {
      aaa = res.split('#####');
      if (aaa.length == 2) {
        // only two parts so it must be a stock message
        showAjaxErrorBox(aaa[1]);
      } else {
        // assume it is a showpart colour/size template
        lbError = true;
        lbLocation = ''; // stop it refreshing the page
        matrixAjaxRefresh = ajaxRefresh; // ditto
        ajaxRefresh = ''; // ditto
        clearTimeout(timer1);
        popupClose('');
        ajaxRefresh = matrixAjaxRefresh;

//alert(aaa[0]);
        // put the template in the work area div so I can create the matrix and find out its size before making it visible
        makeWorkArea(aaa[0]);  // aaa[0] is the template

        var idx = trim(aaa[1]);  /// aaa[1] is the part code = index into the json array
        // aaa[2] is the json data
//alertX_BR(aaa[2]);
        eval(aaa[2]); // if you get mx_data doesnt exist error here, you probably need to put "var mx_data = new Array();" into the main template
        if (window.showpartf) {
          drawMatrix(idx, 'matrixHead_qb','matrixBody_qb', 'iB_qb', getTranslatedText('tt_buy_matrix_header','Please click on a tick to choose a style/colour.'));
        } else {
          switch(mx_data[idx].template)
          {
            case 'MTX_LARGE' : func = createMatrixTable; break;
            case 'DDL_LARGE' : func = createMatrixTable; break;
            case 'LIST' : func = createMatrixTable; break;
            case 'MTX_LARGE_TABLE' : func = createMatrixTable; break;
            case 'MTX_LARGE_ALPHA' : func = createMatrixAlphabet; break;
            case 'MTX_LARGE_BRA' : func = createMatrixBra; break;
          }
          // matrix box is now on the workarea, process it there so the height is known before adding to the showX popup
          func(idx,'matrixHead_qb','matrixBody_qb', 'iB_qb', getTranslatedText('tt_buy_matrix_header','Please click on a tick to choose a style/colour.'));
        }
        if (window.showpartf) {
          // switched only so I can see if the header parameter works - sleepover dont want a header on the live site
          matrixPopupId = showX(getWorkArea(),'','',getTranslatedText('tt_buy_choose','Please choose your style')); // save the id so I can kill the popup if we are on the basket page
        } else {
          matrixPopupId = showX(getWorkArea(),'','',''); // save the id so I can kill the popup if we are on the basket page
        }
/*
      document.getElementById('ajaxBasket').innerHTML = aaa[0];
      if (aaa[1].substring(0,8) == 'showpart') {
        lbError = true;
        lbLocation = ''; // stop it refreshing the page
        ajaxRefresh = ''; // ditto
        clearTimeout(timer1);
        popupClose();

        showpart_part = aaa[1].substring(8,20)
        aaa[1] = 'Your Express Ordered item needs a colour/size to be selected<br><br>Please click the \'Select\' button to proceed to the selection page';
        promptX(aaa[1], showpartCloseFunc, 0, {'select':'Select', 'cancel':'Cancel'}, '');
*/
      }
    }
    else  // its not out of stock
    {
      err = '';
      if ((res.substr(0,2) == '<%') || (res == '')) // script code was returned
        { err='Y'; res = ''; }
      if (res.substr(0,60).match(/\<\!DOCT/i))  // html was returned
        { err='HTML'; }
      if (err == 'HTML') {
        showAjaxErrorBox(res);
      }
      else
      if (err == 'Y') {
        var el = document.getElementById('lbBody');
        if (el) {
          el.innerHTML = getTranslatedText('tt_buy_err','Error:') + '<br />'
                       + getTranslatedText('tt_buy_err_server','Server did not respond.') + '<br />'
                       + getTranslatedText('tt_buy_err_again','Please try again in a few seconds.');
        }
      }
      else {
        if (replaceBasket) {
          document.getElementById('ajaxBasket').innerHTML = res;
        }
        if ( document.getElementById('lbTxtT')) { document.getElementById('lbTxtT').innerHTML = getTranslatedText(verb2,verb2e); }
        if ( document.getElementById('lbTxtS') && document.getElementById('basket_itemcount')) {
          var q = document.getElementById('basket_itemcount').innerHTML;
          document.getElementById('lbTxtS').innerHTML = getTranslatedText('tt_buy_basket_plural','Your basket now holds [x] items',{ '[x]':q });
        }
        hideDiv('lbHead1');
        showDiv('lbHead2');


	//S2Tech_addItemToBasket('<elucid_part>','<elucid_title>','<elucid_price>','1');

        clearTimeout(timer1);
        if (buyDisplay == 'lightbox') {
          timer1 = window.setTimeout("timer1timer()",lightBoxTimeOut);
        }
        if (window.switchPromoButton) { switchPromoButton(); }
      }
//      safetoclose = true;
    }
    if (vouc_data) {
//alertX_BR(vouc_data);
      eval(vouc_data);
      updateMobileBasketTotal();
      if (!replaceBasket) {
        var el = document.getElementById('basket_itemcount');
        if (el) { el.innerHTML = vouc_data_qty; }
        var el = document.getElementById('basket_itemtotal');
        if (el) { el.innerHTML = vouc_data_curr+roundDP(vouc_data_tot,2); }
      }

      if (buyDisplay == 'basket') {
        showBasketDropDown();
        if ((lbLocation == '') && (ajaxRefresh != '')) {
//          alertX('<br />Updating Basket.<br /><br />Please wait...<br /><br />This box will close when the update is complete.<br /><br /><br />');
          location = ajaxRefresh;
        }
      }

      if (window.quickShopBasket) { quickShopBasket(); }
      if (window.afterGoodBuy) { afterGoodBuy(); }
    }
    stopOneClick();
    if (window.afterBuyAjax) { window.afterBuyAjax(); }

    if (buyLotsAlertX) {
      closeAlertX(buyLotsAlertX,null,true);
      buyLotsAlertX = null;
    }
  }
}

//function voucherBuy(ev, part,lineNo,voucher,vtype) {
function voucherBuy(part,lineNo,voucher,vtype) {
  if (!BUY_http) { BUY_http = getBuyAjax(); }

  voucherLineNo = lineNo;
  voucherName = voucher;
  voucherType = vtype;
  var fixer = new Date();
  // sitecodes should be provided by asp and in store_finder.tmp as a js var
  var iURL = thisUrl+"showPart_ajax.asp?part="+part+"&template_suffix=_voucher7&voucherType="+voucherType+"&when="+fixer.getTime();
//alert(iURL);
  BUY_http.open("GET", iURL, true);
  pauseBuy(100);
  BUY_http.onreadystatechange = function(){ show_voucher7_matrix(); };
  BUY_http.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2005 00:00:00 GMT");
  BUY_http.setRequestHeader("Content-Type", "text/plain");
  BUY_http.send(null);
  updateMobileBasketTotal();
}

var freeCount = 0;
function updateFreeCount() {
  if (!freeCount) return;
  var el = document.getElementById('matrix_buy_count');
  if (el) {
    var count = el.value-0;
    var el = document.getElementById('free_count');
    if (el) { el.innerHTML = freeCount; }
    var el = document.getElementById('free_count_s');
    if (el) { el.innerHTML = (count > 1 ? 's' : ''); }
    var el = document.getElementById('free_count_remaining');
    if (el) { el.innerHTML = count; }
  }
}

function show_voucher7_matrix() {
  showRes = false;
  if (BUY_http.readyState == 1) {
    var el = document.getElementById("matrixWaiting"+voucherLineNo);
    if (el) { el.style.display = 'block'; el.innerHTML = BUY_http.responseText; }
  }

  if (BUY_http.readyState == 4) {
    res = BUY_http.responseText;
    showRes = true;//res.length > 0;
    var el = document.getElementById("matrixWaiting"+voucherLineNo);
    if (el) { el.style.display = 'none'; }
  }

  if (showRes) {
    var el = document.getElementById('matrix_buy_count');
    if (el) {
      freeCount = el.value-0;
    }
    aaa = res.split('#####');

    // put the template in the work area div so I can create the matrix and find out its size before making it visible
    makeWorkArea(aaa[0]);  // aaa[0] is the template
//alert(res);
    var idx = trim(aaa[1]);  /// aaa[1] is the part code
    // aaa[2] is the json data
    eval(aaa[2]); // if you get mx_data doesnt exist error here, you need to put "var mx_data = new Array();" into the main template
//alertX_BR(aaa[2]);

    matrixAjaxRefresh = ajaxRefresh;
    var matrix = (mx_data[idx] && mx_data[idx].feat && (mx_data[idx].feat.length > 0));

    if (matrix) {
      if (window.showpartf) {
        drawMatrix(idx, 'matrixHead_qb','matrixBody_qb', 'iB_qb', 'Please click on a tick to choose a style/colour.');
      } else {
        switch(mx_data[idx].template)
        {
          case 'MTX_LARGE' : func = createMatrixTable; break;
          case 'LIST' : func = createMatrixTable; break;
          case 'MTX_LARGE_TABLE' : func = createMatrixTable; break;
          case 'MTX_LARGE_ALPHA' : func = createMatrixAlphabet; break;
          case 'MTX_LARGE_BRA' : func = createMatrixBra; break;
        }
        // matrix box is now on the workarea, process it there so the height is known before adding to the showX popup
        func(idx,'matrixHead_qb','matrixBody_qb', 'iB_qb', 'Please click on a tick to choose a style/colour.');
      }

      matrixPopupId = showX(getWorkArea()); // save the id so I can kill the popup if we are on the basket page
      updateFreeCount();
    }
  }
}

function refreshMiniBasket() {
  if (!BUY_http) { BUY_http = getBuyAjax(); }

  var fixer = new Date();
  iURL = thisUrl + 'mini_basket.asp'+"?when="+fixer.getTime();
  BUY_http.open("GET", iURL, true);
  pauseBuy(200);
  BUY_http.onreadystatechange = function(){ getRefreshRes(); };
  BUY_http.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2005 00:00:00 GMT");
  BUY_http.setRequestHeader("Content-Type", "text/plain");
  BUY_http.send(null);
}

function getRefreshRes() {
  if (BUY_http.readyState == 1) {
  }

  if (BUY_http.readyState == 4) {
    res = BUY_http.responseText;
//alertX_BR(res);
    res = res.split('#####');

    if (res[0]) {
      var el = document.getElementById('ajaxBasket');
      if (el) { el.innerHTML = res[0]; }
    }
    if (res.length > 1) {
      // element[1] will contain the basket summary in the vouc_data array
//alertX_BR(res[1]);
      
      eval(res[1]);
      if (window.quickShopBasket) { quickShopBasket(); }
      basketPopupAborted = false; //reset this incase its ios5
    }
    // update mobile basket total item qty after back button pressed
    updateMobileBasketTotal(0);
  }
}

function getBuyAjax() {
  var xhttp;
   try {   // The following "try" blocks get the XMLHTTP object for various browsers
      xhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
      try {
        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (e2) {
 // This block handles Mozilla/Firefox browsers...
     try {
       xhttp = new XMLHttpRequest();
     } catch (e3) {
       xhttp = false;
     }
      }
    }
  return xhttp; // Return the XMLHTTP object
}

function pauseBuy(millis) {
   var date = new Date();
   var curDate = null;
   do { curDate = new Date(); }
   while(curDate-date < millis);
}

// review moved to reviewget.tmp

// gift vouchers moved to loyalty.js

function twoDP(num){
  result = roundDP(num,2);
}

//-------------------------------------------------------------------------
//  general AJAX stuff
//-------------------------------------------------------------------------
var Gen_http = null;
var Gen_callback = null;
var Gen_alertX = null;
var Gen_alert = 'Please wait...';
var Gen_lastUrl = '';

function callAjax(url, callback) {
// callback needs to be function(state, status, res)
// where state = ajax readyState (1,4 etc), status = httpstatus (200 etc), res = ajax responseText
  Gen_http = getBuyAjax(); // refresh every time otherwise 2nd call might not work
  Gen_callback = callback;

  var fixer = new Date();
  var iURL = (url.indexOf(':') > 0 ? '' : thisUrl) + url + (url.indexOf('?') > 0 ? '&' : '?') + 'ajax=y&when='+fixer.getTime();

  if (Gen_lastUrl == iURL) { return; }  // we should never be calling the same url twice in the same second
  Gen_lastUrl = iURL;
  
  Gen_http.open("GET", iURL, true);
//  Gen_http.setRequestHeader("Content-Type", "text/plain; charset=ISO-8859-1");

//  pauseBuy(100);
  Gen_http.onreadystatechange = function(){ getGenRes(); };
  Gen_http.setRequestHeader("Content-Type", "text/plain; charset=UTF-8");
  Gen_http.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2005 00:00:00 GMT");
  Gen_http.send(null);
  return true;
}

function getGenRes() {
  if (Gen_callback) {
    if (Gen_http.readyState == 4 && Gen_http.status == 0 && Gen_http.responseText == '') {
      closeAlertX(Gen_alertX);
      if (IP.substr(0,12) == '192.168.128.' || IP.substr(0,10) == '80.193.99.' || IP.substr(0,12) == '213.160.116.') {
        Gen_callback(Gen_http.readyState, Gen_http.status, 'DH bad ajax response - response.redirect or http/https or third party domain issues.')
      } else {
        Gen_callback(Gen_http.readyState, Gen_http.status, 'Unable to reach the server.')
      }
    }
    else
    if (Gen_http.readyState == 4) {
      closeAlertX(Gen_alertX);
      Gen_callback(Gen_http.readyState, Gen_http.status, Gen_http.responseText)
    } else {
      Gen_callback(Gen_http.readyState, -1, '')
    }
  }
}

function loadFileAjax(filename, callback) {
//  if filename contains a . then it will be loaded as a txt file else its a tmp file
  return callAjax("content.asp?tmp="+filename,callback);
}

function postAjax(form, callback, extra, message) {
// you can add data-ajax-action="the real action" to the form attributes and then just call this
// or
// to make a normal form do an ajax post just make sure the form has an id and
// add data-ajax-callback="callbackfunc" to the form attributes
//   callback needs to be function(state, status, res)
//   where state = ajax readyState (1,4 etc), status = httpstatus (200 etc), res = ajax responseText
// Then submit the form or use common->funcion submitForm to do the submit
// common->initFrm called by checkData will rejig things so that this function gets called

  Gen_http = getBuyAjax(); // refresh every time otherwise 2nd call might not work
  extra = (extra ? extra+'&' : '');
//alert(form+' - '+callback);
  if (typeof(form) == 'string') { form = document.getElementById(form); }
  if (form) {
    var url = form.getAttribute('data-ajax-action');
    if (url) { url = url.replace(/dotext/ig,'.asp'); } else { url = form.action; }
    var params = formToParams(form);
    if (typeof(callback) == 'string') {
      Gen_callback = eval(callback);
    } else {
      Gen_callback = callback;
    }

//    Gen_http.form = form;
    if (message) { var mes = message; } else { var mes = getTranslatedText('tt_postajax_wait',Gen_alert); }
    if (trim(mes)) { Gen_alertX = alertX('<div id="basketHelp">'+mes+'</div>'); }
//alert(url);
    Gen_http.open('POST', url, true);
    Gen_http.onreadystatechange = function(){ getGenRes(); };
    Gen_http.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=UTF-8");
    Gen_http.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2005 00:00:00 GMT");
//alert('PA '+url+ ' ' + extra+params);
    Gen_http.send(extra+params);
    return;
  }
}

function formToParams(form) {
  if (typeof(form) == 'string') { form = document.getElementById(form); }
  var s = 'isajax=y&ajax=y'; var a = [];

  function fieldValue(el) {
      var n = el.name, t = el.type, tag = el.tagName.toLowerCase();

      if (!n || el.disabled || t == 'reset' || t == 'button' ||
          ((t == 'checkbox' || t == 'radio') && !el.checked) ||
          (t == 'submit' || t == 'image') ||
          (tag == 'select' && el.selectedIndex < 0)) {
        return { 'type':t, 'val':null };
      }
      return { 'type':t, 'val':el.value };
  }

  var i, j, n, v, el, els, max, jmax;
  els = form.elements;
  if (els) {
    for (i = 0; i < els.length; i++) {
      el = els[i];
      n = el.name;
      if (!n) { continue; }
      v = fieldValue(el);
      if (v.type == 'radio') {
        if (a[n] == null && (v.val !== null)) { a[n] = v.val; }
      }
      else if (v.type == 'checkbox') {
        if (v.val !== null) { a[n] = v.val; }
      }
      else if (v.val != null) {
        a[n] = v.val;
      }
    }
  }
  for (n in a) {
   s += (s != '' ? '&' : '') + encodeURIComponent(n) + '=' +encodeURIComponent(a[n]);
  }
  return s;
}
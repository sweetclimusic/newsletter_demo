var handleDesktopNavResizing = function() {
	/* desktop header animation */
	var desktopScrolling = false,
	desktopNav = $('.navbar'),
	previousTop = 0,
	currentTop = 0,
	scrollDelta = 10,
	scrollOffset = 150;

	$(window).on('scroll', function(){
		if( !desktopScrolling ) {
			desktopScrolling = true;
			(!window.requestAnimationFrame)
					? setTimeout(shrinkNav, 250)
					: requestAnimationFrame(shrinkNav);
		}
	});

	function shrinkNav() {
		currentTop = $(window).scrollTop();
		checkNavigation(currentTop);
		previousTop = currentTop;
		desktopScrolling = false;
	}

	function checkNavigation(currentTop) {
		if(previousTop - currentTop > scrollDelta) {
			// scrolling up...
			desktopNav.removeClass('navbar-hide');

			if(currentTop < scrollOffset) {
				desktopNav.removeClass('navbar-shrunk');
			}
		} else if(currentTop - previousTop > scrollDelta && currentTop > scrollOffset) {
			// scrolling down...
			desktopNav.addClass('navbar-shrunk');
			if(currentTop > 800) {
				desktopNav.addClass('navbar-hide');
			}
		}
	}
}

var handleMobileNavEvents = function() {
	var phoneHeader = document.getElementById('phoneHead');
	if(phoneHeader && window.is_mob) {
		phoneHeader.style.visibility = "visible !important";
	}

	/* mobile nav */
	_iosHeaderFix = {
		params: {
			$body : $(window),
			$pagePosition: 0,
			$positionProp: '',
			$mobNav: $('#phoneHead'),
			checkScrolled: false
		},
		positionFix: function (hasScrolled, position, nav) {
			_iosHeaderFix.params.$positionProp =  _iosHeaderFix.params.$mobNav.css('position');
			if (_iosHeaderFix.params.checkScrolled && (position === 0)) {
				nav.css('position', 'absolute');
				_iosHeaderFix.params.checkScrolled = false;
			}
			else if (position > 0) {
				nav.css('position', 'fixed');
				_iosHeaderFix.params.checkScrolled = true;
			}
		},
		watcher : function (body, nav) {
			body.scroll( function () {
					_iosHeaderFix.params.$pagePosition = _iosHeaderFix.params.$body.scrollTop();
					_iosHeaderFix.positionFix(_iosHeaderFix.params.checkScrolled, _iosHeaderFix.params.$pagePosition, nav);
			});
		}
	}

	_iosHeaderFix.watcher(_iosHeaderFix.params.$body, _iosHeaderFix.params.$mobNav);

	/************************************

	New Mobile Header jQuery

	*************************************/

	var hamburgerBtn = $('#hamburgerBtn'),
	hamburgerImg = hamburgerBtn.children('img'),
	hamburgerImgLink = 'https://www.joebrowns.co.uk/assets/images/mobileIcons/hamburger-icon',
	userBtn = $('#userBtn'),
	userImg = userBtn.children('img'),
	userImgLink = 'https://www.joebrowns.co.uk/assets/images/mobileIcons/profile-icon',
	loginDropdown = $('#loginDropdown'),
	searchBtn = $('#searchBtn'),
	searchImg = searchBtn.children('img'),
	searchImgLink = 'https://www.joebrowns.co.uk/assets/images/mobileIcons/search-icon',
	bagBtn = $('#bagBtn'),
	basket_itemcount_mobile = $('#basket_itemcount_mobile'),
	pmd_b_search = $('#pmd_b'),
	pmd_ax_menu = $('#pmd_ax'),
	menuOpen = false,
	userOpen = false,
	searchOpen = false,
	searchInput_mob = $('#pmd_b .main input.schInput'),
	searchInputBtn_mob = $('#pmd_b .main input.go'),
	uvUserInfo = window.universal_variable.user,
	loginBtnMob = $('ul#userDropdown li a.loginMob')[0],
	registerBtnMob = $('ul#userDropdown li a.loginMob')[1];


	searchInput_mob.on('keydown keyup keypress' , function(){

	var searchInputText = searchInput_mob.attr('value');

	if(searchInputText.length >= 3){
		searchInputBtn_mob.addClass('iconReady--search');
	} else {
		searchInputBtn_mob.removeClass('iconReady--search');
	}

	});

	// console.log(hamburgerImg[0]);

	// Hamburger Btn
	hamburgerBtn.click(function(e){
	e.preventDefault();
	if(searchOpen){
		searchImg[0].src = searchImgLink + '.png'
		pmd_b_search.removeClass('pmbOpen');
		searchOpen = false;
	}
	if(userOpen){
		userImg[0].src = userImgLink + '.png'
		loginDropdown.removeClass('dropDownLogin');
		userOpen = false;
	}

	if(!menuOpen){
	pmd_ax_menu.addClass('slideMenu');
		hamburgerImg[0].src = hamburgerImgLink + '_highlighted.png' ;
		menuOpen = true;
	}else{
		hamburgerImg[0].src = hamburgerImgLink + '.png'
		pmd_ax_menu.removeClass('slideMenu');
		menuOpen = false;
	}
	});

	// User Btn
	userBtn.click(function(e){
	e.preventDefault();

	if (searchOpen) {
		searchImg[0].src = searchImgLink + '.png'
		pmd_b_search.removeClass('pmbOpen');
		searchOpen = false;
	}
	if (menuOpen) {
	hamburgerImg[0].src = hamburgerImgLink + '.png'
	pmd_ax_menu.removeClass('slideMenu');

	menuOpen = false;
	}

	// console.log(uvUserInfo.name)

	if (!userOpen) {
	loginDropdown.addClass('dropDownLogin');
	userImg[0].src = userImgLink + '_highlighted.png';
	userOpen = true;

	if(uvUserInfo.email.length > 0 && uvUserInfo.name.length > 0 && uvUserInfo.user_id.length > 0) {
		loginBtnMob.href = 'https://www.joebrowns.co.uk/' + 'login?submit=Logout';
		loginBtnMob.innerHTML = 'Logout';
		registerBtnMob.href = 'https://www.joebrowns.co.uk/' + 'myaccount';
		registerBtnMob.innerHTML = 'My Account';
	} else {
		loginBtnMob.href = 'https://www.joebrowns.co.uk/' + 'login';
		loginBtnMob.innerHTML = 'Login';
		registerBtnMob.href = 'https://www.joebrowns.co.uk/' + 'register';
		registerBtnMob.innerHTML = 'Register';
	}

	} else {
	userImg[0].src = userImgLink + '.png';
	loginDropdown.removeClass('dropDownLogin');
	userOpen = false;
	}
	});

	// Search Btn
	searchBtn.click(function(e){
	e.preventDefault();

	if (userOpen) {
	userImg[0].src = userImgLink + '.png';
	loginDropdown.removeClass('dropDownLogin');
	userOpen = false;
	}
	if (menuOpen) {
	hamburgerImg[0].src = hamburgerImgLink + '.png';
	pmd_ax_menu.removeClass('slideMenu');

	menuOpen = false;
	}

	if (!searchOpen) {
	pmd_b_search.addClass('pmbOpen');
		searchImg[0].src = searchImgLink + '_highlighted.png' ;
	searchInput_mob.focus();
		searchOpen = true;
	} else {
	searchImg[0].src = searchImgLink + '.png';
	pmd_b_search.removeClass('pmbOpen');
	searchInput_mob.blur();
	searchOpen = false;
	}
	});

	// Bag Btn
	function updateBasketIcon() {
		var basketItemNum = basket_itemcount_mobile.text();

		if(basketItemNum == '0'){
			basket_itemcount_mobile .removeClass('notZero');
		} else {
			basket_itemcount_mobile.addClass('notZero');
		}
	}

	basket_itemcount_mobile.bind('DOMNodeInserted ', function() {
		updateBasketIcon();
	});

	updateBasketIcon();
}

$(document).ready(function() {
	if (!window.is_mob) {
		handleDesktopNavResizing();
	} else {
		handleMobileNavEvents();
	}
});

$(window).resize(function() {
	var windowWidth = window.innerWidth;

	if (!window.is_mob && windowWidth > 720) {
		handleDesktopNavResizing();
	} else {
		handleMobileNavEvents();
	}
});
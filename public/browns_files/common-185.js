// activate debug using ?showdebug=true - asp will show links for source and log
if (window.IP == undefined) { IP = ''; }
if (IP.substr(0,12) == '192.168.128.' || IP == '95.138.201.43' || (window.location.href.toLowerCase().indexOf('showdebug=true') > -1)) {
  window.onerror = function(message, url, lineNumber) {
    alert('ERROR:\n'+message+'\n'+url+'\nLine '+lineNumber);
    return false;   // did i handle it ?
  };
}
var log = '', canshowdebug = false;
function addLog(s) { if (canshowdebug) { log += s + '\r\n'; } }
function showLog() { alertX_BR(log); }
function showSource() { alertX_BR(document.head.innerHTML + document.body.innerHTML); }

/* these can be overridden in custom.js or homehtml or templates */
var EUCookieSet = false;
var cookiesOn = false;
var co_name = 'Dev Site';
var showPagerArrows = true;
if (!window.backmode) { var backmode="normal"; }
if (!window.thisUrl) { var thisUrl = ""; }
if (!window.delim404) { var delim404 = "/" }
var pp_id = 'pp';
var sp_id = 'sp';
var pu_id = 'pu';
var search_id = 'product';
var focusTo = '';
var pageIsLoaded = false;
var animate = true;
var customSelectDoAll = false; // override in custom.js if needed - false = only do selects with data-custom="" attribute
var customCheckBoxRadioDoAll = false; // override in custom.js if needed - false = only do checkboxes with data-custom="" attribute
var is_mob = (window.orientation != undefined) || navigator.userAgent.match(/mobile/i) != null;  // windows 8 phone hasnt got orientation
var customBeforeUnload = null; // override in custom.js to point to a function() if you want it
var customResize = null; // override in custom.js to point to a function() if you want it
var showResizeTip = false;
var isResponsiveSite = false;  // override in custom.js if needed - (always true on portal)
var responsiveMultiplier = 1;
var responsiveWidth = 1024;
var responsiveStyle = 'designed';
var responsiveOverride = '';
var responsiveLimits = [];
var loadmark = 0;
var dragging = false;   // set to true when scroller is finger dragging - use in onclick of contents to stop accidental clicks
var doUnloadPopup = true;
var checkData_immediate = false;  // true = error check immediately e.g. onblur, false = error check only after clicking submit
var checkData_errorPlacement = 'popup'; // choices are "popup" or "below" - this dictates position of validation error messages

// JB-467 Variables for text validation
var def_textbox_regex = "^[a-zA-Z0-9 ?$*!(),.?@:;?']+$"
var def_email_regex = "^\u0000-\u00fe"

function isIE6() {
  var av = navigator.appVersion.toLowerCase();
  var i = av.indexOf('msie 6');
  return (i < 50 && i > -1);
}
function isIE() {
  var av = navigator.appVersion.toLowerCase();
  var i = av.indexOf('msie');
  return (i < 50 && i > -1);
}
function hide(el,swapto) {
  if (typeof(el) == 'string') { el = document.getElementById(el); }
  if (el && el.style) { el.style.display = 'none'; }
  if (swapto) {
    if (typeof(swapto) == 'string') { var elto = document.getElementById(swapto); } else { var elto = swapto; }
    if (elto) {elto.style.display = 'block'; }
  }
}

function hideSelects(ele) {
  if (isIE6()) {
    var eles = document.all.tags("SELECT");
    for(var m = 0; m < eles.length; ++m) {
      if (eles[m] != ele) {  eles[m].style.visibility='hidden'; }
    }
  }
}

function showSelects() {
  if (isIE6()) {
    for(var s = 0; s < document.all.tags("SELECT").length; ++s) {
        document.all.tags("SELECT")[s].style.visibility='visible';
    }
  }
}

function showDiv(id) { var el = document.getElementById(id);  if (el) { el.style.display = 'block'; return el; }}
function hideDiv(id) { var el = document.getElementById(id);  if (el) { el.style.display = 'none'; return el; }}

var meFocussing = false;
function setFocusTo(ele) {
  if (isIE()) meFocussing = true;   // IE (surprise) calls dataman onblur which shows error messages too early
  if (ele) { focusel = ele; } else { focusel = window.focusTo; }
  if (focusel) {
    var el = document.getElementById(focusel);
    if (el) { el.focus(); try { el.select(); } catch(err) {}; }
  }
//  meFocussing = false;
}

function mouseX(evt) {
  if (evt.touches) return evt.touches[0].pageX;
  if (evt.pageX) return evt.pageX;
  else if (evt.clientX)
     return evt.clientX + (document.documentElement.scrollLeft ?
     document.documentElement.scrollLeft :
     document.body.scrollLeft);
  else return null;
}

function mouseY(evt) {
  if (evt.touches) return evt.touches[0].pageY;
  if (evt.pageY) return evt.pageY;
  else if (evt.clientY)
     return evt.clientY + (document.documentElement.scrollTop ?
     document.documentElement.scrollTop :
     document.body.scrollTop);
  else return null;
}

function getBounds(ele) {
  var res = { x:0, y:0, w:0, h:0 }
  if (typeof(ele) == 'string') { var el = document.getElementById(ele); } else { var el = ele; }
  if (el) {
    res.x = el.offsetLeft; res.y = el.offsetTop;  res.h = el.offsetHeight;  res.w = el.offsetWidth;
    while((el=el.offsetParent) != null) {
      if (el.style) {
        res.x += el.offsetLeft ? (el.offsetLeft+(el.clientLeft ? el.clientLeft : 0)-(el.scrollLeft ? el.scrollLeft : 0)) : 0;
        res.y += el.offsetTop ? (el.offsetTop+(el.clientTop ? el.clientTop : 0)) : 0;
        var pos = cssStyle(el,'position');
        if (pos == 'fixed') {  // just add the document scroll position and bail out
          res.x += (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft);
          res.y += (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);
          break;
        }
      }
    }
  }
  return res;
}
//--------------------------------------
function xOnPage(x,w,centerIt,ele,fixed) {
  var sw = screenWidth();
  var wmin = screenLeft();
//showDebug(sw+' '+w+' '+x+' ');
  if (isIE6()) { fixed = false; }
  if (fixed) { wmin = 0; }

  var sw = sw + wmin;
  if (centerIt) { x = (Math.round((sw - wmin - w) / 2)) + wmin; }
//  if ((sw > -1)) {	if (x<0) { x=0; }		if ((x + w) > sw - 10) {	x = sw - w - 10;	} }
  if ((sw > -1)) {	if (x<0) { x=0; }		if ((x + w) > sw) {	x = sw - w;	} }
		if (x < wmin) { x = wmin; }
  if (ele && w > sw) { ele.style.width = sw-10+'px'; }
//showDebug(x,true);
		return x;
}
function yOnPage(y,h,centerIt,ele,fixed) { // pass ele to make sure max height is not exceeded
  var sh = screenHeight();
  var hmin = screenTop();

  if (isIE6()) { fixed = false; }
  if (fixed) { hmin = 0; }

  var sh = sh + hmin;
  if (centerIt) { y = (Math.round((sh - hmin - h) / 2)) + hmin; }
  if ((sh > -1)) {	if (y<0) { y=0; }		if ((y + h) > sh - 10) {	y = sh - h - 10 ; } }
		if (y < hmin) { y = hmin; }
  if (ele && h > sh) { ele.style.height = sh-6+'px'; }
 	return y;
}
//--------------------------------------
// popup.... are routines to use if you want a mouse listener and optional hotspot.
// use: call popupInit(closefunc, hotid) at the start of your popup show function and pass it your hide/close function.
// hotId is the id of the click-to-close area - pass '' for anywhere on the page or !divid to close anywhere EXCEPT divid.
// If you want to manually close the popup, call popupClose('','myresult') NOT your hide/close function
// 'myresult' will be put in global var popupResult so you can control what to do in the hide/close function
var popupSafeToClose = false;
var popupOMD = null;
var popupDiv = '';
var popupCloseFunc = null;
var popupResult = '';
var popupClosing = false;
var popupPending = null;
var popupClosedTime = 0;
//
function popupInit(closeFunc, hotId) {
  if (popupClosing) { return false;  }
  popupPending = null;
  // tidy up any open popup
  if (popupSafeToClose) { popupRemoveListener(); if (popupCloseFunc) { popupCloseFunc();} popupCloseFunc = null;  popupSafeToClose = false; }
  // prep the listener for the new popup
  popupCloseFunc = closeFunc;  popupDiv = hotId;  popupAttachListener();  hideSelects();  popupSafeToClose = true;
  return true;
}

function popupClose(e,closeResult) {
  if (popupSafeToClose) {
    if (e == undefined) var e = window.event; // make sure IE has the event in e
    if (e && e.button && e.button == 2) return;
    if (popupDiv.match('!')) { var pd = popupDiv.substring(1,100); var inpd = false; } else { var pd = popupDiv; var inpd = true; }
//document.title = document.title + 'z'+(e=='')+'z';
//alert(e+' '+pd+' '+popupInDiv(e,pd,inpd));
    if (pd == '' || (pd != '' && (e == '' || e == 'timer' || (popupInDiv(e,pd,inpd))))) {
      popupClosing = true;
      popupResult = (closeResult==undefined ? null : closeResult);
//      popupResult = ((closeResult == '') || (closeResult==null) || (closeResult==undefined) ? '' : closeResult);
      popupRemoveListener();
      popupSafeToClose = false;
      if (popupCloseFunc) { popupCloseFunc();}
      popupCloseFunc = null;
      showSelects();
      popupClosing = false;
      popupClosedTime = Date.parse(new Date());

      if (popupPending) { popupPending(); popupPending = null; }
    }
  }
}

function popupJustClosed() {
  var now = Date.parse(new Date());
//  alert(now+' '+popupClosedTime);
  var i = Math.abs(now - popupClosedTime);
//  alert(i);
  popupClosedTime = 0;
  return (i < 100);
}

function popupAttachListener() {
   	if (document.layers) {	document.captureEvents(Event.MOUSEDOWN);	}
   	popupOMD = document.onmousedown;
   	if (popupOMD != null) {	document.onmousedown = function(event) { popupOMD(event); popupClose(event); } }
    else { document.onmousedown = function(event) { popupClose(event); } }
}

function popupRemoveListener() {
   	if (popupOMD != null) {	document.onmousedown = popupOMD; } else { document.onmousedown = null; }
}

function popupInDiv(e, divID, testbool, t) {  // pass t if you need to override the top most element - eg mouseout = e.toElement
  if (!e) var e = window.event; // make sure IE has the event in e
  if (!divID) { divID = popupDiv; }
		if (e && e.srcElement) {
			if (t == undefined) { t = e.srcElement; }
			while (t && t.parentElement != null) {
				if (t.id && t.id==divID) {	return testbool; }
				t = t.parentElement;
			}
		}
		else if (e) {
 			if (t == undefined) {
      if (e.target) { var t = e.target; }
      else
      if (e.targetTouches) { var t = e.targetTouches[0].target; }
    }

//   try { if (t.id) {} } // will get error here if firefox and input box is clicked on - mar 2010
//   catch(errr) { alert('err: '+errr); return !testbool;}

   try {
   		while (t && (t.parentNode != null) && (t.parentNode != undefined) && (t.attributes)) {
   				if (t.id==divID) { return testbool; }
 	  			t = t.parentNode;
 	  	}
  	}
   catch(err)
   { alert('error: '+err.description);}
		}
		return !testbool;
}
// end popup listener
//--------------------------------------
function getXCookie( check_name ) {
   var a_all_cookies = document.cookie.split( ';' );
   var a_temp_cookie = '';
   var cookie_name = '';
   var cookie_value = '';
   var b_cookie_found = false; // set boolean t/f default f

   for ( i = 0; i < a_all_cookies.length; i++ )
   {
      a_temp_cookie = a_all_cookies[i].split( '=' );
      cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');  // trim
      if ( cookie_name == check_name )
      {
         b_cookie_found = true;
         if ( a_temp_cookie.length > 1 ) { cookie_value = unescape( a_temp_cookie[1].replace(/^\s+|\s+$/g, '') ); }
//alert(document.cookie);
         return cookie_value;
         break;
      }
      a_temp_cookie = null;
      cookie_name = '';
   }
   if ( !b_cookie_found ) { return ''; }
}
//--------------------------------------
// expires is in days
function setXCookie( name, value, expires, path, domain, secure ) {
  if (!EUCookieSet && window.EUCookieMode == 'full') { return; }

  var today = new Date();
  today.setTime( today.getTime() );
  if ( expires ) { expires = expires * 1000 * 60 * 60 * 24; }
  var expires_date = new Date( today.getTime() + (expires) );

  var s = name + "=" +escape( value ) +
    ( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" ) + //expires.toGMTString()
    ( ( path ) ? ";path=" + path : "" ) +
    ( ( domain ) ? ";domain=" + domain : "" ) +
    ( ( secure ) ? ";secure" : "" );
  document.cookie = s;
}
//--------------------------------------
function deleteCookie( name, path, domain ) {
  if ( getXCookie( name ) ) document.cookie = name + "=" +
     ( ( path ) ? ";path=" + path : "") +
     ( ( domain ) ? ";domain=" + domain : "" ) +
     ";expires=Thu, 01-Jan-1990 00:00:01 GMT";
}
//----
function cssStyle(el, property) {
  var elo = el;
  if (typeof(el) == 'string') { el = document.getElementById(el); }
  if (!el) { alert('cssStyle error: element '+elo+' property '+property+' does not exist'); return null; }
  var res = el.style[property];
  if (!res) {
    if (el.currentStyle && el.currentStyle[property]) {
      res = el.currentStyle[property];
    } else {
      // this method uses 'background-image' not the passed format of 'backgroundImage'
      prop = "";
      for (var i=0; i < property.length; i++) {
        if (property.charAt(i) == property.charAt(i).toUpperCase()) {
          prop += '-' + property.charAt(i).toLowerCase(); }
        else {
          prop += property.charAt(i); }
      }
      try {
      res = getComputedStyle(el,'').getPropertyValue(prop);
      } catch(err) {}
    }
  }
  if (!res) { res = ''; }
  if (typeof(res) == 'string') { res = res.replace('px',''); }
  return res;
}
//------------
var isModal = true;
var shadowOffset = 8;
var shadowBox = null;

function showShadowBox( id, mode ) {
  var offs = { 't':shadowOffset, 'l':shadowOffset, 'w':0, 'h':0 }
  if (mode == 'center') { offs = { 't':-3, 'l':-3, 'w':6, 'h':6 } }

  var div = createDynamicPopup('shadowBox');  div.style.zIndex = '0';
  if (div) {
    if (typeof(id) == 'string') { var el = document.getElementById(id); } else {var el = id; }
    var i = cssStyle(el,'zIndex');
    if (i-0 > 0) { div.style.zIndex = (i-1) + ''; }
    var fixed = cssStyle(el,'position');
    if (fixed = 'fixed') {
      hmin = 0; wmin = 0;
    } else {
      var hmin = (document.documentElement.scrollTop ? document.documentElement.scrollTop : (window.pageYOffset ? window.pageYOffset : 0));
      if (typeof(hmin) != 'number' || hmin < 0) { hmin = 0; }
      var wmin = (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : (window.pageXOffset ? window.pageXOffset : 0));
      if (typeof(wmin) != 'number' || wmin < 0) { wmin = 0; }
    }
    div.style.top = (el.offsetTop+offs.t+hmin) + 'px';
    div.style.left = (el.offsetLeft+offs.l+wmin) + 'px';
    div.style.height = (el.offsetHeight + offs.h) + 'px';
    div.style.width = (el.offsetWidth + offs.w) + 'px';
    div.style.display = 'block';
    shadowBox = div;      // keep a global reference to it
    el.shadow = shadowBox;  // so drag can find the div
  }
}
function hideShadowBox( ) {
  var div = document.getElementById('shadowBox');
  if (div) {
    div.style.display = 'none';
  }
}

function screenWidth() {
  var sw = [];  for (var i=0; i<=3; i++) { sw[i] = -1; }
  if (window.innerWidth) {sw[1] = window.innerWidth;}
  if (document.body && document.body.clientWidth) {sw[2] = document.body.clientWidth;}
  if (document.documentElement && document.documentElement.clientWidth) {sw[3] = document.documentElement.clientWidth;}

  // innerwidth tends to lie on apple when they feel like messing it up so just use the smallest
  sw[0] = 9999;  for (var i=0; i<=3; i++) { if (sw[i] > 0 && sw[i] < sw[0]) { sw[0] = sw[i]; } }
  if (sw[0] == 9999) { sw[0] = 980; }
addLog('screenWidth='+sw[0]);
  return sw[0];
}

function modalCoverScreenWidth(){ // not used
  var sww;
  var screenWW = document.getElementById('pageBody');
  sww = screenWW.clientWidth;
  return sww;
}

function screenLeft() {
  var wmin = (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : (window.pageXOffset ? window.pageXOffset : 0));
  if (typeof(wmin) != 'number' || wmin < 0) { wmin = 0; }
  return wmin;
}
function screenHeight() {
  var sh = -1;
  if (sh < 1 && window.innerHeight){sh = window.innerHeight;}
  if (sh < 1 && document.documentElement && document.documentElement.clientHeight){sh = document.documentElement.clientHeight;}
  if (sh < 1 && document.body) {sh = document.body.clientHeight;}
  return sh;
}
function screenTop() {
  var hmin = (document.documentElement.scrollTop ? document.documentElement.scrollTop : (window.pageYOffset ? window.pageYOffset : 0));
  if (typeof(hmin) != 'number' || hmin < 0) { hmin = 0; }
  return hmin;
}
function screenSize() {
addLog('screensize caller='+whoCalledMe());
  var sw = screenWidth();
  var wmin = screenLeft();
  var sh = screenHeight();
  var hmin = screenTop();
  return { 'x':wmin, 'y':hmin, 'w':sw, 'h':sh };
}

function documentSize() {
//alert(window.innerHeight+'|'+window.scrollMaxY+'|'+document.body.scrollHeight+'|'+document.body.offsetHeight);
  if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
    var y = document.body.scrollHeight;
    var x = document.body.scrollWidth;
  } else { // works in Explorer 6 Strict, Mozilla (not FF) and Safari
    var y = document.body.offsetHeight;
    var x = document.body.offsetWidth;
  }
  if (y == 0) {
    var y = document.body.scrollHeight || document.documentElement.scrollHeight;
    var x = document.body.scrollWidth || document.documentElement.scrollWidth;
  }
  if (y == 0 && window.innerHeight != undefined && window.scrollMaxY != undefined) {// Firefox
    var y = window.innerHeight + window.scrollMaxY;
    var x = window.innerWidth + window.scrollMaxX;
  }
  if (y == 0)  y = 5000;
  return { 'width':x, 'height':y };
}

function autoSetFrameSize(el, err_height) {  // el should be 'this' in the onload of the iframe
  try {
    var c=(el.contentWindow || el.contentDocument);
    if (c.document)c=c.document;
    var h = c.body.scrollHeight || c.documentElement.scrollHeight;
  } catch(err) { var h = err_height ? err_height : '200';}
  el.style.height = h + 'px';
}

var coverCount = 0;
var coverZIndex = new Array();
var lastCoverZindex = 0;
var coverElements = [];
function showModalCover( clickToClose, refresh ) {
  var div = document.getElementById('modalCover');
  if (!div) {
    if (refresh == true) { return; }
    div = createDynamicPopup('modalCover');  setZIndex(div);  lastCoverZindex = globZindex;
  }
  if (div) {
    if (refresh == true && div.style.display != 'block') { return; }
    div.style.left = '0px';
    div.style.top = '0px';

    var s = screenSize();
    var d = documentSize();
    var h = s.h;
    if (h < d.height) { h = d.height; }
    var w = s.w;
    if (w < d.width) { w = d.width; }
//    w = w-17; // DSDS allow for scrollbar, stops horizontal scroll showing when modal cover shown - dh didnt work well on ipad
    div.style.width = w+'px';
    div.style.height = (h)+'px';
    div.style.display = 'block';
    if (refresh != true) {
      coverCount++;
      coverZIndex[coverCount] = lastCoverZindex;
    }
/*
  needs a little more code to stop it disabling the elements on the div popping up above the cover.
    coverElements.length = 0;
    var els = document.getElementsByTagName('INPUT');
    for (var i=0; i<els.length; i++) {
      if (els[i].disabled != true) {
        coverElements[coverElements.length] = {count:coverCount, el:els[i] };
        els[i].disabled = true;
      }
    }
    var els = document.getElementsByTagName('SELECT');
    for (var i=0; i<els.length; i++) {
      if (els[i].disabled != true) {
        coverElements[coverElements.length] = {count:coverCount, el:els[i] };
        els[i].disabled = true;
      }
    }
    var els = document.getElementsByTagName('A');
    for (var i=0; i<els.length; i++) {
      if (els[i].disabled != true) {
        coverElements[coverElements.length] = {count:coverCount, el:els[i] };
        els[i].disabled = true;
      }
    }
*/
  }
  if (clickToClose && refresh != true) { div.onclick = clickToClose; }
}

function hideModalCover( ) {
  var div = document.getElementById('modalCover');
  if (div) {
    coverCount--;
/*
    for (var i in coverElements) {
      if (coverElements[i].count == coverCount) {
        coverElements[i].el.disabled = false;
      }
    }
    for (var i=coverElements.length-1; i=0; i--) {
      if (coverElements[i].count == coverCount) {
        coverElements.splice(i,1);
      }
    }
*/

    if (coverCount <= 0)
      { div.style.display = 'none'; }
    else
      { div.style.zIndex = coverZIndex[coverCount]}
  }
}
var globZindex = 7000;
function setZIndex(el) {
  if (typeof(el) == 'string') { el = document.getElementById(el); }
  globZindex = globZindex + 10;
  if (el) { el.style.zIndex = globZindex; }
}

function createDynamicPopup(id, modal, style, parent ) {
  if (modal) { var cover = createDynamicPopup('modalCover');  setZIndex(cover); lastCoverZindex = globZindex; }

//  var div = document.getElementById(id);
// 	if (div) { div.parentNode.removeChild(div);

  var div = document.getElementById(id);
  if (!div) {
    var div = document.createElement('div');
    div.setAttribute('id', id);
    div.setAttribute('name', id);
    if (style) { div.setAttribute('className',style); }
    if (style) { div.setAttribute('class',style); }
    if (typeof(parent) == 'string' && parent) { parent = document.getElementById(parent); }
    if (!parent || parent == undefined) { parent = document.body }
    try { parent.appendChild(div); } catch(err) { }
  }
  div.style.display = 'none';
  setZIndex(div)
  return div;
}
function returnPressed(event) { // onkeydown event
  event = event || window.event;
  var key = (event.keyCode ? event.keyCode : event.which);
  return key == 13;
}
function integerOnly(event) {  // onkeydown event
  event = event || window.event;
  var key = (event.keyCode ? event.keyCode : event.which);
  var shift = (event.shiftKey ? event.shiftKey : event.shiftKey);
  shift = shift || false;
// 0..9 with no shift, 0-9 keypad, return-13, tab-9, del-46, backspace-8, left-37, right-39, home-36, end-35
  if (key == 0 || key == 229)
    { return true }  // certain incarnations of android cant be bothered returning keyup/down codes and always give 0 or 229
  else if ((((key >= 48) && (key <= 57)) && shift==false) || ((key >= 96) && (key <= 105)) || (key == 13) || (key == 8) || (key == 9) || (key == 46) || (key == 39) || (key == 37) || (key == 35) || (key == 36))
    { return true; }
  else
    { event.returnValue = false; return false; }
}

function integerOnlyKU(event, ele) {  // onkeyup event
  var t = ele.value.replace(/[^0-9\.]/g,'');
  if (is_mob) { ele.value = ''; ele.value = t; }  // frig around ipad bug - ele.value does not contain non 0..9 but ipad shows them in the box
  else
  if (t != ele.value) { ele.value = t; }
}

function wborder(el) {
// returns the padding and border width for el's style
//  var x = (cssStyle(el,'borderLeftWidth')-0) + (cssStyle(el,'borderRightWidth')-0) +
//          (cssStyle(el,'paddingLeft')-0) + (cssStyle(el,'paddingRight')-0);

  function perc(res) {
    if (res.indexOf('%') > 0) {
      var wid = el.offsetWidth;
      res = res.replace('%','');
      res = Math.round(wid*res/100);
    }
    return res-0;
  }

  var bl = cssStyle(el,'borderLeftWidth');
  var br = cssStyle(el,'borderRightWidth');
  var pl = cssStyle(el,'paddingLeft');
  var pr = cssStyle(el,'paddingRight');
  var x = perc(bl)+perc(br)+perc(pl)+perc(pr);

  return isNaN(x) ? 0 : x;
}
function hborder(el) {
// returns the padding and border width for el's style
//  var x = (cssStyle(el,'borderTopWidth')-0) + (cssStyle(el,'borderBottomWidth')-0) +
//          (cssStyle(el,'paddingTop')-0) + (cssStyle(el,'paddingBottom')-0);

  function perc(res) {
    if (res.indexOf('%') > 0) {
      var wid = el.offsetWidth;
      res = res.replace('%','');
      res = Math.round(wid*res/100);
    }
    return res-0;
  }

  var bl = cssStyle(el,'borderTopWidth');
  var br = cssStyle(el,'borderBottomWidth');
  var pl = cssStyle(el,'paddingTop');
  var pr = cssStyle(el,'paddingBottom');
  var x = perc(bl)+perc(br)+perc(pl)+perc(pr);

  return isNaN(x) ? 0 : x;
}
function fullPath(path) {
  return (path.match('://') ? path : thisUrl+path);
}
function transitionStop(el) {
  if (el && el.trans) { el.trans.finish(); }
}
function transition(from, tooo, param) {
  var i_trans = new c_trans(from, tooo, param);
}
//var ctc = 0;
function c_trans(from, tooo, param) {
//from:  { id:id, el:element, x:left, y:top, w:width, h:height }
//tooo:  { id:id, el:element, x:left, y:top, w:width, h:height }
//param  { classname:'abc', speed:##, movediv:'Y', grow:'Y', vanish:'Y', hide:'Y', onend:funcname } NOTE ALL FIELD NAMES ARE LOWERCASE
// speed is approx time the transition should take in millisecs, note IE is a bit slow
// vanish:'Y' overrides the tooo size/position so it ends up as 10x10 box in the center of tooo.id
// grow='Y' auto calculates the from position to be a 10x10 box in the center of tooo.id
//     - specify x,y,w,h in from to override the 10x10 centred start position
// movediv:'Y' will move the actual div passed as 'from'
// hide:'Y' will hide the div at the end and move it back to its original position and size Only works if movediv is used
// onend : pass it a function to call when the transition is complete - format: function(div) where div will be the 'from' div
// focus : will set focus to the passed id after the onend has been called
// note, pass a div or el as from if you need to access the div in the onend function

   c_trans.prototype.die = function() {
     clearTimeout(this.timer);
     if (this.from.el && this.from.el.trans) { this.from.el.trans = null; }

     if (!this.param.movediv) {
       this.div.style.display = 'none';
       var elp = this.div.parentNode;
       if (elp) { elp.removeChild(this.div); }
     }
     else if (this.param.hide) {
       this.div.style.display = 'none';
//       ctc++;
//document.title = 'xxposf'+ctc+' '+this.posf.x+' '+this.posf.y+' '+this.posf.w+' '+this.posf.h
       if (this.inited && this.posf.w > 0) {
         // put it back in its original position
         this.div.style.top = this.posf.y + 'px';
         this.div.style.left = this.posf.x + 'px';
         if (this.from.w != this.to.w) { this.div.style.width = this.posf.w + 'px'; }
         if (this.from.h != this.to.h) { this.div.style.height = this.posf.h + 'px'; }
       }
     }
   }

   c_trans.prototype.finish = function() {
     if (this.from.el && this.from.el.offsetLeft < -1500) { this.from.el.style.left = this.post.x + 'px'; }
     this.die();
     if (this.param.overflow) { this.from.el.style.overflow = this.param.overflow; }
     if (this.param.onend) { this.param.onend(this.from.el); }
     if (this.param.focus) { var el = document.getElementById(this.param.focus); if (el) { el.focus(); } }
     this.destroy;
   }

   c_trans.prototype.onTimer = function() {
     try {
       this.count++;
       if (this.count <= this.maxCount) {
         if (this.loops[this.loop] == 'wh' || this.loops[this.loop] == 'w') {
           this.pos.x = this.pos.x + this.xstep;
           this.pos.w = this.pos.w - this.wstep;
         }
         // padding can make this go -ive -  bail if it does
         if (this.pos.w < 0) { this.pos.w = 0; this.count = this.maxCount+2; }

         if (this.loops[this.loop] == 'wh' || this.loops[this.loop] == 'h') {
           this.pos.y = this.pos.y + this.ystep;
           this.pos.h = this.pos.h - this.hstep;
         }
         if (this.pos.h < 0) { this.pos.h = 0; this.count = this.maxCount+2; }

         if (this.loops[this.loop] == 'fi') {
           this.opacity += this.ostep;
           this.div.style.opacity = this.opacity / 100;
           this.div.style.filter = "alpha(opacity=" + Math.round(this.opacity) + ")";
         }

         this.div.style.top = Math.round(this.pos.y) + 'px';
         this.div.style.left= Math.round(this.pos.x) + 'px';
         if (this.from.w != this.to.w) { this.div.style.width = Math.round(this.pos.w) + 'px'; }
         if (this.from.h != this.to.h) { this.div.style.height = Math.round(this.pos.h) + 'px'; }
       }
//document.title = 'pos '+this.pos.x+' '+this.pos.y+' '+this.pos.w+' '+this.pos.h
       if (this.count <= this.maxCount+1) {
         if (this.count > this.maxCount && this.loop < this.loops.length-1)
         {
           this.loop++;  this.count = 0;
         }
         this.timer = setTimeout( ( function ( obj ) { return function () { obj.onTimer( ); }; } )( this ), tinc );
       } else {
  //     this.div.style.zIndex = 100;
  //alert('trans width='+this.div.offsetWidth+' '+this.div.pos.w+' '+this.div.opos.w+' '+this.wstep);
         this.finish();
       }
     }
     catch(err)
     {}
   }

   var copyof = function(x) { var ret = {}; for (var i in x) { ret[i] = x[i]; } return ret; }
   var exists = function(val) { return val != undefined; }

   c_trans.prototype.init = function() {
     this.posf = { x:0, y:0, w:10, h:10 }
     this.post = { x:100, y:100, w:10, h:10 }

     if (exists(this.from.el)) { this.elf = this.from.el; this.posf = getBounds(this.elf); }
     if (exists(this.from.id)) { this.elf = document.getElementById(this.from.id); this.posf = getBounds(this.elf); this.from.el = this.elf; }
     if (exists(this.from.x)) { this.posf.x = this.from.x; }
     if (exists(this.from.y)) { this.posf.y = this.from.y; }
     if (exists(this.from.w)) { this.posf.w = this.from.w; }
     if (exists(this.from.h)) { this.posf.h = this.from.h; }
     if (this.from.el) { this.from.el.trans = this; }
//for (var xxx in posf) { alert(xxx + ' = ' + posf[xxx]) }

     if (exists(this.to.el)) { this.elt = this.to.el; this.post = getBounds(this.elt); }
     if (exists(this.to.id)) { this.elt = document.getElementById(this.to.id); this.post = getBounds(this.elt); this.to.el = this.elt; }
     if (exists(this.to.x)) { this.post.x = this.to.x; }
     if (exists(this.to.y)) { this.post.y = this.to.y; }
     if (exists(this.to.w)) { this.post.w = this.to.w; }
     if (exists(this.to.h)) { this.post.h = this.to.h; }
     if (this.to.el) { this.to.el.trans = this; }
//for (var xxx in post) { alert(xxx + ' = ' + post[xxx]) }

     var hmin = (document.documentElement.scrollTop ? document.documentElement.scrollTop : (window.pageYOffset ? window.pageYOffset : 0));
     if (typeof(hmin) != 'number' || hmin < 0) { hmin = 0; }
     var fixed = (cssStyle(this.from.el,'position') == 'fixed' && !isIE6());
//alert(fixed);
     if (fixed) { this.post.y += hmin;  this.posf.y += hmin; }

     if (this.posf && this.post) {
       if (this.param.movediv) {
         this.div = this.elf;
       } else {
         this.div = createDynamicPopup('trans_div'+c_trans.divno);
         c_trans.divno++;
         if (this.from.el) { this.from.el.style.left = '-10000px'; }
       }
// line below commented to make lolo faq popOpen work with rolldown()
//       this.div.style.position = 'absolute';
       if (!this.param.movediv) {
         if (this.param.classname)
           { this.div.className = this.param.classname; }
         else
           { this.div.style.background = 'white'; this.div.style.border = '1px solid red'; }
       }

       if (this.loops[0] == 'fi') {
         this.div.style.opacity = this.opacity / 100;
         this.div.style.filter = "alpha(opacity=" + this.opacity + ")";
       }

       var wfix = wborder(this.div);
       this.posf.w -= (wfix);
       this.post.w -= (wfix);

       var hfix = hborder(this.div);
       this.posf.h -= (hfix);
       this.post.h -= (hfix);

       if (this.posf.w < 0) { this.posf.w = 0; }
       if (this.posf.h < 0) { this.posf.h = 0; }

       if (this.param.grow) {
         // default is grow from 10x10 centered on 'to' but can be overridden by setting the 'from' x,y,w,h
         if (this.from.w == undefined) { this.posf.w = 10; }
         if (this.from.x == undefined) { this.posf.x = Math.round(this.post.x + (this.post.w/2)-(this.posf.w/2)); }
         if (this.from.h == undefined) { this.posf.h = 10; }
         if (this.from.y == undefined) { this.posf.y = Math.round(this.post.y + (this.post.h/2)-(this.posf.h/2)); }
       }
//for (var xxx in posf) { alert(xxx + ' = ' + posf[xxx]) }

       if (this.param.vanish) {  // shrink to 10x10 centered on 'to'
         this.post.x = Math.round(this.post.x + (this.post.w/2)-5); this.post.w = 10;
         this.post.y = Math.round(this.post.y + (this.post.h/2)-5); this.post.h = 10;
       }

//ctc++;
//document.title = 'posf'+ctc+' '+this.posf.x+' '+this.posf.y+' '+this.posf.w+' '+this.posf.h+' post '+this.post.x+' '+this.post.y+' '+this.post.w+' '+this.post.h+' '+this.div.style.display;
       // something was causing a call with posf bits = 0 when the minibasket and lightbox were showing which screws up future calls
       if (this.posf.x == 0 && this.posf.y == 0 && this.posf.w == 0 && this.posf.h == 0) {
         this.finish();
         return;
       }

       this.div.style.top = this.posf.y + 'px';
       this.div.style.left = this.posf.x + 'px';
       if (this.from.w != this.to.w) { this.div.style.width = this.posf.w + 'px'; }
       if (this.from.h != this.to.h) { this.div.style.height = this.posf.h + 'px'; }
       this.pos = copyof(this.posf);
       this.div.style.display = 'block';
       this.xdiff = (this.posf.x - this.post.x);
       this.ydiff = (this.posf.y - this.post.y);
       this.wdiff = (this.posf.w - this.post.w);
       this.hdiff = (this.posf.h - this.post.h);

       this.maxCount = Math.round(this.param.speed / ((tinc + 100) * this.loops.length)) + 1;
       this.xstep = -this.xdiff / this.maxCount;
       this.ystep = -this.ydiff / this.maxCount;
       this.wstep = this.wdiff / this.maxCount;
       this.hstep = this.hdiff / this.maxCount;

       if (this.loops[0] == 'fi') {
         tinc = 50;
         this.maxCount = this.param.speed / (tinc + 20);
         this.ostep = 80 / this.maxCount;
       }
//alert('posf '+posf.x+' '+posf.y+' '+posf.w+' '+posf.h+' post '+post.x+' '+post.y+' '+post.w+' '+post.h+' '+this.div.style.display);
//document.title = 'posf '+this.posf.x+' '+this.posf.y+' '+this.posf.w+' '+this.posf.h+' post '+this.post.x+' '+this.post.y+' '+this.post.w+' '+this.post.h+' '+this.div.style.display;

       this.count = 0;
       this.inited = true;
       this.timer = setTimeout( ( function ( obj ) { return function () { obj.onTimer( ); }; } )( this ), 1 );
     }
   }

  var tinc = 33;
  this.inited = false;
  this.from = from;
  this.to = tooo;
  this.param = param;
//for (var xxx in param) { alert(xxx + ' = ' + param[xxx]) }

  this.loops = new Array;
  this.loop = 0;
  if (this.param.action == undefined)
    { this.loops[0] = 'wh'; }
  else
    { this.loops = this.param.action.split(',');
  }
  this.opacity = 20;
  if (this.param.speed == undefined) { this.param.speed = 20; }
  if (this.param.movediv == undefined) { this.param.movediv = ''; }
  if (this.param.classname == undefined) { this.param.classname = ''; }
  if (this.param.onend == undefined) { this.param.onend = ''; }
  if (this.param.hide == undefined) { this.param.hide = ''; }
  if (this.param.focus == undefined) { this.param.focus = ''; }
  this.init();
}
c_trans.divno = 0;
// ---------
function rolldown(div, endfunc, onendoverflow) {
  if (typeof(div) == 'string') { div = document.getElementById(div); }
  if (div) {
    var pos = cssStyle(div,'position');
    if (pos == 'absolute') {
      var left = div.trueleft ? div.trueleft-0 : div.offsetLeft-0;
      var from = { el:div, x:left, y:div.offsetTop, w:div.offsetWidth, h:1 };
      var too = { x:left, y:div.offsetTop, w:div.offsetWidth, h:div.offsetHeight };
    } else {
      var from = { el:div, x:0, y:0, w:div.offsetWidth, h:1 };
      var too = { x:0, y:0, w:div.offsetWidth, h:div.offsetHeight };
    }
    div.style.display = 'none';
    div.style.overflow = 'hidden';
    if (onendoverflow == undefined) { onendoverflow = ''; }  // = 'visible' or 'auto' to override the setting after the rolldown
    transition( from, too, { speed:500, action:'h', movediv:'Y', onend:endfunc, overflow:onendoverflow } );
  }
}
function rollup(div, endfunc) {
  // div must be fixed size, set in css
  if (typeof(div) == 'string') { div = document.getElementById(div); }
  if (div) {
    var pos = cssStyle(div,'position');
    if (pos == 'absolute') {
      var from = { el:div, x:div.offsetLeft, y:div.offsetTop, w:div.offsetWidth, h:div.offsetHeight };
      var too = { x:div.offsetLeft, y:div.offsetTop, w:div.offsetWidth, h:0 };
    } else {
      var from = { el:div, x:0, y:0, w:div.offsetWidth, h:div.offsetHeight };
      var too = { x:0, y:0, w:div.offsetWidth, h:0 };
    }
    transition( from, too, { speed:500, action:'h', movediv:'Y', hide:'Y', onend:endfunc } );
  }
}
// -----------
function createImg(id, owner) {
  var img = document.createElement('img');
  img.setAttribute('id', id);
  img.setAttribute('name', id);
  img.style.display = 'none';
  img.style.zIndex = 1;
  owner.appendChild(img);
  return img;
}
function createDiv(id, owner, visible, abs, classs) {
  var div = document.createElement('div');
  div.setAttribute('id', id);
  div.setAttribute('name', id);
  if (classs) { div.className = classs; }
  div.style.display = (visible === true ? 'block' : 'none' );
  if (abs === true) { div.style.position = 'absolute'; div.style.top = '0px'; div.style.left = '0px' } else { div.style.position = 'relative'; }
  div.style.zIndex = 1;
  owner.appendChild(div);
  return div;
}
// imageTrans('homeImage','homeImg','http://www.elucidportal.co.uk/topgrade/products/images/data/',sequ, {'effect':'slideleft', 'interval':'3000', 'speed':'500', 'leftbtn':'homeleft', 'rightbtn':'homeright' } )
function c_imageTrans(div_id,imgclass,path,imagedefs,options) {
  c_imageTrans.prototype.transTimer = function() {
    try {
      var done = false;
      if (this.images[this.image].width > 0) { //ie the image has loaded
        if (this.firsttrans) {
          this.imgs[1].setAttribute('imgno',this.image);
          this.imgs[1].alt = this.imagealts[this.image];
          this.imgs[1].title = this.imagealts[this.image];
          if (this.imagemaps[this.image]) { this.imgs[1].style.cursor = ''; } else { this.imgs[1].style.cursor = (this.imagelinks[this.image] ? 'pointer' : 'default'); }
          this.showOverlays(1,this.image);
          this.firsttrans = false;
        }

        // modified for responsive sites
        if (this.step == 0) {
          this.imgs[1].style.width = this.imgs[0].offsetWidth+'px';
          this.imgs[1].style.height = this.imgs[0].offsetHeight+'px';
        }

        if (this.effect == 'swap') {
          this.divs[0].style.display = 'none';
          this.imgs[0].src = fullPath(this.path)+this.imagenames[this.image];
          this.divs[0].style.display = 'block';
          done = true;
        }
        else if (this.effect == 'fadein') {
          if (this.step == 0) {
            this.imgs[1].src = fullPath(this.path)+this.imagenames[this.image];
            this.limit = 100;
            this.pos = 0;
            this.jumpInc = this.imgs[0].offsetWidth * (1000 / this.speed) * (this.timerInc / 1000) / 25;
            if (this.jumpInc < 2) { this.jumpInc = 2; }
//            this.divs[1].style.marginTop = '-'+this.imgs[0].offsetHeight+'px';
//            this.divs[1].style.marginLeft = '0px';
            this.divs[1].style.top = '0px';
            this.divs[1].style.left = '0px';
            this.imgs[1].style.opacity = this.pos / 100;
            this.imgs[1].style.filter = "alpha(opacity=" + Math.round(this.pos) + ")";
            this.imgs[1].style.display = 'block';
            this.imgs[0].style.opacity = this.limit - (this.pos / this.limit);
            this.imgs[0].style.filter = "alpha(opacity=" + (this.limit- Math.round(this.pos)) + ")";
            this.divs[1].style.display = 'block';
          }
          this.step++;
          this.pos = this.pos + this.jumpInc;
          if (this.pos > this.limit) { this.pos = this.limit; }
          this.imgs[1].style.opacity = this.pos / this.limit;
          this.imgs[1].style.filter = "alpha(opacity=" + Math.round(this.pos) + ")";
          this.imgs[0].style.opacity = this.limit - (this.pos / this.limit);
          this.imgs[0].style.filter = "alpha(opacity=" + (this.limit- Math.round(this.pos)) + ")";
          if (this.pos >= this.limit) {
            done = true;
          }
        }
        else if (this.effect == 'slideleft') {
          if (this.step == 0) {
            this.imgs[1].src = fullPath(this.path)+this.imagenames[this.image];
            this.limit = 0;
            this.pos = this.imgs[0].offsetWidth;
            this.imgs[0].offsetWidth * this.timerInc / this.speed;
            this.jumpInc = this.imgs[0].offsetWidth * (1000 / this.speed) * (this.timerInc / 1000);
            if (this.jumpInc < 1) { this.jumpInc = 1; }
            this.divs[1].style.top = '0px';
            this.divs[1].style.left = this.pos+'px';
            this.divs[1].style.display = 'block';
          }
          this.step++;
          this.pos = this.pos - this.jumpInc;
          if (this.pos < this.limit) { this.pos = this.limit; }
          this.divs[1].style.left = this.pos+'px';
          if (this.pos <= this.limit) {
            done = true;
          }
        }
        else if (this.effect == 'slideright') {
          if (this.step == 0) {
            this.imgs[1].src = fullPath(this.path)+this.imagenames[this.image];
            this.limit = 0;
            this.pos = -this.imgs[0].offsetWidth;
            this.jumpInc = this.imgs[0].offsetWidth * (1000 / this.speed) * (this.timerInc / 1000);
            if (this.jumpInc < 1) { this.jumpInc = 1; }
            this.divs[1].style.top = '0px';
            this.divs[1].style.left = this.pos+'px';
            this.divs[1].style.display = 'block';
          }
          this.step++;
          this.pos = this.pos + this.jumpInc;
          if (this.pos > this.limit) { this.pos = this.limit; }
          this.divs[1].style.left = this.pos+'px';
          if (this.pos >= this.limit) {
            done = true;
          }
        }
        else if (this.effect == 'stretchleft') {
          if (this.step == 0) {
            this.imgs[1].src = fullPath(this.path)+this.imagenames[this.image];
            this.limit = this.imgs[0].offsetWidth;;
            this.pos = 0;
            this.jumpInc = this.imgs[0].offsetWidth * (1000 / this.speed) * (this.timerInc / 1000);
            if (this.jumpInc < 1) { this.jumpInc = 1; }
            this.divs[1].style.top = '0px';
            this.divs[1].style.left = this.limit+'px';
            this.imgs[1].style.width = this.pos+'px';
            this.divs[1].style.display = 'block';
          }
          this.step++;
          this.pos = this.pos + this.jumpInc;
          if (this.pos > this.limit) { this.pos = this.limit; }
          this.imgs[1].style.width = this.pos+'px';
          this.divs[1].style.left = this.limit-this.pos+'px';

          if (this.imgs[1].offsetWidth >= this.limit) {
            done = true;
          }
        }
        else if (this.effect == 'stretchright') {
          if (this.step == 0) {
            this.imgs[1].src = fullPath(this.path)+this.imagenames[this.image];
            this.limit = this.imgs[0].offsetWidth;;
            this.pos = 0;
            this.jumpInc = this.imgs[0].offsetWidth * (1000 / this.speed) * (this.timerInc / 1000);
            if (this.jumpInc < 1) { this.jumpInc = 1; }
            this.divs[1].style.top = '0px';
            this.divs[1].style.left = '0px';
            this.imgs[1].style.width = this.pos+'px';
            this.divs[1].style.width = this.pos+'px';
            this.divs[1].style.display = 'block';
          }
          this.step++;
          this.pos = this.pos + this.jumpInc;
          if (this.pos > this.limit) { this.pos = this.limit; }
          this.imgs[1].style.width = this.pos+'px';
          this.divs[1].style.width = this.pos+'px';

          if (this.imgs[1].offsetWidth >= this.limit) {
            done = true;
          }
        }
        if (done) {
            this.imgs[0].src = fullPath(this.path)+this.imagenames[this.image];
            try { this.imgs[1].style.width = cssStyle(this.imgs[0],'width')+'px'; } catch(err) { }
            this.imgs[0].setAttribute('imgno',this.image);
            this.imgs[0].alt = this.imagealts[this.image];
            this.imgs[0].title = this.imagealts[this.image];
            if (this.imagemaps[this.image]) { this.imgs[0].style.cursor = ''; } else { this.imgs[0].style.cursor = (this.imagelinks[this.image] ? 'pointer' : 'default'); }
            this.showOverlays(0, this.image);
            this.divs[0].style.display = 'block';
            this.imgs[0].style.opacity = 100;
            this.imgs[0].style.filter = "alpha(opacity=100)";
        }
      }
      else
      {
        done = true;  // image not ready so start timer for next transition
      }
    }
    catch(err)
    { alert(err.description); return;}

    if (this.aborted) { return; }

    if (done) {

      for (var i=0; i < this.imagenames.length; i++) {
        var el = document.getElementById(this.div_id+'_blob_'+i);
        if (el) {
          if (i == this.image) { el.className = this.blobon; } else { el.className = this.bloboff; }
        }
      }

      this.timer = setTimeout( ( function ( obj ) { return function () { if (obj) { obj.endTrans( ); } }; } )( this ), 10 );
    } else {
      this.timer = setTimeout( ( function ( obj ) { return function () { if (obj) { obj.transTimer( ); } }; } )( this ), 1000 / this.timerInc );
    }
  }

  c_imageTrans.prototype.endTrans = function() {
    this.divs[1].style.display = 'none';
    if (this.autoscroll) {
      this.timer = setTimeout( ( function ( obj ) { return function () { obj.onTimer( ); }; } )( this ), this.interval );
    }
  }

  c_imageTrans.prototype.trans = function() {
    this.firsttrans = true;
    this.timer = setTimeout( ( function ( obj ) { return function () { obj.transTimer( ); }; } )( this ), 10 );
  }

  c_imageTrans.prototype.onTimer = function() {
    try {
      if (this.aborted) { return; }

      if (pageIsLoaded ) {
        if (!this.loaded) { // time to load all the images
          for (var i=0; i < this.imagenames.length; i++ ) {
            this.images[i] = new Image();
            this.images[i].src = fullPath(this.path)+this.imagenames[i];
          }
          this.loaded = true;
          if (this.image < 0) { this.image += this.images.length; } //fix
          this.showOverlays(0,this.image);
          if (!animate || this.imagenames.length < 2) { return; }
          this.timer = setTimeout( ( function ( obj ) { return function () { obj.onTimer( ); }; } )( this ), this.interval );
        }
        else
        {
          if (this.aborted) { return; }
          if (this.mouseIsOver) {
            this.timer = setTimeout( ( function ( obj ) { return function () { obj.onTimer( ); }; } )( this ), this.initinterval );
          } else {
            // dont inc image if mouse is over
            this.image++; if (this.image >= this.images.length) { this.image = 0; }
            this.effectno++; if (this.effectno >= this.effects.length) { this.effectno = 0; }
            this.effect = this.effects[this.effectno];
            this.step = 0;
            this.trans();
          }
        }
      }
      else if (this.autoscroll)
      {
        this.timer = setTimeout( ( function ( obj ) { return function () { obj.onTimer( ); }; } )( this ), this.initinterval );
      }
    }
    catch(err)
    { alert(err.description); return;}
  }

  c_imageTrans.prototype.doPrev = function() {
    clearTimeout(this.timer);
    this.imgs[0].src = fullPath(this.path)+this.imagenames[this.image];
    // move back two and then call onTimer that moves to the next one
    this.autoscroll = false;
    this.aborted = false; // let it work if button was clicked
    this.image -= 2; if (this.image < 0 && this.images.length > 0) { this.image += this.images.length; } //fix
    if (this.effects.length == 1) {
      if (this.effects[0].indexOf('slide') > -1) { this.effects[0] = 'slideright'; }
      if (this.effects[0].indexOf('stretch') > -1) { this.effects[0] = 'stretchright'; }
      if (this.effects[0].indexOf('fadein') > -1) { this.speed = 50; } // make it fast when clicked
    } else {
      this.effectno -= 2; if (this.effectno < 0) { this.effectno += this.effects.length; }
    }
    this.onTimer();
  }
  c_imageTrans.prototype.doNext = function() {
    clearTimeout(this.timer);
    this.imgs[0].src = fullPath(this.path)+this.imagenames[this.image];
    this.autoscroll = false;
    this.aborted = false; // let it work if button was clicked
    if (this.effects.length == 1) {
      if (this.effects[0].indexOf('slide') > -1) { this.effects[0] = 'slideleft'; }
      if (this.effects[0].indexOf('stretch') > -1) { this.effects[0] = 'stretchleft'; }
      if (this.effects[0].indexOf('fadein') > -1) { this.speed = 50; } // make it fast when clicked
    }
    this.onTimer();
  }

  c_imageTrans.prototype.doPage = function(blob) {
    var page = blob.pageno;
    if (this.image == page) return;
    clearTimeout(this.timer);
    this.imgs[0].src = fullPath(this.path)+this.imagenames[this.image];
    this.autoscroll = false;
    this.aborted = false; // let it work if button was clicked
    if (this.effects.length == 1) {
      if (this.effects[0].indexOf('slide') > -1) {
        if (page < this.image) { this.effects[0] = 'slideleft'; } else { this.effects[0] = 'slideright'; }
      }
      if (this.effects[0].indexOf('stretch') > -1) {
        if (page < this.image) { this.effects[0] = 'stretchleft'; } else { this.effects[0] = 'stretchright'; }
      }
      if (this.effects[0].indexOf('fadein') > -1) { this.speed = 50; } // make it fast when clicked
    }
    this.image = page-1; if (this.image < 0 && this.images.length > 0) { this.image += this.images.length; } //fix
    this.onTimer();
  }

  c_imageTrans.prototype.abort0 = function() {
    var i = this.imgs[0].getAttribute('imgno');
    if (this.imagelinks[i]) {
      this.aborted = true;
      this.imgs[1].style.display = 'none';
      this.imgs[0].style.opacity = 100;
      this.imgs[0].style.filter = "alpha(opacity=100)";
      if (this.imageclicks[i]) { eval(this.imageclicks[i]); }
      if (this.target[i]) {
        var win = window.open(this.imagelinks[i],'trans','');
        try { win.focus(); } catch(err) {}
      } else {
        window.location = this.imagelinks[i];
      }
    }
  }

  c_imageTrans.prototype.abort1 = function() {
    var i = this.imgs[1].getAttribute('imgno');
    if (this.imagelinks[i]) {
      this.aborted = true;
      this.imgs[1].style.display = 'none';
      this.imgs[0].style.opacity = 100;
      this.imgs[0].style.filter = "alpha(opacity=100)";
      if (this.imageclicks[i]) { eval(this.imageclicks[i]); }
      if (this.target[i]) {
        var win = window.open(this.imagelinks[i],'trans','');
        try { win.focus(); } catch(err) {}
      } else {
        window.location = this.imagelinks[i];
      }
    }
  }

  c_imageTrans.prototype.stop = function() {
    this.aborted = true;
    clearTimeout(this.timer);
    this.step = 0;
    this.imgs[0].style.opacity = 100;
    this.imgs[0].style.filter = "alpha(opacity=100)";
    this.imgs[1].style.display = 'none';
  }

  c_imageTrans.prototype.restart = function() {
    clearTimeout(this.timer);
    this.step = 0;
    this.autoscroll = true;
    this.timer = setTimeout( ( function ( obj ) { return function () { obj.onTimer( ); }; } )( this ), this.interval );
    this.aborted = false;
  }

  c_imageTrans.prototype.mouseover = function() {
    this.mouseIsOver = true;
  }
  c_imageTrans.prototype.mouseout = function() {
    this.mouseIsOver = false;
  }

  c_imageTrans.prototype.showOverlays = function(img, image) {
    if (this.imagecountdown[image]) {
      this.cdts[img].style.top = (this.imagecountdown[image].y*responsiveMultiplier)+'px';
      this.cdts[img].style.left = (this.imagecountdown[image].x*responsiveMultiplier)+'px';
      this.cdts[img].style.display = 'block';
      this.cdts[img].style.visibility = 'visible';
      var txt = hp_decode('trans_'+this.imageidxs[image]+'_timer_end',this.imagecountdown[image].end);
      var clas = (this.imagecountdown[image].clas ? 'cd_c_'+this.imagecountdown[image].clas.replace('#','') : '');
      var countdown1 = { div_id:this.cdts[img].id, end_time:this.imagecountdown[image].t,
                         end_msg:txt, classroot:clas+' countdown' }
      showCountdown(countdown1);
    } else {
      this.cdts[img].style.visibility = 'hidden';
      this.cdts[img].innerHTML = '';
    }

    for (var j=0; j<4; j++) {
      if (this.imageoverlays[image][j]) {
//        this.ovls[img][j].style.top = (this.imageoverlays[image][j].y*responsiveMultiplier)+'px';
//        this.ovls[img][j].style.left = (this.imageoverlays[image][j].x*responsiveMultiplier)+'px';
        this.ovls[img][j].style.top = (this.imageoverlays[image][j].y)+'px';
        this.ovls[img][j].style.left = (this.imageoverlays[image][j].x)+'px';
// zoom works on ipad and changes top,left and font size
        this.ovls[img][j].style.zoom = (100*responsiveMultiplier)+'%';
        if (window.hp_decode) {
          var txt = hp_decode('trans_'+this.imageidxs[image]+'_text_'+j,this.imageoverlays[image][j].text);
        } else {
          var txt = getTranslatedText('trans_'+this.imageidxs[image]+'_text_'+j,this.imageoverlays[image][j].text)
        }
        this.ovls[img][j].innerHTML = txt;
        this.ovls[img][j].style.display = 'block';
        this.ovls[img][j].style.visibility = 'visible';
        this.ovls[img][j].style.cursor = (this.imagelinks[image] ? 'pointer' : 'default');
        this.ovls[img][j].onclick = ( function ( obj ) { return function () { obj.abort0( ); }; } )( this )
      } else {
        this.ovls[img][j].style.visibility = 'hidden';
        this.ovls[img][j].innerHTML = '';
      }
    }

    if (this.imagemaps[image]) {
      this.imgs[img].usemap = '#'+this.imagemaps[image];
    } else {
      this.imgs[img].usemap = null;
    }
  }

  c_imageTrans.prototype.init = function() {
    var div = document.getElementById(div_id);
    if (div) {
      div.style.overflow = 'hidden';
      this.divs[0] = createDiv(div_id+'div1',div, true);
      this.divs[1] = createDiv(div_id+'div2',div, false, true);
      this.imgs[0] = createImg(div_id+'img1',this.divs[0]);
      this.imgs[1] = createImg(div_id+'img2',this.divs[1]);
      this.imgs[0].className = this.imgclass;
      this.imgs[1].className = this.imgclass;
////      this.imgs[0].style.position = 'absolute';
//      this.imgs[1].style.position = 'absolute';
      div.style.position = 'relative';
      this.imgs[0].src = fullPath(this.path)+this.imagenames[0];
      this.imgs[0].style.display = 'block';
      this.imgs[1].style.display = 'block';
      this.imgs[1].cssFloat = 'left';
      this.divs[1].style.overflow = 'hidden';
      this.imgs[0].onmouseover = ( function ( obj ) { return function () { obj.mouseover( ); }; } )( this )
      this.imgs[0].onmouseout = ( function ( obj ) { return function () { obj.mouseout( ); }; } )( this )
      this.imgs[1].onmouseover = ( function ( obj ) { return function () { obj.mouseover( ); }; } )( this )
      this.imgs[1].onmouseout = ( function ( obj ) { return function () { obj.mouseout( ); }; } )( this )
      this.imgs[0].onclick = ( function ( obj ) { return function () { obj.abort0( ); }; } )( this )
      this.imgs[1].onclick = ( function ( obj ) { return function () { obj.abort1( ); }; } )( this )
      this.imgs[0].oncontextmenu = ( function ( obj ) { return function () { obj.stop( ); }; } )( this )
      this.imgs[1].oncontextmenu = ( function ( obj ) { return function () { obj.stop( ); }; } )( this )
      this.imgs[0].alt = this.imagealts[0];
      this.imgs[0].title = this.imagealts[0];
      this.imgs[0].setAttribute('imgno','0');
      if (this.imagemaps[0]) { this.imgs[0].style.cursor = ''; } else { this.imgs[0].style.cursor = (this.imagelinks[0] ? 'pointer' : 'default'); }

      this.cdts[0] = createDiv(div_id+'cd1',this.divs[0],false,true,'countdown_holder');
      this.cdts[1] = createDiv(div_id+'cd2',this.divs[1],false,true,'countdown_holder');

      for (var j=0; j<4; j++) {  // max of 4 per image
        this.ovls[0][j] = createDiv(div_id+'ovl1_'+j,this.divs[0],false,true,'overlay_holder');
        this.ovls[1][j] = createDiv(div_id+'ovl2_'+j,this.divs[1],false,true,'overlay_holder');
      }
    }
  }

  checkAnimate();
  var def_effects = new Array ( 'fadein', 'swap', 'slideleft', 'slideright', 'stretchleft', 'stretchright' );
  this.aborted = false;
  this.autoscroll = true;
  this.divs = new Array;  // two divs to hold the imgs
  this.imgs = new Array;  // two html img elements for the switch
  this.cdts = new Array;   // two divs for the countdown timers
  this.ovls = new Array;   // two arrays to hold the overlay texts
  this.ovls[0] = new Array;   // overlay texts needed
  this.ovls[1] = new Array;
  this.div_id = div_id;
  this.images = new Array;  // actual images
  this.imagenames = new Array;
  this.imagealts = new Array;
  this.imagelinks = new Array;
  this.imageclicks = new Array;
  this.imagecountdown = new Array;
  this.imageoverlays = new Array;
  this.imageidxs = new Array;
  this.imagemaps = new Array;
  this.blobbox = null;
  this.blobon = '';
  this.bloboff = '';
  this.firsttrans = true;
  this.target = new Array;
  if (typeof(imagedefs) == 'string') {
    var defs = imagedefs.split(',');  // image name|link|alt comma sep list
    for (var i =0; i < defs.length; i++) {
      var defsa = defs[i].split('|');
      this.imagenames[this.imagenames.length] = defsa[0];
      this.imagelinks[this.imagelinks.length] = defsa[1];
      this.imagealts[this.imagealts.length] = defsa[2];
    }
  } else { // its an array of json arrays
    for (var i=0; i < imagedefs.length; i++) {
      this.imagenames[i] = imagedefs[i].img;
      this.imagelinks[i] = imagedefs[i].link;
      this.imagealts[i] = imagedefs[i].alt;
      this.imageclicks[i] = imagedefs[i].click;
      this.imagemaps[i] = imagedefs[i].map || '';
      this.imagecountdown[i] = (imagedefs[i].timer && imagedefs[i].timer != undefined && imagedefs[i].timer.f != undefined ? imagedefs[i].timer : null);
      this.imageoverlays[i] = new Array();
      for (var j=0; j<4; j++) {
        this.imageoverlays[i][j] = (imagedefs[i].textoverlays ? imagedefs[i].textoverlays[j] : null);
      }
      this.imageidxs[i] = imagedefs[i].idx;  // index to identify translated div text
      this.target[i] = imagedefs[i].target == undefined ? '' : imagedefs[i].target;
    }
  }

  this.imgclass = imgclass;
  this.path = path;
  this.effectno = -1;
  this.init();
  this.loaded = false;
  this.img = 0;
  this.image = 0;
  this.initinterval = 100; // timer interval for loaded check
  this.timerInc = 25;  // frames per sec
  this.jumpInc = 1; // default movement per timer call
  this.speed = 100;  // timer interval for transition
  this.interval = 1000; // timer interval between transitions
  this.effects = def_effects;
  this.mouseIsOver = false;

  if (typeof(options) == 'string') {
    if (options != 'random') { this.effects = options.split(','); }
  } else {
    if (options.effect) { if (options.effect != 'random') { this.effects = options.effect.split(','); } }
    if (options.speed) { this.speed = options.speed-0; }
    if (options.interval) { this.interval = options.interval-0; }
    if (options.leftbtn) {
      var el = document.getElementById(options.leftbtn);
      if (el) {
        el.onclick = ( function ( obj ) { return function () { obj.doPrev( ); }; } )( this );
      }
    }
    if (options.rightbtn) {
      var el = document.getElementById(options.rightbtn);
      if (el) {
        el.onclick = ( function ( obj ) { return function () { obj.doNext( ); }; } )( this );
      }
    }
    if (options.blobbox && this.imagenames.length > 1) {
      this.blobbox = createDynamicPopup(this.div_id+'_blobbox',false,options.blobbox,this.div_id);
      this.blobon = options.blobon;
      this.bloboff = options.bloboff;
      var blobi = '';
      for (var i=0; i < this.imagenames.length; i++) {
        blobi += '<div id="'+this.div_id+'_blob_'+i+'" class="'+(i==0 ? this.blobon : this.bloboff)+'"  > </div>';
      }
      this.blobbox.innerHTML = blobi;  blobi = '';
      this.blobbox.style.display = '';  // go to css setting

      for (var i=0; i < this.imagenames.length; i++) {
        var el = document.getElementById(this.div_id+'_blob_'+i);
        if (el) {
          el.pageno = i;
          el.onclick = ( function ( obj ) { return function () { obj.doPage(this); }; } )( this );
        }
      }

    }
  }
//  if (!animate) { return; }
  this.timer = setTimeout( ( function ( obj ) { return function () { obj.onTimer( ); }; } )( this ), this.initinterval );
}
function imageTrans(div_id,imgclass,path,imagenames,alt,options) {
// dont start if responsive size doesnt allow it
  if (window.responsiveSwitchOk) {
    if (!responsiveSwitchOk('imageTrans')) { return; }
  }

// imagenames can be a string with 'jpg|link|alt,jpg|link|alt' or a json array [ { 'img':'', 'link':'', 'alt':'' },... ]
  var it = new c_imageTrans(div_id,imgclass,path,imagenames,alt,options);
  return it;
}

function trim(str, chars) {
	return ltrim(rtrim(str, chars), chars);
}

function ltrim(str, chars) {
	chars = chars || "\\s";
 try {
  	var res = str.replace(new RegExp("^[" + chars + "]+", "g"), "");
 } catch(err) {};
	return res;
}

function rtrim(str, chars) {
	chars = chars || "\\s";
 try {
  	var res = str.replace(new RegExp("[" + chars + "]+$", "g"), "");
 } catch(err) {};
	return res;
}

var preLoadImages = '';  // comma seperated list of file names
function preLoad() {
// image path must be relative to root. Comma seperate multiple values
  if (document.images) {
    var imga = preLoadImages.split(',');
    for (var i=0; i < imga.length; i++) {
      if (imga[i] != '') {
        var rslt = new Image();
        rslt.src = fullPath(imga[i]);
      }
    }
  preLoadImages = '';
  }
}
// load most of the images after the main body has loaded.
// to use just change img src="??" to img data-src="??" and add src="<elucid_nonSSL>assets/images/blank.gif"
// for different mobile images add data-msrc="??" - to stop mobile loading an image use data-msrc=""
// if the image needs onload or onerror handlers add them as data-onload="alert('i loaded')" data-onerror="alert(this)"
var postLoadImagesTimerTimer = null
function postLoadImagesTimer(event) {
  postLoadImages();
}
function postLoadImages(src) {
  if (src == 'scroll' || src == 'resize') {  // stop too many calls
    clearTimeout(postLoadImagesTimerTimer);
    postLoadImagesTimerTimer = setTimeout(postLoadImagesTimer,300);
    return;
  }
  var dload=null, derror=null;
  function setSrc(img, src, addEventHandlers) {
    if (addEventHandlers && dload) { img.onload = new Function(dload); }
    if (addEventHandlers && derror) { img.onerror = new Function(derror); }
//showDebug(' going to: '+src+' ',true);
    if (img.src != src) { img.src = src; }
  }
  var ovr = false;
  var loadAll = (window.onlyPostLoadVisibleImages !== true) || (ovr);
  var s = screenSize();
  var Coll = document.getElementsByTagName("IMG");
  for (var m = 0; m < Coll.length; m++) {
    var dsrc = Coll[m].getAttribute('data-src');
    var origsrc = Coll[m].getAttribute('data-origsrc');
//showDebug(' dsrc='+dsrc,true);
    if (dsrc && !origsrc && dsrc != Coll[m].src) {  // imageerr function will have run if origsrc exists so dont try again.
      var visible = true;
      var b = getBounds(Coll[m]);
      if (b.y+b.h < s.y || b.y > s.y+s.h) { visible = false; }
//showDebug(' visi='+b.y+' '+b.h+' '+s.y+' '+s.h+' ',true);
      if (loadAll || visible) {
        var osrc = Coll[m].getAttribute('data-osrc');
        if (!osrc) { osrc = Coll[m].src; Coll[m].setAttribute('data-osrc',osrc); } // save the original
        var msrc = Coll[m].getAttribute('data-msrc');
        dload = Coll[m].getAttribute('data-onload');

	//showDebug(dsrc+' ', true);

        derror = Coll[m].getAttribute('data-onerror');
        if (responsiveSwitchOk('use_data_src')) { // true = not mobile
          if (Coll[m].src = dsrc) { setSrc(Coll[m],dsrc,true); }
          if (msrc = null) { Coll[m].setAttribute('data-src',''); } // stop it looking for this image again
        } else {
          if (msrc == "" || msrc == 'none') { /* dont load anything  */}
          else
          if (msrc) { if (Coll[m].src != msrc) { setSrc(Coll[m],msrc,true); } }
          else
          if (dsrc) { if (Coll[m].src != dsrc) { setSrc(Coll[m],dsrc,true); } }
          else      { if (Coll[m].src != osrc) { setSrc(Coll[m],osrc,false); } }
        }
//if (m == 70) alert(dsrc+' | '+msrc+' | '+osrc+' | '+Coll[m].src);
      }
    }
    // data-currency-src means there are currency dependent versions of this image
    else if (Coll[m].getAttribute('data-currency-src')) {
      var s = Coll[m].getAttribute('data-currency-src');
      s = s.replace('.jpg',currency_code+'.jpg');
      s = s.replace('.gif',currency_code+'.gif');
      s = s.replace('.png',currency_code+'.png');
      Coll[m].src = s;
      Coll[m].setAttribute('data-currency-src','');
    }
    else if (Coll[m].getAttribute('data-gbponly-src')) {
      var s = Coll[m].getAttribute('data-gbponly-src');
      if (window.currency_code && currency_code != 'GBP') {
        Coll[m].style.display = 'none'; // not gbp = hide it
      } else {
        s = s.replace('.jpg',currency_code+'.jpg');
        s = s.replace('.gif',currency_code+'.gif');
        s = s.replace('.png',currency_code+'.png');
        Coll[m].src = s;
        Coll[m].setAttribute('data-gbponly-src','');
      }
    }
  }

  var Coll = document.getElementsByTagName("INPUT");
  for (var m = 0; m < Coll.length; ++m) {
    var dsrc = Coll[m].getAttribute('data-src');
    if (dsrc) {
        var osrc = Coll[m].getAttribute('data-osrc');
        if (!osrc) { Coll[m].setAttribute('data-osrc',Coll[m].src); } // save the original
        var msrc = Coll[m].getAttribute('data-msrc');

        if (responsiveSwitchOk('use_data_src')) { // true = not mobile
          if (Coll[m].src = dsrc) { setSrc(Coll[m],dsrc,true); }
          if (msrc = null) { Coll[m].setAttribute('data-src',''); } // stop it looking for this image again
        } else {
          if (msrc == "" || msrc == 'none') { /* dont load anything  */}
          else
          if (msrc) { if (Coll[m].src != msrc) { setSrc(Coll[m],msrc,true); } }
          else
          if (dsrc) { if (Coll[m].src != dsrc) { setSrc(Coll[m],dsrc,true); } }
          else      { if (Coll[m].src != osrc) { setSrc(Coll[m],osrc,false); } }
        }
    }
  }
  if (window.resetMenuHeight) { resetMenuHeight(); }
}

function makeImgVisible(img) {
//document.title = img.src;
  img.src.indexOf('blank.gif') == -1 ? img.style.display='block' : img.style.display='none';
}

function addSortIdx(arr) {
  for (var i=0; i<arr.length; i++) { arr[i].sort_idx = i; } // sort_idx is a work around for chrome sort randomizer
  return arr;
}
// seq should be a numeric sequence number but can be null or true or 'last' for backward compatibility
// the events will be run in sequence order - true = 100 null = 101 last = 102
// important < 100, facebook, google > 5000
// document.title = document.title + seq+', ';
// THESE Functions and var SHOULD BE IN HOMEHTML FOR non render blocking logic to work
if (!window.loadEvents) { var loadEvents = new Array(); }
function addInitEvent(func) { loadEvents.push( { 'func':func, 'seq':-1 } ); } // non render blocking logic to delay init functions
//function addInitEvent(func) { func(); }  // standard run javascript now method
function addLoadEvent(func, seq) { loadEvents.push( { 'func':func, 'seq':seq } ); }

function loadSort(a,b) {
  var res = a.seq - b.seq-0;
  if (res == 0) { res = a.sort_idx-0 - b.sort_idx-0; }
  return res;
}

function doLoad() {
addLog('rsdol='+getResponsiveStyle());
  pageIsLoaded = true;
//  doResize();
  backController();
  checkAnimate();
  if (window.doOnPageLoad) { doOnPageLoad(); }
  initHeaderDivs();

  for (var i=loadEvents.length-1; i >= 0; i--) {
    switch(loadEvents[i].seq) {
      case true : loadEvents[i].seq = 100; break;
      case undefined : loadEvents[i].seq = 101; break;
      case 'last' : loadEvents[i].seq = 102; break;
    }
  }
  loadEvents = addSortIdx(loadEvents);  // sort_idx is a work around for chrome sort randomizer
  loadEvents = loadEvents.sort(loadSort);
//showJson(loadEvents);
  for (var i=0; i<loadEvents.length; i++) {
//document.title = document.title + loadEvents[i].seq+', ';
//alert(loadEvents[i].seq+'\r\n'+loadEvents[i].func);
//alert(loadEvents[i].func);
//var str = loadEvents[i].func.toString();
//showDebug(str.substr(0,20)+'<br>',true);
    if (typeof(loadEvents[i].func) == 'string') {
      eval(loadEvents[i].func+'()');  // can pass string so js doesnt need to find the function during loading
    } else {
      loadEvents[i].func();
    }
  }
}
function doUnLoad() {
// just here so ff onload fires when back button is clicked
}
function doBeforeUnload() {
  window.onbeforeunload = null;
  if (customBeforeUnload) { customBeforeUnload(); }
}
function doScrollEvent() {
  if (window.onlyPostLoadVisibleImages === true) { postLoadImages('scroll'); }
}
var x_last_wid = -1, x_last_ht = -1, x_inResize = false, x_ccc = 0;
function doResize() {
  if (!pageIsLoaded) return;
  if (x_inResize) { return; }  // this can be set true in a resize event handler to stop infinite looping
  var wid = screenWidth();
  var ht = screenHeight();
//x_ccc++;
//document.title = wid+' '+x_last_wid+' '+ht+' '+x_last_ht+' '+x_ccc+' | ';
  if (wid != x_last_wid || ht != x_last_ht) {
    showModalCover(false,true); // just does a refresh of size if its visible
    if (customResize) { customResize(); }
    if (window.onlyPostLoadVisibleImages === true) { postLoadImages('resize'); }
    x_last_wid = wid;
    x_last_ht = ht;
  }
}
function backController() {
  if (!window.H_ID) { return; }
  H_ID_c = getXCookie('HIDCookie');
  var isnew = (H_ID == H_ID_c);
//  console.log(H_ID+' | '+H_ID_c+' | '+backmode+' isnew='+isnew);

  if (!isnew) {
    if (backmode == 'normal') {
      refreshMiniBasket();
    }
    if (backmode == 'basket'){
      document.body.innerHTML = '';
      location.reload(true);
    }
    if (backmode == 'lock'){
//dh cybersource        document.body.innerHTML = '';
//alert('reload');
//dh cybersource       location.reload(true);
    }
    if (window.backFunction) { window.backFunction(); }
  }
  var fixer = new Date();
	 loadmark = fixer.getTime();
  var el = document.getElementById('loadctrl');
  if (el) { if (el.value == "0") { el.value = loadmark; } }
}

function checkAnimate() {
// add ani=false to the querystring to stop animations
  try { // can except in a window.open popup
  var s = window.location.href.toLowerCase();
  if (s.indexOf('ani=') > -1) {
    var ani = (s.indexOf('ani=false') > -1 ? 'F' : 'T');
    setXCookie('animate',ani);
    animate = (ani != 'F');
  } else {
    var ani = getXCookie('animate');
    if (ani == 'F') { animate = false; }
  }
  } catch(err) {}
}
function setHeight() {
var y = -1;
if (self.innerHeight){y = self.innerHeight;}
else if (document.documentElement && document.documentElement.clientHeight){y = document.documentElement.clientHeight;}
else if (document.body){y = document.body.clientHeight;}
if ((y > 100) && (document.body.offsetHeight) && (y > document.body.offsetHeight))
  { document.body.style.height = y + 'px'; }
}
//--------------------------------------
function noBack(path) {
// call this rather than loction= to stop the current page being added to the browser history
// not tested but the logic needs to be replace the url rather than locate to another
  window.URL.replace(path);
}

function imageErr(el, id, hideel, stopclick, restart) {
// usage = <img onerror="imageErr(this, 'imagesize', 'hide1;hide2', 'id1;id2;over|id1;out|id1');" >
// id can be 'large','medium' etc of a full img file path
// hideel is a ; delim list of ids to hide eg. the id of a box with a zoom prompt in it
// stopclick is a ; delim list of ids to remove the onclcik handler from.
// stopclick can be used to switch off mouseover and mouseout by adding 'over|' of out| before the id
// restart = true will re-install the onerror handler - use this if the image src may be changed again
// original image 'src' will be saved into data-origsrc so that the original image path and name can be found
  var safe = el.onerror;
  el.onerror = null;  // stop stack overflow if err image doesnt exist
//return
  if (el.src.indexOf('coming_soon') > -1) { return; }
  if (id == 'large' || id == 'zoom' || id == 'medium' || id == 'thumb' || id == 'small' || id == 'smallhero' || id == 'alt') {
    id = thisUrl+'products/images/'+id+'/no_image_s.jpg';
  }
  if (id == 'swatch' ) { id = thisUrl+'products/images/no-swatch.jpg'; }
//alert(id);
// save the original 'src' attribute
  var os = el.getAttribute('data-origsrc');
  if (!os || ((os.match(/pixel.gif/i) == null) && (os.match(/blank.gif/i) == null))) {
    el.setAttribute('data-origsrc',el.src);
  }
  el.src=id;
//hideel = '';
  if (hideel) {
    var ar = hideel.split(';');
    for (var i=0; i<ar.length; i++) {
      if (ar[i]) {
        ely = document.getElementById(ar[i].replace(' ',''));
        if (ely) {ely.style.display='none'; }
      }
    }
  }
  if (stopclick) {
    var ar = stopclick.split(';');
    for (var i=0; i<ar.length; i++) {
      if (ar[i]) {
        var typ = '';
        var nam = ar[i].replace(' ','');
        if (nam.match(/over\|/i)) { typ = 'min'; nam = nam.substr(5); }
        if (nam.match(/out\|/i)) { typ = 'mout'; nam = nam.substr(4); }
        ely = document.getElementById(nam);
        if (ely) {
          if (typ == '') { ely.onclick = null; ely.style.cursor = 'default'; }
          if (typ == 'min') { ely.onmouseover = null; }
          if (typ == 'mout') { ely.onmouseout = null; }
        }
      }
    }
  }
  if (restart) {
    el.onerror = safe;
  }
}
function pluralise(id, stext, count) {
  var el = document.getElementById(id);
  if (el) { if (count != 1) { el.innerHTML = el.innerHTML.replace(stext,stext+'s'); } }
}

function noNull(s) {
  if (s == null) { s = ''; }
  return s;
}

var searchFocused = null;
function searchFocus(el,text) {
  if (typeof(el) == 'string') { el = document.getElementById(el); }
  if (el) {
    if (text && text.substr(0,3) == 'tt_') {
      clearField(el,text,'Search');
    } else {
      clearField(el,'tt_srch_def_value','Search');
    }
    searchFocused = el;
  }
}

function clearField(el,text,textnt) {
// will clear the default text in a box -
// usage should be onclick=clearField(this,'default') onkeydown=clearField(this,'default') onfocus=clearField(this,'default')
// or data-def="default" onclick=clearField(this) onkeydown=clearField(this) onfocus=clearField(this)
// text should be a translated text id begining with tt_
// if no translator is in use then text can be the default text to clear
// textnt can optionally be passed as the default english text to use if no translated text exists
  if (typeof(el) == 'string') { el = document.getElementById(el); }
  if (el) {
    //alert(el);
    var def = el.getAttribute('data-def');  // default english if no translate data
    var def_tt = el.getAttribute('data-tt');  // default tt_ id as an alt to passing 'text' in every call
    var dv = '';
    if (text == undefined) { text = def_tt || def; }
    if (textnt == undefined) { if (def) { textnt = def; } else { textnt = text; } }
    if (text && text != undefined) {
      if (text.substr(0,3) == 'tt_') {
        dv = getTranslatedText(text,textnt);

      } else {
        dv = text;
      }
    }
    if (dv && dv == el.value || def == el.value) { el.value = ''; }
  }
}

function fillField(el,text){
  // if a field has default text and user clicks clearField() clears it
  // this puts the default text back in if field empty
  if (typeof(el) == 'string') { el = document.getElementById(el); }
  if (el && el.value == '') {
    var def = el.getAttribute('data-def');  // default english if no translate data
    var def_tt = el.getAttribute('data-tt');  // default tt_ id as an alt to passing 'text' in every call
    var dv = '';
    if (text == undefined) { text = def_tt || def; }
    if (text){
      el.value = getTranslatedText(text);
    }
  }
}

var videoCount = 0;
function showVideo(name, title, wid, ht) {
  name = (name.indexOf(':') > -1 ? name : thisUrl+'products/video/'+name+'.wmv');
  wid = (wid ? wid : 320);
  ht = (ht ? ht : 290);
  var s = '';
  videoCount++;
  s+= '<object id="MediaPlayer'+videoCount+'" CLASSID="CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95" codebase="https://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701" standby="Loading..." width='+wid+' height='+ht+' type="application/x-oleobject" >';
  s+= '<param name="FileName" value="'+name+'">';
  s+= '<param name="AnimationatStart" value="true">';
  s+= '<param name="TransparentatStart" value="true">';
  s+= '<param name="AutoStart" value="true">';
  s+= '<param name="AutoSize" value="false">';
//  s+= '<param name="ShowTracker" value="true">';
//  s+= '<param name="ShowGotoBar" value="false">';
//  s+= '<param name="ShowDisplay" value="false">';
//  s+= '<param name="ShowStatusBar" value="false">';
  s+= '<param name="UIMode" value="mini">';
//  s+= '<param name="ShowControls" value="true">';
  s+= '<param name="Volume" value="-450">';
  s+= '<embed type="application/x-mplayer2" pluginspage="https://www.microsoft.com/Windows/MediaPlayer/" src="'+name+'" name="MediaPlayer'+videoCount+'"  width='+wid+' height='+ht+'  autostart=1 showcontrols=1 volume=-450>';
  s+= '</object>';
  showX(s,'',wid+80,title);
}

function printDiv(divID, ovWid, ovHt) {
// note the div passed must have a width and height set in css to use auto setting
  diva = divID.split(';');
  var w = 0;
  var h = 0;
  for (var i=0; i < diva.length; i++) {
    if (diva[i]) {
      var el = document.getElementById(diva[i]);
      w += el.clientWidth-0;
      h += el.clientHeight-0;
    }
  }
  if (w < 200) { w = 600; }
  if (h < 200) { h = 400; }
  if (ovWid != undefined) { w = ovWid; }
  if (ovHt != undefined)  { h = ovHt; }
  var w1 = w+20;
  var h1 = h;
  h1 = h1+100;
  if (h1 > self.innerHeight)  { h1 = self.innerHeight; }
  var x = "width="+w1+",height="+h1+",resizable,scrollbars=yes,menubar=yes";
  var win = window.open(thisUrl+'printdiv.html?divid='+divID+'&wid='+w+'&hei='+h,'printdiv',x);
  try {
    win.focus();
    win.moveTo(50, 50)
  }
  catch(err)
  {}
}

function stopRightClick() {
  function clickIE() {if (document.all) {return false;}}
  function clickNS(e) {if (document.layers||(document.getElementById&&!document.all)) { if (e.which==2||e.which==3) {(message);return false;}}}
  if (document.layers) {document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;}
  else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;}
  document.oncontextmenu=new Function("return false")
}

function touchDoc(event) {
// touch anywhere handler for closing menus etc.
  if (window.touchDocCustom) { window.touchDocCustom(event); }
}
function mobListen() {
// same as adding ontouchstart="touchDoc" in html
 document.addEventListener("touchstart", touchDoc, false);
 //ios5 has stuffed up the onload call after back button so do this
 window.onpageshow = function(evt) {
   if (evt.persisted) {
     window.onload(evt);
   }
 }
}
function setMessage(s) {
// use for top of the page messages
  var el = document.getElementById('headMessage');
  if (el) { el.innerHTML = s; }
}

function initHeaderDivs() {
  // header divs are empty so that google doesnt pick up google index text from them at the top of the page
  // copy text from the holder divs lower down the page
  var el = document.getElementById('headerContainerPrint');
  var el1 = document.getElementById('headerContainerPrint_Holder');
  if (el && el1 ) { el.innerHTML = el1.innerHTML; }

  var el = document.getElementById('headMessages');
  var el1 = document.getElementById('headMessages_Holder');
  if (el && el1 && (trim(el1.innerHTML) != '')) { el.innerHTML = el1.innerHTML; }

  var el = document.getElementById('EUCookieMessage');
  var el1 = document.getElementById('EUCookieMessage_Holder');
  if (el && el1 && (trim(el1.innerHTML) != '') && cookiesOn && !EUCookieSet) { el.innerHTML = el1.innerHTML; }
  if (window.EUCookieMode == 'notify') { setEUCookie('notify'); }
}

function nonPHPImageName(imgname) {
// removes the showpartf_image.php stuff from an image name and returns just the raw path
  var res = imgname;
  if (imgname.indexOf('showpartf_image.php') > -1) {
    res = imgname.split('showpartf_image.php')[0] + imgname.split('large=')[1];
  }
  return res;
}

function makeWorkArea(s) {
  // temp off screen work area. First used as a place to process the ajax matrix html
  // done so that the matrix height is known before passing it to showAlertX
  var div = createDynamicPopup('workArea');
  if (div) {
    div.style.left = '-3000px';
    div.style.display = 'block';
    div.innerHTML = s;
  }
}
function getWorkArea() {
  var div = document.getElementById('workArea');
  var res = 'workArea doesnt exist';
  if (div) {
    res = div.innerHTML;
    div.innerHTML = '';
  }
  return res;
}

// override alertConfig in custom.js or individual vars before a call to alertX if needed
var alertConfig = {
 defaultHeaders : true,
 cssButtons : true,
 buttonExt : 'jpg',
 buttonAlt : true,
 buttonAlign: 'center',
 buttonWidth: '100px',
 useIcons : true,
 leftFillHeader : false,
 printButton : false,
 hideHead : false,
 removePadding : false,
 clickCoverToClose : false,
 spacingtop : 10,
 noclose : false,
 noscroll : false,
 nocover : false,
 forcesquarecorners : false,
 closeMsg : '',
 resets : { hideHead : false, removePadding : false, clickCoverToClose : false, noclose : false,
            noscroll : false, nocover : false, forcesquarecorners : false }
}
// next vars are set by alertXHeader so that drag handler and popupInit can get the ids used in the header
var alertCount = 0, alertKeyCount = 0;
var alertXId = '';
var alertXCloseId = '';
var alertXDragId = '';

function doAlertXButtons(id, buttons) {
  var s = '';
  var c = 0;
  for (var key in buttons) {
    var val = buttons[key];  // key is used as image name for non css buttons
    c++;
    if (c==1) { s+='<table id="'+id+'PromptTable" align="'+alertConfig.buttonAlign+'" cellspacing="0" cellpadding="0" style="width:auto; margin:0 auto; padding:0;" ><tr>'; }
    if (alertConfig.cssButtons) {
      s+='<td align="center"><a class="BMBtn" id="closeButton" onclick="closeAlertX(\''+id+'\',\''+key+'\');" style="width:'+alertConfig.buttonWidth+'; margin:0px 0;color: white;" alt="'+(alertConfig.buttonAlt ? val : '')+'" >'+val+'</a></div></td>';
    } else {
      s+='<td align="center" style="padding:4px 5px 0 5px;"><img onclick="closeAlertX(\''+id+'\',\''+key+'\');" src="'+thisUrl+'assets/images/alert_'+key+'.'+alertConfig.buttonExt+'" alt="'+(alertConfig.buttonAlt ? val : '')+'" /></td>';
    }

  }
  if (c > 0) { s+='</tr></table>'; }

  return s;
}

// header is in a function so I can use if for all popups to get the same look for all of them
// id should only be passed by initalertX
function alertXHeader(header, id) {

  var s = '';
  var clicky = (id ? 'onclick="closeAlertX(\''+id+'\');"' : 'onclick="popupClose(\'\',\'\');"');
  var styl = (alertConfig.noclose ? 'style="display:none;" ' : '');
  if (alertConfig.hideHead) {
    s+='<table id="alertXHead'+alertCount+'" class="alertXHead_hide" cellspacing="0" cellpadding="0" border="0" ><tr>';
    s+='<td id="alertXClose'+alertCount+'" class="alertXCloseHide" align="right" valign="top"><img src="'+thisUrl+'assets/images/alertclose.'+alertConfig.buttonExt+'" '+clicky+' '+styl+' alt="close" title="close" /></td>';
    s+='</tr></table>';
//    s+='<span class="escCloseMsg">Hit escape to close this message</span>';
  } else {
    s+='<table id="alertXHead'+alertCount+'" class="alertXHead" cellspacing="0" cellpadding="0" border="0" ><tr>';
    if (alertConfig.useIcons) {
      if (header.match(/alert/i)) { s+='<td id="alertXIconI"></td>'; }
      if (header.match(/confirm/i)) { s+='<td id="alertXIconQ"></td>'; }
    }
    header = trim(header);
    header = header.replace(/alert:/i,'');
    header = header.replace(/confirm:/i,'');
    headerStyle = '';
    if (alertConfig.printButton && responsiveSwitchOk('alertXPrint')) {
      s+='<td id="alertXPrint" class="alertXPrint"><img src="'+thisUrl+'assets/images/alertprint.'+alertConfig.buttonExt+'" onclick="printDiv(\'alertXMessage'+alertCount+'\');" alt="print" title="print" /></td>';
      headerStyle = ' style="text-align:center;"';
    }
    if (!alertConfig.useIcons && !alertConfig.printButton && alertConfig.leftFillHeader) {
      s+='<td id="alertXLeftFill" class="alertXLeftFill">&nbsp;</td>';
    }
    s+='<td id="alertXHeadText'+alertCount+'" class="alertXHeadText" '+headerStyle+'>'+header+'</td>';
    s+='<td id="alertXClose'+alertCount+'" class="alertXClose" align="right" valign="top"><img src="'+thisUrl+'assets/images/alertclose.'+alertConfig.buttonExt+'" '+clicky+' '+styl+' alt="close" title="close" /></td>';
    s+='</tr></table>';

//    s+='<span class="escCloseMsg">Hit escape to close this message</span>';
  }
  alertXId = 'alertX'+alertCount;
  alertXCloseId = 'alertXClose'+alertCount;
  alertXDragId = 'alertXHeadText'+alertCount;
//alert(s);
  s+=alertConfig.closeMsg;
  return s;
}
function initAlertX(header, buttons, justify, modal) {
  alertCount++;
  var id = 'alertX'+alertCount;
  var alertClass = 'alertX';
  el = createDynamicPopup(id, modal, alertClass);

  if (alertConfig.forcesquarecorners) { el.style.borderRadius = 0; }
  var s = alertXHeader(header, id);

  var styl = 'style="text-align:'+justify+'; "';
  s+='<div class="alertXMessage" id="alertXMessage'+alertCount+'" class="alertXMessage" '+styl+'></div>';
  var c = 0; for (var key in buttons) { c++; }
  if (c) {
    var styl = 'style="'+(alertConfig.noclose ? 'display:none; ' : '')+'"';
    s+='<div class="alertXPrompt" id="'+id+'Prompt" '+styl+'><div id="'+id+'PromptContainer">';
    s+= doAlertXButtons(id, buttons);
    s+='</div></div>';
  }

  if (el) {
    el.innerHTML = s;
    var el1 = document.getElementById(alertXDragId);
    if (!is_mob && el1 && window.DragHandler) { var AlertX = DragHandler.attach(el1,el); }// make it draggable
  }
  // resets for next call
  alertConfig.printButton = false;
  alertKeyCount = alertCount;
  document.onkeyup = key_event;
  return el;
}

function closeThisAlertX(thisel, val) {
  // will find the AlertX holding the thisel element and close it with 'val' as the closeresult
  var el = thisel;
  while((el=el.offsetParent) != null) {
    if (el.id.substr(0,6) == 'alertX' && el.id.substr(0,13) != 'alertXMessage') {
      closeAlertX(el.id,val);
      return true;
    }
  }
  return false;
}

function closeAlertX(id, val, forceClose) {
  var el = document.getElementById(id);
  if (el) {
    var res = true;
    if (el.closer && el.closer != undefined) { res = el.closer(val); }
    if (forceClose || res || res == undefined) {
      el.style.display = 'none';
//      hideShadowBox();
      hideModalCover();
      alertKeyCount--;  // just in case there is another below this alertX
      showSelects();
      document.body.removeChild(el);
    }
  }
}

// escape key closes alertX pop ups
function key_event(e) {
  e = e || event;
  if (e.keyCode == 27) {
    closeAlertX('alertX'+alertKeyCount,null);
  }
}


var alertData = new Array();
var alertPreProcess = null;  // point this to a function xyz(s) to process the string just before it is put in innerHTML
var alertPostProcess = null;  // point this to a function xyz(el) to process the popup element once it is part of the HTML
function showAlertX(s, header, buttons, width, justify, closer, modal ) {
// returns the ID of the Alert box in case it is needed for manual close or button changing
  if (alertConfig.noclose) {
    alertConfig.clickCoverToClose = false;
    closer = function() { return false; };
  }

  // translation
  if (s.indexOf('tt_') == 0) {  // text string can just be a tt_blah identifier
    s = getTranslatedText(s,s);
  }

  if (header && header.indexOf('tt_') > -1) {
    header = getTranslatedText(header,header);
  }

  for (var button in buttons) {
    if (buttons[button].indexOf('tt_') > -1) {
      buttons[button] = getTranslatedText(buttons[button],buttons[button]);
    }
  }

  if (!header) { header = ''; }
  el = initAlertX(header, buttons, justify, modal);
  if (el) {
    el.closer = closer;
    var elm = document.getElementById('alertXMessage'+alertCount);
    if (alertConfig.forcesquarecorners) { elm.style.borderRadius = 0; }

    // was it a div id passed rather than text
    if (s.length < 30 && s.indexOf(' ') == -1) {
      var eldata = document.getElementById(s);
      if (eldata) {
        // the first call will put contents into alertData array and kill the original
        // all future calls will use the alertData data
        // this makes sure that if the passed div id contains an input form, there arent duplicates of all the field ids.
        if (alertData[s]) {
          s = alertData[s];
        } else {
          alertData[s] = getTemplate(s, true); //eldata.innerHTML;
          s = alertData[s];
//          eldata.innerHTML = '';
        }
      }
    }

    if (alertPreProcess) { s = alertPreProcess(s); } // used in showpartf.js
    alertPreProcess = null;

    if (alertConfig.clickCoverToClose) {
      showModalCover( new Function("closeAlertX('"+alertXId+"');"));
    } else if (alertConfig.nocover) {

    } else {
      showModalCover();
    }
    var twid = 0;
    if (!width) {
      temp = createDynamicPopup('alertXT');
      if (temp) {
        s = s.replace(/#39/g,'\'');  // restore #39's to quotes before browser sees it
        temp.innerHTML = s;
        temp.style.display = 'block';
        twid = temp.offsetWidth-0;  // this is the width the text wants to try to be
        temp.innerHTML = '';
        try { document.body.removeChild(temp); } catch(err) { }
      }
    }
    var ss  = screenSize();
    el.style.display = 'block';
    if (alertConfig.removePadding) { elm.style.padding = '0px'; el.style.padding = '0px'; }

    var hel = document.getElementById('alertXHead'+alertCount);
    if (hel) {
      hel.style.width = 'auto';
      var hwid = hel.offsetWidth;
      hel.style.width = '100%';
  //    alert(twid+' '+hwid);
      if (hwid > twid) { twid = hwid; }
    }

    hideSelects();

    var elpt = document.getElementById('alertX'+alertCount+'PromptTable');
    if (elpt && elpt.offsetWidth > twid) { twid = elpt.offsetWidth; }
    if (twid == 0) { twid = 400; }
    if (twid < 200) { twid = 200; }
    if (width) { twid = width; }

    try { elm.innerHTML = s; } catch(err) {}

    if (alertPostProcess) { alertPostProcess(el); }
    alertPostProcess = null;

    var cw = 0;    var ch = 0;
    var elc = document.getElementById('alertXCloseImg'+alertCount);
    if (elc) {
      var cw = cssStyle(elc,'right')-0;  if (cw > 0) { cw = 0 } else { cw = -cw; }
      var ch = cssStyle(elc,'top')-0;    if (ch > 0) { ch = 0 } else { ch = -ch; }
    }

    var elp = document.getElementById('alertX'+alertCount+'Prompt');
    if (twid == '100%') {
      elm.style.width = '100%';
      el.style.width = '';
    } else {
      if (twid > ss.w-wborder(el)-wborder(elm)) { twid = ss.w-wborder(el)-wborder(elm); }
      elm.style.width = twid + 'px';
      el.style.width = twid + wborder(elm) + 'px';
    }

    if (responsiveStyle != 'phone') {
      var maxmh = 0;
      if (el.offsetHeight+ch+alertConfig.spacingtop > ss.h) {
        var elh = document.getElementById('alertXHead'+alertCount);
        maxmh = ss.h-(elp ? elp.offsetHeight : 0)-elh.offsetHeight-hborder(el)-hborder(elm)-alertConfig.spacingtop;
        if (elm.offsetHeight > maxmh) {
          if (alertConfig.noscroll) {
  //          el.style.top = '0';
          } else {
            if (maxmh-0 > 0) { elm.style.height = maxmh+'px'; }
          }
        }
      }

      var scrollbarW = elm.offsetWidth-elm.clientWidth;
      if (scrollbarW == 0 && (elm.scrollWidth > elm.offsetWidth)) { scrollbarW = 20; } else if (scrollbarW < 0 || scrollbarW > 50) { scrollbarW = 0; }
      var scrollbarH = elm.offsetHeight-elm.clientHeight;
      if (scrollbarH == 0 && (elm.scrollHeight > elm.offsetHeight)) { scrollbarH = 20; } else if (scrollbarH < 0 || scrollbarH > 50) { scrollbarH = 0; }

      if (scrollbarH > 0 && el.offsetWidth+scrollbarW+cw < ss.w) {
        twid += scrollbarW;
        elm.style.width = twid + 'px';
        el.style.width = twid + wborder(elm) + 'px';
      }
      if (maxmh > 0 && scrollbarW > 0 && el.offsetHeight+scrollbarH+ch < ss.h) {
        maxmh += scrollbarH;
        elm.style.height = maxmh+'px';
      }
    }

    var x = el.offsetLeft;
    var y = el.offsetTop;
    var w = el.offsetWidth;
    var h = el.offsetHeight;

    x = xOnPage(x,w,true);
    y = yOnPage(y,h-alertConfig.spacingtop,true);

    el.style.left = x + 'px';
    el.style.top  = y + 'px';
    if (!modal) { showShadowBox(el); }
  }
  try { el.focus(); } catch(err) {};
//alert('el='+el.style.zIndex+' cover='+document.getElementById('modalCover').style.zIndex);
  // resets for next call
  for (key in alertConfig.resets) {
    alertConfig[key] = alertConfig.resets[key];
  }

  return 'alertX'+alertCount;
}

function alertXrecentre(id) {
  var el = document.getElementById(id);
  if (el) {
    var x = el.offsetLeft;
    var y = el.offsetTop;
    var w = el.offsetWidth;
    var h = el.offsetHeight;
    x = xOnPage(x,w,true);
    y = yOnPage(y,h-alertConfig.spacingtop,true);
    el.style.left = x + 'px';
    el.style.top  = y + 'px';
  }
}

// closeFunc must have a single param which will contain the key of the button clicked or undefined if the X is clicked
// width will force the width of the box if set. '' or 0 will auto set the width
// header will override the default box header
// buttons is a json array like { 'visible text':'return value', 'Cancel':'cancel', 'Ok':'ok' }
// all alert boxes are independent so to close one manually you will need
// EITHER var myid = alertX(...) then closeAlertX(myid,'result',false)
// OR call closeThisAlertX(thisel, val) from an element in the box
// alertX buttons are { key:value }
//  where key is passed to closeFunc
//  key is image name for nonCSS buttons
//  and value is text on CSS button + alt on CSS and nonCSS buttons

// an example alertX closing function - tt_ strings in include_translation_text.tmp
function alertCloseTest(val) {
  if (val == 'ok') { alert(getTranslatedText('tt_act_ok','ok clicked - return is false so it wont close')); return false; }
  if (val == 'cancel') { alert(getTranslatedText('tt_act_canc','cancel clicked - will close'));  }
  if (val == undefined) { alert(getTranslatedText('tt_act_x','you clicked the close icon'));  }
}
function alertX_SetButtons(id, buttons) {
// allows the buttons to be changed dynamically  eg { 'Close':'close' }
// save the result of alertX() so it can be passed here as id
  var el = document.getElementById(id+'PromptContainer');
  if (el) {
    var s = doAlertXButtons(id, buttons);
    el.innerHTML = s;
  }
}

function alertX(s,closeFunc, width, header, align, buttons) {
  if (!s || s == undefined) { s = ''; }
  s = s + '';
//  s = s.replace(/\n/g,'<br>');
//  s = s.replace(/#39/g,'\'');
  if (!header && alertConfig.defaultHeaders) { header = co_name+' '+getTranslatedText('tt_alert_alert','Alert'); }
  if (!align) { align = 'center'; }
  if (!buttons) {
    if (alertConfig.cssButtons) {
      buttons = { 'ok':'tt_alert_ok' };
    } else {
      buttons = { 'ok':'OK' };
    }
  }
  return showAlertX(s,header, buttons, width, align, closeFunc, isModal);
}
function alertX_BR(s,closeFunc, width, header, align, buttons) {
  if (!s || s == undefined) { s = ''; }
  s = s + ''; // make sure its a string
  s = s.replace(/\</g,'&lt;');
  s = s.replace(/\>/g,'&gt;');
  s = s.replace(/\n/g,'<br>');
  if (!header && alertConfig.defaultHeaders) { header = getTranslatedText('tt_alert_alert','Alert'); }
  if (!align) { align = 'left'; }
  if (!buttons) {
    if (alertConfig.cssButtons) {
      buttons = { 'ok':'tt_alert_ok' };
    } else {
      buttons = { 'ok':'OK' };
    }
  }
  return showAlertX(s,header, buttons, width, align, closeFunc, isModal);
}
function showX(s,closeFunc, width, header) {
  if (!s || s == undefined) { s = ''; }
  s = s + '';
  if (!header && alertConfig.defaultHeaders) { header = co_name+' '+getTranslatedText('tt_alert_alert','Alert'); }
  return showAlertX(s,header,{}, width, 'center', closeFunc, isModal);
}
function confirmX(s, closeFunc, width, header) {
//  s = s.replace(/\n/g,'<br>');
//  s = s.replace(/#39/g,'''');
  if (!header && alertConfig.defaultHeaders) { header = co_name+' '+getTranslatedText('tt_alert_confirm','Confirm'); }
  if (alertConfig.cssButtons) {
    buttons = { 'ok':'tt_alert_ok', 'cancel':'tt_alert_cancel' };
  } else {
    buttons = { 'ok':'OK', 'cancel':'Cancel' };
  }
  return showAlertX(s, header, buttons, width, 'left', closeFunc, isModal);
}
function promptX(s, closeFunc, width, buttons, header) {
//  s = s.replace(/\n/g,'<br>');
//  s = s.replace(/#39/g,"'");
  return showAlertX(s,header,buttons, width, 'left', closeFunc, isModal);
}

function makeTable(fmt, s, tclass) {
// fmt is an array of columns defined as ['width','th justify','td justify','head text']
// justify is C, L or R
  var justy = new Array; justy['L']='Left'; justy['C'] = 'center'; justy['R'] = 'right';
//  if (tclass) { tclass = ' class="'+tclass+'"'; } else { tclass='';}
  if (tclass) {} else {tclass='';}

  var dohead = false;  // set to true if text supplied in the fmt array
  var totw = 0;
  var cols = fmt.length;
  for (var i=0; i<cols; i++) {
    var col = fmt[i];
    if (col[3]) { dohead = true; }
    totw += col[0]-0;
  }

  res = '<table cellspacing="0" cellpadding="4" border="0" width="'+totw+'" '+tclass+'>';
  if (dohead) {
    res += '<tr>';
    for (var i=0; i<fmt.length; i++) {
      var col = fmt[i];
      var val = (col[3] ? col[3] : '&nbsp;');
      res += '<th width="'+col[0]+'" align="'+justy[col[1]]+'">'+val+'</th>';
    }
    res += '</tr>';
  }

  var a = s.split('|');
  var col = 0;
  for (var i=0; i<a.length-1; i++) {
    col++;
    if (col == 1) { res += '<tr>'; }
    var val = (a[i] ? a[i] : '&nbsp;');
    res += '<td width="'+fmt[col-1][0]+'" align="'+justy[fmt[col-1][2]]+'">'+val+'</td>';
    if (col == cols) { res += '</tr>'; col = 0; }
  }

  res += '</table>';
  return res;
}

function seoName(s) {
   var res = trim(s);
   res = res.replace(/ /g,'-')
   res = res.replace(/&amp;/g,'')
   res = res.replace(/&/g,'')
   res = res.replace(/\//g,'-')
   res = res.replace(/\+/,'-')
   res = res.replace(/--/g,'-')
   res = res.replace(/---/g,'-')
   res = res.replace(/--/g,'-')
   res = res.replace(/\(/g,'')
   res = res.replace(/\)/g,'')
   res = res.replace(/\./g,'')
   res = res.replace(/\,/g,'')
   res = res.replace(/\:/g,'')
   res = res.replace(/\'/g,'')
   res = res.replace(/"/g,'')
   res = res.replace(/%/g,'')
   res = res.replace(/#/g,'')
   res = res.replace(/\*/g,'')
   res = res.replace(/�/g,'')
   res = res.replace(/�/g,'')
   res = res.replace(/%2D/g,'-')
   return res.toLowerCase();
}

//--------------------------------------

// js version of templateswitch processing
function getBlock(s, start_tag, end_tag) {
  var res = {'ok':false, bstt:-1, bend:-1, bstag:start_tag, betag:end_tag }
  res.bstt = s.indexOf(start_tag);
  res.bend = s.indexOf(end_tag);
  if (res.bstt > -1 && res.bend > -1) {
    res.ok = true;
  }
  return res;
}
function removeBlockTags(s, block) {
  return (block.ok ? s.replace(block.bstag,'').replace(block.betag,'') : s);
}
function removeBlock(s, block) {
  return (block.ok ? s.replace(s.substring(block.bstt,block.bend+block.betag.length),'') : s);
//  return (block.ok ? s.substring(0,block.bstt) + s.substr(block.bend+block.betag.length,100000) : s);
}

function getTemplate(id,clear) {
  var idl = id;
  if (typeof(idl) == 'string') { idl = document.getElementById(idl); }
  if (!idl || idl.innerHTML == undefined) {
    var res = 'holder template "'+id+'" does not exist';
  } else {
    var res = idl.innerHTML;
    if (clear === true) { try { idl.innerHTML = ''; } catch(err) {} } // may be needed to prevent duplicate ids
  }
  // remove comments from the start and end of the string
  var i = res.indexOf('<!--');
  if ((i==0) || ((i > 0) && (trim(res.substring(0,i-1)) == ''))) {
    res = res.substring(i+4,res.length);
    var i = res.lastIndexOf('-->');
    if ((i > 0) && (trim(res.substring(i+3,res.length)) == '')) {
      res = res.substring(0,i-1);
    }
  }
  // remove /* */ comments from the string
  var i = res.indexOf('/*');
  var c = 0;
  while (i > -1 && c < 10) {
    c++;
    var j = res.indexOf('*/');
    if (j > -1) {
      res = res.replace(res.substring(i,j+2),'');
      i = res.indexOf('/*');
    } else {
      i = -1;
    }
  }

  // process device dependent blocks in the template
  var block = null;
  for (var resp in responsiveLimits) {
    while ((block = getBlock(res,'[xx_excluded_start_'+resp+']','[xx_excluded_end_'+resp+']')) && block.ok) {
      if (resp != responsiveStyle) {
        res = removeBlockTags(res,block);
      } else {
        res = removeBlock(res,block);
      }
    }
    while((block = getBlock(res,'[xx_included_start_'+resp+']','[xx_included_end_'+resp+']')) && block.ok) {;
      if (resp == responsiveStyle) {
        res = removeBlockTags(res,block);
      } else {
        res = removeBlock(res,block);
      }
    }
  }

//alert(res);
//  res = res.replace(/\<!--/i,'');
//  res = res.replace(/\--\>/i,'');
  return trim(res);
}

function addBasketPopupContents() {
  var s = '';
  if (window.vouc_data && vouc_data.length > 0) {
    var tmpo = getTemplate('basketPopupLines_holder');
    if (tmpo) {
      var tmp = getTemplate('basketPopupLine_holder');
      if (tmp) {
        for (var i=0; i<vouc_data.length; i++) {
          var tmp1 = '';
          tmp1+=tmp;
          tmp1 = tmp1.replace(/[\<\[]xx_part\]/g, vouc_data[i].part);

          var major = (vouc_data[i].template2 ? vouc_data[i].template2 : vouc_data[i].major);
          tmp1 = tmp1.replace(/\[xx[_ ]partcolour\]/ig, vouc_data[i].partcolour ? vouc_data[i].partcolour  : major);
          tmp1 = tmp1.replace(/\[xx[_ ]major\]/g, major);
          tmp1 = tmp1.replace(/\[xx[_ ]part_link\]/g, (vouc_data[i].major ? vouc_data[i].major : vouc_data[i].part));
          tmp1 = tmp1.replace(/\[xx[_ ]descr\]/g, vouc_data[i].descr);
          tmp1 = tmp1.replace(/\[xx[_ ]descr_link\]/g, seoName(vouc_data[i].descr));
          tmp1 = tmp1.replace(/\[xx[_ ]qty\]/g, vouc_data[i].qty);
          tmp1 = tmp1.replace(/\[xx[_ ]price\]/g, vouc_data_curr+roundDP(vouc_data[i].total*vouc_data_mult,2));
          tmp1 = tmp1.replace(/\[xx[_ ]normal\]/g, vouc_data_curr+roundDP(vouc_data[i].normal*vouc_data[i].qty*vouc_data_mult,2));
          tmp1 = tmp1.replace(/\[xx[_ ]net\]/g, vouc_data_curr+roundDP(vouc_data[i].net*vouc_data_mult,2));
          tmp1 = tmp1.replace(/\[xx[_ ]odd_even\]/g, (i % 2 == 0) ? 'even' : 'odd' );
          s+= tmp1;
        }
      }
      tmpo = tmpo.replace('\[xx_lines\]',s);
      var el = document.getElementById('basketPopupScroll');
      if (el) { el.innerHTML = tmpo; }
    }
    var el = document.getElementById('basketPopupCarrier');
    if (el) { el.innerHTML = vouc_data_curr+roundDP((vouc_carrier*vouc_data_mult),2); }
    var el = document.getElementById('basketPopupCount');
    if (el) { el.innerHTML = vouc_data_qty; }
    var el = document.getElementById('basketPopupDiscount');
    if (el) { el.innerHTML = vouc_data_curr+roundDP(vouc_data_disc*vouc_data_mult,2); }
    var el = document.getElementById('basketPopupTotal');
    if (el) { el.innerHTML = vouc_data_curr+roundDP((vouc_data_tot)*vouc_data_mult,2); }
    var el = document.getElementById('basketPopupTotalVat');
    if (el) { el.innerHTML = vouc_data_curr+roundDP((vouc_data_tot-vouc_data_net-vouc_carrier)*vouc_data_mult,2); }
    var el = document.getElementById('basketPopupTotalNet');
    if (el) { el.innerHTML = vouc_data_curr+roundDP(vouc_data_net*vouc_data_mult,2); }

    if (window.customMiniBasketFunc) { customMiniBasketFunc(); } // code in custom.js if needed
  }
}

function initBasketPopup(modal) {
  var id = miniBasketConfig.popupdiv;
  var div = document.getElementById(id);
  if (!div || !window.vouc_data) { return false; }

  if (div.style.display != 'block') { var div = createDynamicPopup(id,modal); }

  addBasketPopupContents();

  return div;
}
var c = 0;
//--------------------------------------
function toggleBasketPopup(event) {
  var div = document.getElementById(miniBasketConfig.popupdiv);
  var wasVisible = div && div.style.display == 'block';
  if (wasVisible) {
    hideBasketPopup(event);
  } else {
    showBasketPopup(event, miniBasketConfig.minibasketdiv, 0);
  }
}
function onOpenBasketPopupTimer() {
  showBasketPopupReal(null, openBasketEle)
}
function showBasketPopup(event, ele, time) {
  if (window.responsiveSwitchOk && !responsiveSwitchOk('popupbasket')) { return; }

  if (is_mob) {
    var div = document.getElementById(miniBasketConfig.alink);
    if (div) {
      if (div.onmouseover) {
        div.ontouchstart = div.onmouseover; div.onmouseover = null;
//        document.title = '';
      }
      if (div.onclick) {
//        var ooc = div.onclick+' ';
      }
    }
//    var div = document.getElementById(miniBasketConfig.popupdiv);
//    if (div) { div.setAttribute('isopen','N'); }
  }
  clearTimeout(openBasketPopupTimer);
  clearTimeout(closeBasketPopupTimer);
  openBasketEle = ele;
  basketPopupInterval = (time == undefined ? 0 : time);
  openBasketPopupTimer = setTimeout( 'onOpenBasketPopupTimer()', 300 );
}
function showBasketPopupReal(event, ele) {
  clearTimeout(openBasketPopupTimer);
  clearTimeout(closeBasketPopupTimer);
  if (basketPopupAborted) { return }

  var div = document.getElementById(miniBasketConfig.popupdiv);
  var wasVisible = div && div.style.display == 'block';
//document.title = document.title + '|'+wasVisible+'-'+div.style.display;
  var calledFromBuy = basketPopupInterval > 0;

//  if (wasVisible && ) {
    // its a call from an ajax return and the dropdown is dropped so refresh and start the timer
//    calledFromBuy = true;
//    addBasketPopupContents();
//    refreshBasketPopup();
//    closeBasketPopupTimer = setTimeout( 'onCloseBasketPopupTimer()', basketPopupInterval );
//    basketPopupInterval = 0;
//    return;
//  }
//alert('cc'+wasVisible);
  if (wasVisible && !calledFromBuy) { return } // stop repeated firing of mouseover being a problem

  var div = initBasketPopup();
  if (div && window.vouc_data != undefined && window.vouc_data.length > 0) {
    c++;
    if (!wasVisible) popupInit(hideBasketPopupReal,'xxxx');

    var els = document.getElementById('basketPopupScroll');
    els.style.height = 'auto';
    els.style.overflow = 'visible';

    if (!wasVisible) div.style.left = '-10000px';  // create it off screen so i can make it visible to find its size
//div.style.left = '0px';
    div.style.overflow = 'visible';
    div.style.height = 'auto';
    div.style.display='block';

    if (typeof(ele) == 'string') { var ele = document.getElementById(ele); }

    var xy = getBounds(ele);
    var align = ele.getAttribute('data-align');
    var fixL = ele.getAttribute('data-offsetLeft');
    if (!fixL) { fixL = 0; } else { fixL = fixL-0; }
    var fixT = ele.getAttribute('data-offsetTop');
    if (!fixT) { fixT = 0; } else { fixT = fixT-0; }
    var maxh = ele.getAttribute('data-maxheight');
    if (!maxh) { maxh = 0; } else { maxh = maxh-0; }
    var phonetop = ele.getAttribute('data-phonetop');
    if (responsiveStyle == 'phone' && phonetop) {
      var sw = screenWidth();
      y = yOnPage(phonetop-0,div.offsetHeight);
    } else {
      y = yOnPage(xy.y+xy.h+fixT,0);
    }
//alert(y);
    if (responsiveStyle == 'phone') {
      var sw = screenWidth();
      x = xOnPage((sw-div.offsetWidth) / 2,div.offsetWidth);
    } else if (align == 'R') {
      x = xOnPage(xy.x+fixL+xy.w-div.offsetWidth,div.offsetWidth);  // align right
    } else {
      x = xOnPage(xy.x+fixL,div.offsetWidth+wborder(div)); // align left
    }

    var dxy = getBounds(div);
    var exy = getBounds(els);
    var sxy = screenSize();
    if (maxh > 0 && sxy.h > maxh) { sxy.h = maxh; }  // pretend the screen height is maxh
    if (y + dxy.h > sxy.y + sxy.h) {
      els.style.overflow = 'hidden';
      els.style.overflowY = 'scroll';
      var h = sxy.y + sxy.h - y + exy.h - dxy.h;
      if (h < 20) { h = 20; }
      h = h;
      els.style.height = h + 'px';
    }

    div.style.top = y + 'px';
    div.style.left = x + 'px';
    div.trueleft = x;
    hideSelects();
//document.title = div.offsetHeight+ ' | '+h+' | '+exy.h+' | '+sxy.h;
    div.onmouseover = new Function("keepBasketPopup();");
    div.onmouseout = new Function("hideBasketPopup();");

    if (!wasVisible) {
      rolldown(div, rolldownEnded);
    }

    if (calledFromBuy) { closeBasketPopupTimer = setTimeout( 'onCloseBasketPopupTimer()', basketPopupInterval ); }
    basketPopupInterval = 0;

    if (window.miniBasketConfig) {
      var img = document.getElementById(miniBasketConfig.img);
      if (img) {
        img.src = miniBasketConfig.srchide;
        img.alt = miniBasketConfig.althide;
      }
    }
  }
}
//--------------------------------------
var openBasketPopupTimer = null;
var openBasketEle = null;
var closeBasketPopupTimer = null;
var basketPopupAborted = false;
var basketPopupInterval = 0;
function hideBasketPopupReal(ss) {
  var div = document.getElementById(miniBasketConfig.popupdiv);
//  if (id) { id.style.display='none'; showSelects(); }
  if (div) { rollup(div, rollupEnded); }

  if (window.miniBasketConfig) {
    var img = document.getElementById(miniBasketConfig.img);
    if (img) {
      img.src = miniBasketConfig.srcshow;
      img.alt = miniBasketConfig.altshow;
    }
  }
}
function onCloseBasketPopupTimer() {
  clearTimeout(closeBasketPopupTimer);
  closeBasketPopupTimer = null;
  popupClose('','');
}
function rolldownEnded() {
  var div = document.getElementById(miniBasketConfig.popupdiv);
  if (div) { div.setAttribute('isopen','Y'); }
}
function rollupEnded() {
  showSelects;
  var div = document.getElementById(miniBasketConfig.popupdiv);
  if (div) { div.setAttribute('isopen','N'); }
}
function hideBasketPopup(e) {
//  e = e || window.event; // make sure IE has the event in e
//alert(popupInDiv(e,'miniBasketMain',true)+'xx'+popupInDiv(e,'basketPopup',true)+'yy'+(!popupInDiv(e,'miniBasketMain',true) && !popupInDiv(e,'basketPopup',true)));
//  if (!popupInDiv(e,'miniBasketMain',true) && !popupInDiv(e,'basketPopup',true)) {
    clearTimeout(openBasketPopupTimer);
    closeBasketPopupTimer = setTimeout( 'onCloseBasketPopupTimer()', 500 );
//  }
}
function keepBasketPopup() {
  clearTimeout(closeBasketPopupTimer);
}
function hideBasketPopup_CO(event,miniBasketEle) {
  if (window.responsiveSwitchOk && !responsiveSwitchOk('popupbasket')) { return true; }  // let the click happen if no popup

  // called when mini basket is clicked to go to checkout
  if (is_mob) {
    var div = document.getElementById(miniBasketConfig.popupdiv);
    if (div) {
      var v = div.getAttribute('isopen');
      if (v != 'Y') { return false; }
    }
  }
  basketPopupAborted = true;
  popupClose('','');
  if (miniBasketEle) {
    miniBasketEle.className = miniBasketEle.className + ' clicked';
  }
  return true;
}

function applyCustomCSS(name) {
  if (window.resp_custom_css) {
    var a = resp_custom_css.split('|');
    for (var i in a) {
      var b = a[i].split('->');
      if (b[0] == name) { return b[1]; }
    }
  }
  return name;
}
function setStylesheet(name, linkId){
  if (!linkId || linkId - undefined) { linkId = 'dyncss'; }
  var link = document.getElementById(linkId);
  if (link) {
    if (name == 'none') {
      link.disabled = true;
    } else {
//      link.disabled = true;  // safari 5.1.7 doesnt work when href changes and link.disabled = true
      name = applyCustomCSS(name);
addLog('loading '+name+'.css');
      if (link.disabled) { link.disabled = false; }
      var ver = window.linkver || '';  // add version number if it exists
      if (link.href == undefined) {
        // iphone 4 fix
        var css = document.createElement('link');
        css.rel = 'stylesheet';
        css.media = 'all';
        css.href = thisUrl+'assets/css/'+name+ver+'.css';
        document.getElementsByTagName('head')[0].appendChild(css);
      } else {
        link.href = thisUrl+'assets/css/'+name+ver+'.css';
      }
//      link.disabled = false;
    }
  }
addLog('setstylesheet='+name+ver+' responsivestyle='+responsiveStyle+' screenwidth='+screenWidth());
//  if (window.tidyParts) { tidyParts(); } // reset the heights of any prodpage lists
}

function setFont(size, linkId) {
  if (size == undefined) { size = getXCookie('style'); }
  if (!size || size == undefined) return false;

  if (!linkId || linkId - undefined) { linkId = 'dyncss'; }
  var link = document.getElementById(linkId);
  if (link) {
    if (size == 'def') {
      link.disabled = true;
    } else {
      link.disabled = true;
      link.href = thisUrl+'assets/css/stylesize'+size+'.css';
      link.disabled = false;
    }
  }

  setXCookie('style',size,365,'/','');
  if (window.tidyParts) { tidyParts(); } // reset the heights of any prodpage lists
}

//--------------------------------------
// search / autocomplete section START
//--------------------------------------

// call stopTypeAhead() / starttypeAhead() to switch this function off and on

// local vars
var searchBox = null;  // pointer to the searchbox element
var searchhttp=null;
var searchedValue = '';
var badValue = '';
var typeAheadLive=true;
var searchTimer = null;

function stopTypeAhead() {
  typeAheadLive = false;
  if (window.typeAheadPopup) { typeAheadPopup.style.display = 'none'; }
}

function startTypeAhead() {
  typeAheadLive = true;
}

function showSuggestions(a) {
//alert(a);
  idSel = 0; // reset so down arrow starts at first item
  var id = 'suggestionBox';
  var div = document.getElementById(id);
  if (!div) {
    div = createDynamicPopup(id,false);
 			div.innerHTML+='<div id="sresults" class="ta" name="suggestionresults"></div>';
  }
  popupInit(hideSuggestionsCore,'!suggestionBox');

// put the shadow png around the box
//  a = '<div id="suggestionT"><!--xx--></div><div id="suggestionM">' + a + '</div><div id="suggestionB"><!--xx--></div>';
  a = '<table cellspacing="0" cellpadding="0" border="0"><tr><td class="tl" style="background-position: 0 0;">'+a;
  a+= '</td><td class="tr" style="background-position: right 0;">&nbsp;<!--x--></td></tr><tr><td class="bot" style=" background-position: left bottom;" ><!--x--></td><td class="bot" style=" background-position: right bottom;"><!--x--></td></tr></table>';

  el = document.getElementById('sresults');
  el.innerHTML = a;

  div.style.display='block';
  var xy = getBounds(searchBoxId);
  y = yOnPage(xy.y+xy.h+4,div.offsetHeight);
  x = xOnPage(xy.x,div.offsetWidth);

  div.style.top = y + 'px';
  div.style.left = x + 'px';
  hideSelects();
//  showShadowBox(div);
}

// using up and down keys to go up and down autocomplete list
// if on keypress there is nothing with id item* then do nothing
var idSel = 0;
var keyPressed;
document.onkeydown = function(event) {
  var itId = document.getElementById('sresults');
  event = event || window.event;
  var key = (event.keyCode ? event.keyCode : event.which);
  switch (key) {
    case 38: // up arrow
      // check if there is an active element and its item*
      if(itId){
        idSel = (idSel-1); if(idSel<1)idSel=0;
        if(idSel<1){ return false; }
        var el = document.getElementById("item"+idSel);
        if(el){ el.className = ""; }
        var el2 = document.getElementById("item"+(idSel-1));
        if(el2){ el2.className = "selected"; }
        var els = document.getElementById("searchwhat");
        if((els)&&(el)){ els.value = searchedValue = el2.getAttribute('data-content').stripslashes(); }
        return false;
      }
      break;
    case 40: // down arrow
      // check if there is an active element and its item*
      if(itId){
        if(sug_numResults == (idSel)){ return false; }
        var el = document.getElementById("item"+idSel);
        if(el){ el.className = "selected"; }
        var elm = document.getElementById("item"+(idSel-1));
        if(elm){ elm.className = ""; }
        var els = document.getElementById("searchwhat");
        if((els)&&(el)){ els.value = searchedValue = el.getAttribute('data-content').stripslashes(); }
        idSel++;
        return false;
      }
      break;
    case 13:
      if(itId){
        var el = document.getElementById("item"+(idSel-1));
        if(el){
          el.click();
          return false;
        } else {
          var el = document.getElementById("searchSingle");
          if (el) { el.submit(); }
        }
      }
      break;
    default:
      if(!itId){ idSel=0; }
      break;
  }
};

//--------------------------------------
var canSearchSugg = true;

function hideSuggestions() {
  popupClose('','');
}
function hideSuggestionsCore() {
  var id = document.getElementById('suggestionBox');
  if (id) { id.style.display='none'; showSelects(); }
//  searchedValue = '';
//  hideShadowBox();
}

// addslashes
function addslashes( str ) {
    return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
}

// stripslashes
String.prototype.stripslashes = function(){
    return this.replace(/\\(.)/mg, "$1");
}

// declaring number of results returned
var sug_numResults;
function getSearchRes() {
  if (!typeAheadLive) { return; }
  if(searchhttp.readyState==4 && searchhttp.status==200) {
    canSearchSugg = true;
    res = searchhttp.responseText;
//alert(res);
    if (res) {
      var aa ='', last = '';
      var pts = res.split('|^^|');
      sug_numResults = (pts.length - 1);
      for (var i=0; i<pts.length; i++) {
        if (trim(pts[i])) {
          var pt = pts[i].split('||');
          f=searchedValue;
          if (pt[3] == 'X' && last == 'P') {  // moving into the search_specials list so add a header
            aa = aa+'<li class="head"><span>'+getTranslatedText('tt_srch_specials','Other pages matching your search:')+'</span></li>';
          }
          last = pt[3];
          var s1 = (pt[2] ? pt[2] : 'products');
          var L_descr = pt[1].toLowerCase();
          var L_f = f.toLowerCase(f);
          var p = L_descr.indexOf(L_f);
          var s = pt[1].substr(p,searchedValue.length);
          var d='<span class="match">'+s+'</span>';
          var dataC = addslashes(pt[1]);
          pt[1]=pt[1].replace(new RegExp(searchedValue,"ig"),d)
          if (last == 'P') {  // sp link
            //SSL
            //aa = aa+'<li><a data-content="'+dataC+'" id=item'+i+' href="'+thisUrl.replace('https:','http:')+'sp'+delim404+s1+delim404+pt[0]+'?tyah=y" class="" onclick="popupClose(\'\',\'\');">'+pt[1]+'</a></li>';
            aa = aa+'<li><a data-content="'+dataC+'" id=item'+i+' href="'+thisUrl.replace('https:',def_proto)+'sp'+delim404+s1+delim404+pt[0]+'?tyah=y" class="" onclick="popupClose(\'\',\'\');">'+pt[1]+'</a></li>';
          }
          if (last == 'X') {  // search_specials link
            if (pt[1].indexOf(/http/ig) > -1) {
              var link = pt[1];
            } else {
              //SSL
              //var link = thisUrl.replace('https:','http:')+pt[0];
              var link = thisUrl.replace('https:',def_proto)+pt[0];
            }
            aa = aa+'<li><a data-content="'+dataC+'" id=item'+i+' href="'+link+'" class="" onclick="popupClose(\'\',\'\');">'+pt[1]+'</a></li>';
          }
        }
      }
    }
    if (!aa) {
      // tt_srch_not_found div is in include_translation_text
//      a=getTranslatedText('tt_srch_not_found','<br />&nbsp;&nbsp;no suggestions found<br /><br />press <b>Enter</b> for a <br /> &nbsp;more thorough search<br /><br />');

      // a = '<br />&nbsp;&nbsp;'
      //   + getTranslatedText('tt_srch_not_found_1','no suggestions found')
      //   + '<br /><br />&nbsp;&nbsp;'
      //   + getTranslatedText('tt_srch_not_found_2','press')
      //   + ' <b>'
      //   + getTranslatedText('tt_srch_not_found_3','Enter')
      //   + '</b> '
      //   + getTranslatedText('tt_srch_not_found_4','for a')
      //   + ' <br />&nbsp;&nbsp;'
      //   + getTranslatedText('tt_srch_not_found_5','more thorough search')
      //   + '<br /><br />';
      a = '<div style="width:100%;padding:10px;">No suggestions found.<br /><br /><strong>Submit search</strong> to view results.</div>'
      badValue = searchedValue;
    } else {
      a = '<ul>'+aa+'</ul>';
      badValue = '';
    }
    showSuggestions(a);
  }
}

function getSuggestions() {
  try {   // The following "try" blocks get the XMLHTTP object for various browsers
     searchhttp = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (e) {
    try {
      searchhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (e2) {
      // This block handles Mozilla/Firefox browsers...
      try {
        searchhttp = new XMLHttpRequest();
      } catch (e3) {
        searchhttp = false;
      }
    }
  }
  if(searchhttp!=null) {
    var fixer = new Date();
    searchhttp.open("GET",thisUrl+"typeAhead.asp?searchwhat="+searchedValue+'&when='+fixer.getTime(),true);
    searchhttp.onreadystatechange=function(){ getSearchRes(); };
    searchhttp.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2005 00:00:00 GMT");
    searchhttp.send(null);
    canSearchSugg = false;  // stop it searching again until this one has replied
  }
}

function doSearch(c) {
  clearTimeout(searchTimer);
  searchTimer = setTimeout(doSearchTimer, 400 );
//  doSearchTimer();
}
function doSearchTimer() {
  if (!typeAheadLive) { return }
  if (canSearchSugg) {
    var def = searchBox.getAttribute('data-def');
    var dv = getTranslatedText('tt_srch_def_value','Search');
    if (dv && dv == searchBox.value || def && def == searchBox.value) { searchBox.value = ''; return; }
    var a = trim(searchBox.value);
    var nosp = trim(a.replace(/ /ig,''));
    if (nosp.length < 3) { a = ''; }
    if (!a) { hideSuggestions(); searchedValue = ''; return }
    if (searchedValue != a) {
      searchedValue = a;
      badValue = '';
      getSuggestions();
    }
  } else {
  searchTimer = setTimeout(doSearchTimer, 400 );
  }
}

function searchSuggKeyDown(c) {
  c = c || window.event;
  var a = (c.keyCode ? c.keyCode : c.which);
  if (a==191) { if (window.event) { event.returnValue = false; } return false; } // ?mark
  return true
}

function initSearchBox() {
  window.focus(); // make sure the search box isnt focused when the page loads

  if (!is_mob && window.innerWidth >= 990) {
    searchBoxId = 'searchwhat';  // the id of the search input box
    //console.log(searchBoxId);
    searchBox = document.getElementById(searchBoxId);
  } else {
    searchBoxId = 'searchphone';
    //console.log(searchBoxId);
  searchBox = document.getElementById(searchBoxId);
  }

  if(searchBox){
//		searchBox.onkeypress = function(c) { return editKeyPress(c)};
    searchBox.onkeyup = function(c) { if((keyPressed==38) || (keyPressed==40)){ return; } else { return doSearch(c); } }
    searchBox.onfocus = function(c) { return doSearch(c) };
    searchBox.onkeydown = function(c) { return searchSuggKeyDown(c) };
  }
}


//--------------------------------------
// search / autocomplete section END
//--------------------------------------

//--------------------------------------
// form prepare, initialisation & validation START
//--------------------------------------

// *********** pre-post form data checking stuff
// to use this all you do is add ' onsubmit="return checkData(this);" ' to the form definition
// and add data-man="" to each input, select or textarea you want to be mandatory.
// data-man can have other values to add extra control to the pre-post checks
// multiple controls are separated by | eg "minl=5|maxl=20|blankok"
// "email" does a check for a valid email address
// "minl" and "maxl" control the min and max length
// "minv" and "maxv" control the min and max values
// "pwd" sets the min and max length to 6 and 20 and stops ' and " being entered
// "cc" does a basic credit card modulus check with min and max lengths of 13 and 19
// "confirm=xxx" will check that the value is the same as the value in the element with id="xxx"
// "novalue=nowt,none" will treat 'nowt' and 'none' as being the same as '' (comma delimited list)
//     -- use this if the default or blank value of a drop down is somthing other than ''
// "blankok" allows restrictions to be set but only checked if the value is not '' eg password change
// "noquote" prevents ' being entered
// "nodblquote" prevents " being entered
// "ukonly=country" country is the id of the country selector. Use this on a postcode field to insist
//    on a postcode, only, if the country code is in the 'postcodeCountries' array
// "oldpw" makes sure that the entered password matches the encrypted old password in:
//    <input type="hidden" name="sctot" id="sctot" value="<elucid_enc_pw>">
// "group=1" will only apply restrictions if a field with "group=1" has data in it
// "CIcheck=country" will force Channel islands in the 'country' field if a CI postcode is entered
//    or force UK if country = CI and postcode is not.
// "usca" will check for a 2 character State code if the country is US or CA.
// "urlok" will allow a url to be entered.
// "one_of_group=myfunc" will insist that at least one of the group of elements is entered.
//    function myfunc() must exist and needs to show an error message popup if the group is empty.

function isEmail(s) {
  return s.match(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,20}$/i) != null;
}
function ccCheck(number) {
  var number=number.replace(/ /g, '');  // Strip any spaces
  var number_length=number.length;
  var parity=number_length % 2;
  var total=0;
  for (i=0; i < number_length; i++) {
    var digit=number.charAt(i);
    if (i % 2 == parity) { digit=digit * 2;  if (digit > 9) { digit=digit - 9; } }
    total = total + parseInt(digit);
  }
  return ((total % 10 == 0) && (number_length >= 13) ? true : false);
}
var checkEleError = null;
var checkErrTimer = null;
var checkErrVal = '';
var checkErrEle = null;
var checkIDCounter = 0;

function showErrUnder(el) {

  var str = el.parentNode.classList;
  var parent;
  var elID = el.getAttribute('id');

  // find out what the parent is
  if(el.getAttribute('data-holder')){ // if select has data-holder then thats parent
    parent = document.getElementById(el.getAttribute('data-holder'));
    //dh only show one error at a time in an error holder
    var diva = parent.getElementsByTagName("div");
    for (var i=0; i < diva.length; i++ ) {
      if (diva[i].id.substr(0,10) == 'errorUnder') {
        diva[i].style.display = 'none';
      }
    }
  } else if(str=="selectBorder"){ // if parent class is selectBorder then we know its a single select
    parent = el.parentNode.parentNode;
  } else { // else get parent
    parent = el.parentNode;
  }

  var ele2d = document.getElementById('errorUnder'+elID);
  if(ele2d)ele2d.outerHTML = ''; // delete error for redraw so count can work if needed

  var checkEleError = document.createElement('div');
  checkEleError.setAttribute('id','errorUnder'+elID);
  checkEleError.setAttribute('class','errorUnder');
  checkEleError.innerHTML = el.stat;
  parent.appendChild(checkEleError);

}

function hideErrUnder(el) {
  // check error is there then get rid
  var elID = el.getAttribute('id');
  var ele = document.getElementById('errorUnder'+elID);
  if(ele)ele.style.display = 'none';
}

function showErr(el) {

  if (!checkEleError) {
    checkEleError = createDynamicPopup('errorPopup','','errorPopup');
    var s = '<table cellspacing="0" cellpadding="0" border="0"><tr>';
    s += '<td id="ceeMain" style="padding:14px 0 0 20px; background-position: 0 0;"></td>';
    s += '<td style="width:20px; height:20px; background-position: right 0;"><!--x--></td>';
    s += '</tr><tr>';
    s += '<td class="bot" style=" background-position: 0 bottom;"><!--x--></td>';
    s += '<td class="bot" style=" background-position: right bottom;"><!--x--></td>';
    s += '</tr></table>';
    checkEleError.innerHTML = s;
  } else {
    globZindex++;
    checkEleError.style.zIndex = globZindex;
  }

  var tmp = document.getElementById('ceeMain');
  tmp.innerHTML = el.stat;
  checkEleError.style.display = 'block';
  var scr = screenSize();
  var exy = getBounds(el);
  var exe = getBounds(checkEleError);

  checkEleError.style.top = exy.y - exe.h+1 + 'px';
  checkEleError.style.left = exy.x - 35 + 'px';
  hideSelects(el);

  if (exy.y-exe.h < scr.y) { // make sure the box is visible by scrolling the page
    var yoffs = exy.y-exe.h-scr.y;
    window.scrollBy(0,yoffs);
  }
}
function hideErr(el) {
  if (checkErrTimer) { clearTimeout(checkErrTimer); }
  if (checkEleError) { checkEleError.style.display = 'none'; }
  showSelects();
}
function checkErrOnTimer(tim) {
  var ele = checkErrEle;
//document.title = ele.tagName+' | '+ele.value+' | '+checkErrVal;
  if (ele.value != checkErrVal || ele.type == 'radio') {
    checkEle(ele, 1);
  } else {
    checkErrTimer = setTimeout( "checkErrOnTimer(this)", 1000 );
    checkErrTimer.ele = ele;
  }
}
function hasNoValue(val, novalue) {
  val = val.toLowerCase();
  if (val === '') { return true; }
  if (novalue) {
    for (var i=0; i<novalue.length; i++) {
      if (novalue[i].toLowerCase() == val.toLowerCase()) { return true; }
    }
  }
  return false;
}
function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}
function getRadioValue(name) {
  var els = document.getElementsByName(name);
  for (var i=0; i<els.length; i++) {
    if (els[i].checked) { return els[i].value; }
  }
  return '';
}
function setRadioValue(name, value) {
  var els = document.getElementsByName(name);
  for (var i=0; i<els.length; i++) {
    els[i].checked = (els[i].value == value);
  }
}
var checkDataGroupData = new Array;
var checkDataGroup = new Array;
function groupIsEmpty(group,form,grp_id) {

  if (!window.checkDataGroup[group]) {
    checkDataGroup[group] = '';
    var eles = form.elements;

//    console.log(eles);
    for (var i=0; i<eles.length; i++) {
      var man = eles[i].getAttribute('data-man');
      if (man && (man.indexOf(grp_id+'='+group) > -1)) {
        checkDataGroup[group] += eles[i].id + ',';
      }
    }
  }
  checkDataGroupData[group] = '';
  var fields = checkDataGroup[group].split(',');
  for (var i=0; i<fields.length; i++) {
    if (fields[i]) {
      var ele = document.getElementById(fields[i]);
      if (ele) {
        checkDataGroupData[group] += trim(ele.value);
      } else {
        alert(fields[i]+' is in checkDataGroup['+group+'] but doesnt exist on the form');
      }
    }
  }
//  showDebug('x= '+checkDataGroupData[group]+' <br>',true);
  return checkDataGroupData[group] == '';
}
var one_of_group_func = null;
var postcodeCountries = 'UK,GB,JE,GY';  // override by copying to custom.js if needed
var CIcheck = { 'match':'(gy|je)[ ]*([0-9])', 'country':'ZZ', 'notCI':'GB' }; // override by copying to custom.js if needed

function blurOnDataMan() {
  var inputs = document.getElementsByTagName('input');
  var selects = document.getElementsByTagName('select');
  var textareas = document.getElementsByTagName('textarea');

  for (var i = 0; i < inputs.length; i++){
    var elg = inputs[i].getAttribute('data-man');
    if(elg!=null){ // if it has data-man then add onblur
      addEvent(inputs[i],'blur.dataman',function(ev) { checkEle(this,0,'','bl') } );
//      inputs[i].setAttribute('onblur.dataman','checkEle(this)');
    }
  }

  for (var i = 0; i < selects.length; i++){
    var elg = selects[i].getAttribute('data-man');
    if(elg!=null){ // if it has data-man then add onblur
      addEvent(selects[i],'blur.dataman',function(ev) { checkEle(this,0,'','bl') } );
//      selects[i].setAttribute('onblur.dataman','checkEle(this)');
    }
  }

  for (var i = 0; i < textareas.length; i++){
    var elg = textareas[i].getAttribute('data-man');
    if(elg!=null){ // if it has data-man then add onblur
        addEvent(textareas[i],'blur.dataman',function(ev) { checkEle(this,0,'','bl') } );
//        textareas[i].setAttribute('onblur','checkEle(this)');
    }
  }
}

function checkEle(ele, from, form, src, wasok) {

//alert(whoCalledMe());
  if (typeof(ele) == 'string') { ele = document.getElementById(ele); }
  if (meFocussing) { meFocussing = false; return true; }
  if (checkErrTimer) { clearTimeout(checkErrTimer); }
  var ok = true;
  if (wasok == undefined) { wasok = true; }
  var man = ele.getAttribute('data-man');
  var par = ele.getAttribute('data-man-par');
  if (par) {
    ele = document.getElementById(par);
    var man = ele.getAttribute('data-man');
  }

  if (!form && ele.form) { form = ele.form; }  // dh in case a group scan is needed
  if (from == undefined) { from = 1; }  // dh assume single item check
///showDebug('from='+from+' ele='+ele.id+'<br>',true);
  var val = trim(ele.value);
  if (man != undefined && val != undefined) {
    var minl = 0; var maxl = 9999; var minv = -999999999; var maxv = 999999999;
    var noquote = false; nodblquote = false; countrySel = null;  CIcountrySel = null;  oldpw = false; uscaState = false;
    var confirm = '';  var novalue = ''; var isval = false; var blankok = false; group = ''; nospace = false;
    var customfunc = null; var one_of_group = ''; var urlok = false; var email = false, cc = false, phone = false, comments = false;
    var mana = man.split('|');
    for (var j=0; j<mana.length; j++) {
      var manj = mana[j].split('=');
      if (manj[0] == 'cc') { minl = 13; maxl = 19; }
      if (manj[0] == 'pwd') { minl = 6; maxl = 20; noquote = true; nodblquote = true; nospace = true;}
      if (manj[0] == 'noquote') { noquote = true; }
      if (manj[0] == 'nodblquote') { nodblquote = true; }
      if (manj[0] == 'minl') { minl = manj[1]-0; }
      if (manj[0] == 'maxl') { maxl = manj[1]-0; }
      if (manj[0] == 'minv') { isval = true; minv = manj[1]-0; }
      if (manj[0] == 'maxv') { isval = true; maxv = manj[1]-0; }
      if (manj[0] == 'confirm') { confirm = document.getElementById(manj[1]); }
      if (manj[0] == 'novalue') { novalue = manj[1].split(','); }
      if (manj[0] == 'blankok') { blankok = true; }
      if (manj[0] == 'ukonly') { countrySel = document.getElementById(manj[1]); postcodeCountries += ','; }
      if (manj[0] == 'CIcheck') { CIcountrySel = document.getElementById(manj[1]);  }
      if (manj[0] == 'oldpw') { oldpw = true; }
      if (manj[0] == 'group') { group = manj[1]; }
      if (manj[0] == 'one_of_group') { one_of_group = manj[1]; blankok = true; }
      if (manj[0] == 'customfunc') { customfunc = manj[1]; }
      if (manj[0] == 'usca') { uscaState = true; countrySel = document.getElementById((manj[1] ? manj[1] : 'country'));}
      if (manj[0] == 'urlok') { urlok = true; }
      if (manj[0] == 'email') { email = true; }
      if (manj[0] == 'cc') { cc = true; }
      if (manj[0] == 'phone') { phone = true; }
      if (manj[0] == 'comments') { comments = true; }
    }
    var errstr = '';

    if (customfunc) {
      var ss = 'var res = '+customfunc+'(\''+ele.id+'\')';
      eval(ss);
      if (!res.ok) { ok = false; errstr += res.errstr }
    }
//alert(group);
    if ((from == 0 || checkData_immediate) && group && groupIsEmpty(group,form,'group')) {  // from = 0 = batch check
    }
    else if (blankok && hasNoValue(val,novalue)) {
    }
    else if (confirm == undefined || (confirm && (confirm.value == undefined))) {
      // if the confirm element doesnt exist show a message to programmer
      ok = false; errstr += 'confirm id does not exist (programmer)';
    }
    else if (confirm) {
      if(!val) { ok = false; errstr += getTranslatedText('tt_checkdata_enter','Please enter a value')+'<br>'; }
      if (confirm.value != val) { ok = false; errstr += getTranslatedText('tt_checkdata_confirm','Confirmation value does not match')+'<br>'; }
    }
    else if (oldpw) {

      if (val == '') { ok = false; errstr += '88'+getTranslatedText('tt_checkdata_oldpw','Please enter your current password')+'<br>'; }
      else {
        var pwel = document.getElementById('sctot');
        if (pwel) {
          var scpw = val.toUpperCase();
          var pwht = 0;
          for (var xi=0; xi < scpw.length; xi++) { pwht += scpw.charCodeAt(xi) * (1+Math.pow(xi*256,2)); }
          if (pwht != pwel.value) { ok = false; errstr += getTranslatedText('tt_checkdata_matchpw','The password you entered does not match your old password')+'<br>'; }
        } else {
          ok = false; errstr += 'sctot not found on page (programmer)';
        }
      }
    }
    else if (uscaState) {
       if (!countrySel) {
         errstr += 'countRy field not found (programmer)';
         ok = false;
       } else {
         if (countrySel.value == 'US' && val.length != 2) {
        	  errstr += getTranslatedText('tt_checkdata_statecode','Please enter a 2 character State code.')+'<br>';
           ok = false;
         }
       }
    }
    else {
//if (man == 'email') { var xx = 'ab %1 cd'; alert(xx.replace(xx
      if (cc && !ccCheck(val) && (val)) {
        ok = false; errstr += getTranslatedText('tt_checkdata_ccno','Credit Card number is invalid')+'<br>';
      } else if (cc && (!val)) {
        ok = false; errstr += getTranslatedText('tt_checkdata_enter','Please enter a value')+'<br>';
      }
      if (email && !isEmail(val) && (val)) {
        ok = false; errstr += getTranslatedText('tt_checkdata_email','Please enter a valid email address')+'<br>';
      } else if (email && (!val)) {
        ok = false; errstr += getTranslatedText('tt_checkdata_enter','Please enter a value')+'<br>';
      }
      if (phone && !isPhone(val)) { ok = false; errstr += getTranslatedText('tt_checkdata_phone','Phone number is not valid, [1] numbers expected (0-9)', {'[1]':minDigitsInIPhoneNumber })+'<br>'; }
      if (val.length < minl) { ok = false; errstr += getTranslatedText('tt_checkdata_minl','Minimum length is [1] characters ([2] Entered)',{ '[1]':minl, '[2]':val.length })+'<br>'; }
      if (val.length > maxl) { ok = false; errstr += getTranslatedText('tt_checkdata_maxl','Maximum length is [1] characters ([2] Entered)',{ '[1]':maxl, '[2]':val.length })+'<br>'; }
      if (nospace && (val.indexOf(" ") > -1)) { ok = false; errstr += getTranslatedText('tt_checkdata_spaces','Spaces are not allowed')+'<br>'; }
      if (noquote && (val.indexOf("'") > -1)) { ok = false; errstr += getTranslatedText('tt_checkdata_quote1',"The single quote character (') is not allowed")+"<br>"; }
      if (nodblquote && (val.indexOf('"') > -1)) { ok = false; errstr += getTranslatedText('tt_checkdata_quote2','The double quote character (") is not allowed')+'<br>'; }
      if (isval && (val-0 < minv || val-0 > maxv || !isNumeric(val))) { ok = false; errstr += getTranslatedText('tt_checkdata_range','Please enter a number between [1] and [2]', {'[1]':minv, '[2]':maxv})+'<br>'; }

      if (errstr == '' && hasNoValue(val, novalue)) {
        var b_ok = false
        if (countrySel && postcodeCountries.indexOf(countrySel.value+',') == -1 ) { b_ok = true; }
        if (!b_ok) {
          ok = false;
          if (ele.tagName == 'SELECT') {
            errstr += getTranslatedText('tt_checkdata_select','Please select a value')+'<br>';
          } else {
            errstr += getTranslatedText('tt_checkdata_enter','Please enter a value')+'<br>';
          }
        }
      } else {
        // something entered
        if (CIcountrySel) {
          var matchit = new RegExp(CIcheck.match, "i")
          if (val.match(matchit) && CIcountrySel.value != CIcheck.country) {
            CIcountrySel.value = CIcheck.country;
            //PM SERV-3198
            //var cimes = getTranslatedText('tt_checkdata_ci','Channel Islands has been auto selected to match your Channel Islands postcode')+'<br>';
            var cimes = getTranslatedText('tt_checkdata_ci','Channel Islands has been auto selected to match your Channel Islands postcode');
            if (from == 0) { alert(cimes); }
            errstr += cimes;
            ok = false;
          } else if (!val.match(matchit) && CIcountrySel.value == CIcheck.country) {
            CIcountrySel.value = CIcheck.notCI;
            //PM SERV-3198
            //var cimes = getTranslatedText('tt_checkdata_ci','Channel Islands has been auto selected to match your Channel Islands postcode')+'<br>';
            var cimes = getTranslatedText('tt_checkdata_ci','Channel Islands has been auto selected to match your Channel Islands postcode');
            if (from == 0) { alert(cimes); }
            errstr += cimes;
            ok = false;
          }
        }
      }
    }

    //PM JB-467
    if (!comments) {
      if (/[^\u0000-\u00fe]/.test(val)) {
        ok = false; errstr = getTranslatedText('tt_checkdata_badchars','Text contains unsupported characters')+'<br>';
      }
    }
//var str = ''; for (var i = 0; i < val.length; i++) {  str += ' '+val.charCodeAt(i); } if (ele.id = 'from_name') { alert(ele.id+ str); }

    if (!urlok) {
      var rex = new RegExp("((http://|https://|ftp://|file://)|(www\\.|ftp\\.))[-A-Z0-9+&@#/%=~_|$?!:,.]*[A-Z0-9+&@#/%=~_|$]", "i")
      var filt = val.match(rex);
      if (filt) { ok = false; errstr += getTranslatedText('tt_checkdata_url','Please remove the Web Site Address from the text.')+'<br>'; }
  //    document.title = val + 'xx'+ filt;
    }

    if (ele.tagName == 'INPUT' && ele.type.toUpperCase() == 'CHECKBOX') {
      if (!ele.checked) {
        ok = false; errstr += getTranslatedText('tt_checkdata_cbmustsel','This must be selected')+'<br>';
      }
    }

    if (ele.tagName == 'INPUT' && ele.type.toUpperCase() == 'RADIO') {
      var radval = getRadioValue(ele.name);
      if (radval == '') {
        ok = false; errstr += getTranslatedText('tt_checkdata_radmustsel','A value needs to be selected')+'<br>';
      }
    }
///showDebug('ele='+ele.name+' '+ok+' '+wasok+' '+one_of_group+'<br>',true);
    if (ok && wasok) {  // only do test if no other problem was found
      if (from == 0 && one_of_group && groupIsEmpty(one_of_group,form,'one_of_group')) {  // from = 0 = batch check
        one_of_group_func = one_of_group;
        ok = false;
      }
    }

    var bdrel = ele;
    var bckel = ele;
    // IE wont do select border so set the parents border instead
    // DSDS this no longer relevant as selects are treated equally, as well they should be, we all should!
    //if (ele.tagName == 'SELECT' || (ele.tagName == 'INPUT' && ele.type.toUpperCase() == 'RADIO')) {  bdrel = ele.parentNode; }
    if (ele.tagName == 'INPUT' && ele.type.toUpperCase() == 'RADIO') { bckel = ele.parentNode; }

    if (!ok) {
      if (ele.done != true) {
        ele.done = true;
        ele.oldbwid = cssStyle(bdrel,'borderTopWidth');
        ele.oldbstyle = cssStyle(bdrel,'borderTopStyle');
        ele.oldbcolor = cssStyle(bdrel,'borderTopColor');
        ele.oldbgcolor = cssStyle(bckel,'backgroundColor');
        ele.oldbgimage = cssStyle(bckel,'backgroundImage');
        ele.oldcolor = cssStyle(ele,'color');
        var pos = getBounds(ele);
        if (ele.tagName == 'INPUT' && ele.type == 'radio') {
          bdrel.onkeyup = new Function("checkEle(this,1,'','ku')");
          bdrel.onmouseup = new Function("checkEle(this,1,'','mu')");
          bdrel.onchange = new Function("checkEle(this,1,'','ch')");
          addEvent(bdrel,'blur.dataman', function(ev) { checkEle(this,0,'','bl') } );
//          bdrel.onblur = new Function("checkEle(this,0,'','bl')");
          bdrel.onfocus = new Function("checkEle(this,1,'','fo')");
          if (!ele.id) { checkIDCounter++; ele.id = 'AutoGenId'+checkIDCounter; }
          bdrel.setAttribute('data-man-par',ele.id);

          // set up all the radio buttons
/*          var rads = ele.form.getElementsByTagName ('input');
          if (rads) {
            for (var i = 0; i < rads.length; ++i) {
              if (rads[i].type == 'radio' && rads[i].name == ele.name) {
                ele.onkeyup = new Function("checkEle(this,1,'','ku')");
                ele.onmouseup = new Function("checkEle(this,1,'','mu')");
                ele.onchange = new Function("checkEle(this,1,'','ch')");
                ele.onblur = new Function("hideErr(this); checkEle(this,0,'','bl')");
                ele.onfocus = new Function("checkEle(this,1,'','fo')");
                ele.setAttribute('data-man-par',ele.id);
              }
            }
          }
*/
        } else {
          ele.onkeyup = new Function("checkEle(this,1,'','ku')");
          ele.onchange = new Function("checkEle(this,1,'','ch')");
          switch(checkData_errorPlacement) {
            case 'below':
                addEvent(ele,'blur.datamanErr',function(ev) { hideErrUnder(this); } );
//                ele.onblur = new Function("hideErrUnder(this); checkEle(this,0,'','bl')");
                break;
            case 'popup':
                addEvent(ele,'blur.datamanErr',function(ev) { hideErr(this); } );
//                ele.onblur = new Function("hideErr(this); checkEle(this,0,'','bl')");
                break;
          }
          ele.onfocus = new Function("checkEle(this,1,'','fo')");
        }
      }
      ele.stat = errstr;
      bdrel.style.border = '1px solid #a44444';
///showDebug(errstr+'<br>',true);
      switch(checkData_errorPlacement) {
        case 'below':
            showErrUnder(ele);
            break;
      }

      if (ele.tagName == 'SELECT') {
        // cross will appear under the dropdown arrow on selector
      } else {
        bckel.style.background = 'url(assets/images/redcross2.gif) right 2px no-repeat #fff';
      }

      if (from == 1) {
        switch(checkData_errorPlacement) {
          case 'popup':
            showErr(ele);
            break;
        }
        checkErrVal = val;
        checkErrTimer = setTimeout( "checkErrOnTimer(this)", 1000 );
        checkErrEle = ele;
      }
    }
    else if (ele.done == true) {
      bdrel.style.border = ele.oldbwid + 'px ' + ele.oldbstyle + ' ' + ele.oldbcolor;
      bckel.style.backgroundImage = ele.oldbgimage;
      bckel.style.backgroundColor = ele.oldbgcolor;
      // if all conditions met, hide error
      switch(checkData_errorPlacement) {
        case 'below':
          hideErrUnder(ele);
          break;
        case 'popup':
          hideErr(ele);
          break;
      }
    }
  }
// document.title = ele.id+' | '+man+' | '+src+' | '+val+' | '+radval+' | '+ok+' | '+from+' | '+errstr;
  return ok;
}

function checkData(form) {
  if (!fprep_done) { return false; }
  if (typeof(form) == 'string') { form = document.getElementById(form); }
  var ok = true;
  var gotox = false;
  one_of_group_func = null;
  if (form) {
    checkDataGroupData.length = 0;
    var eles = form.elements;

//    console.log(eles.length);

    for (var i=0; i<eles.length; i++) {
      var ele = eles[i];
      if (ele.name && ele.offsetWidth) { //dh addition to stop invisible elements from being tested eg cc number from GV page
       	var res = checkEle(ele, 0, form, '',ok);
        if (!res) { ok = false; if (!gotox) { gotox = ele; } }
      }
    }
  }
  if (!ok) {
    var el = gotox.parentNode;
    while(el != null && el.tagName != undefined) {
      var proc = el.getAttribute('data-man-tab');
      if (proc) {
        eval(proc);
        break;
      }
      el = el.parentNode;
    }
    try { gotox.focus(); } catch(err) { };
  }
  if (ok) { // F5 issue - if postedc.value > 1 then asp can tell if the page has already been posted
    if (form.postedc) {
      var val = form.postedc.value;
      val++;
      form.postedc.value = val;
    }
    initFrm(form);
  }
  if (one_of_group_func) {
    var s = 'window.'+one_of_group_func+'()';
    try { eval(s); } catch(err) { alert('checkData error: could not call '+one_of_group_func+' function'); }
    ok = false;
  }
    return ok;
}

function initFrm(form) {
  var p = form.getAttribute('data-xparams');
  if (p) {
    if (form.action.match(/postdata\.asp/ig)) {

      var lc = form.getAttribute('data-lc');
      var fixer = new Date();
    	 var now = fixer.getTime();
    	 var diff = 10;
      var el = document.getElementById('loadctrl');
      if (el) {
        diff = (now-el.value) / 1000;
      } else {
        diff = (now-loadmark) / 1000;
      }
//    alert(el.value+' '+loadmark+' '+diff+'<br>');

      if (diff >= 1 || (lc == 'x')) {
        var callback = form.getAttribute('data-ajax-callback'); // does it need prep for ajax?
        if (callback) {
          if (!form.id) { alert('message from initFrm function -- The form MUST have an id to do an ajax post'); }
          form.setAttribute('data-ajax-action',p.replace(/dotext/ig,'.asp').replace(/\.ext/ig,'.asp'));
          form.action = 'javascript:postAjax(\''+form.id+'\', '+callback+')';
          form.setAttribute('data-ajax-callback',null);
          doUnloadPopup = false;
        } else {
          form.action = p.replace(/dotext/ig,'.asp');
        }
      }
    }
  } else {
    var callback = form.getAttribute('data-ajax-callback'); // does it need prep for ajax?
    if (callback) {
      if (!form.id) { alert('message from initFrm function -- The form MUST have an id to do an ajax post'); }
      form.setAttribute('data-ajax-action',form.action);
      form.action = 'javascript:postAjax(\''+form.id+'\','+callback+')';
      form.setAttribute('data-ajax-callback',null);
      doUnloadPopup = false;
    }
  }
}

var fprep_done = false;
function prepFrm() {

  if (checkData_immediate){ // if client wants error checking on blur set checkData_blurOnDataMan at top of common
    blurOnDataMan();  // assign all the blur handlers
  }

  var frms = document.getElementsByTagName("FORM");
  for (var m = 0; m < frms.length; ++m) {

    var form = frms[m];
    var ex = form.getAttribute('data-xempt');
    if (!ex) {
      var inp = document.createElement('INPUT');
      inp.setAttribute('name','v_'+'xx_'+'fo'+'rm');
      inp.setAttribute('type','hidden');
      inp.setAttribute('value','y');
      form.appendChild(inp);
    }
/*
    var callback = form.getAttribute('data-ajax-callback');
    if (callback) {
      var p = form.getAttribute('data-xparams');
      if (p && form.action.match(/postdata\.asp/ig)) {
        form.setAttribute('data-ajax-action',p.replace(/dotext/ig,'.asp'));
        form.action = 'javascript:postAjax(\''+form.id+'\','+callback+')';
      }
      else if (!p) {
        form.setAttribute('data-ajax-action',form.action);
        form.action = 'javascript:postAjax(\''+form.id+'\','+callback+')';
      }
    }
*/
  }
  fprep_done = true;
}

function addHiddenInput(form,el_name,el_value) {
//alert(el_name+'|'+el_value);
//  var iid=form.name+'_inp';
		var el = document.getElementById(el_name);
  if (el == undefined) {
  		var inp=document.createElement('input');
  		inp.id = el_name; //iid;
  		inp.type = 'hidden';
  		inp.name = el_name;
  		inp.value = el_value;
    form.appendChild(inp);
  } else {
    el.value = el_value;
  }
}

function addHidden(ele, data) {
  // ele is the submit button doing the submission
  // data is optional json data pairs to add to ele's form
  res = false;
  if (typeof(ele) == 'string') { ele = document.getElementById(ele); }
  var t = ele;
		while (t && (t.parentNode != null) && (t.parentNode != undefined) && (t.tagName != 'FORM')) {	t = t.parentNode; }
 	if (t) {
    var el_name = ele.getAttribute('data-name');
    var el_value = ele.getAttribute('data-value');
    if (el_name) {
      addHiddenInput(t,el_name,el_value);
    }
    for (var name in data) {
      addHiddenInput(t,name, data[name]);
    }
  }
  return true; // usage will be <input type="submit" data-name="submitx" data-value="val" onclick="addHidden(this)"; >
}

function submitForm(ele) {
  // ele is the anchor doing the submission or the actual form or the id of either
  res = false;
  if (typeof(ele) == 'string') { ele = document.getElementById(ele); }
  var t = ele;
		while (t && (t.parentNode != null) && (t.parentNode != undefined) && (t.tagName != 'FORM')) {	t = t.parentNode; }
 	if (t) {
    el_name = ele.getAttribute('data-name');
    el_value = ele.getAttribute('data-value');
//    t_callback = t.getAttribute('data-ajax-callback');  // if this exists then it should be an ajax post
    if (el_name) {
      addHiddenInput(t,el_name,el_value);
    }
    res = t.onsubmit ? t.onsubmit() : true;
    if (res) {
//      if (t_callback) {
//        var act = t.action;
//        t.setAttribute('data-ajax-action',act); // frig form to match postAjax
//        postAjax(t,t_callback,'');
//      } else {
        t.submit();
//      }
    }
  }
  return false; // usage is probably <a href="" onclick="return submitForm(this)">
}

function checkID(el) {
  if (typeof(el) == 'string') { el = document.getElementById(el); }
  var ok = false;
  if (el) {
    ok = checkEle(el, 1, null);
    if (!ok) { try {  el.focus(); } catch(err) {};  }
  }
  return ok;
}

function checkChars(ele) {
   var str = ele.value;
   var max = ele.getAttribute('maxlength');
   var nc =  ele.getAttribute('nochars');
   if (!nc) { nc = 'noChars'; }
   var ncele = document.getElementById(nc);
   if (!ncele) { return; }
   var len = max -str.length;
   if (len>=0) {
      ncele.innerHTML = len;
   } else {
      ele.value = str.substr(0, max)
   }
}

// Allow many characters for gift voucher message
function isAlphanumericFSC(e, crok) {
  e = e || window.event;
  var k = (e.keyCode ? e.keyCode : e.which);
  return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 188 || k == 190 || k == 191 || k == 37 || k == 38 || k == 39 || k == 40 || k == 8 || k == 32 || (k >= 48 && k <= 57) || (crok == true && k == 13));
}

function isAlphanumeric(e, crok) {
  e = e || window.event;
  var k = (e.keyCode ? e.keyCode : e.which);
  return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || (crok == true && k == 13));
}

function isNiceChar(e, crok) {
  e = e || window.event;
  var k = (e.keyCode ? e.keyCode : e.which);
  return ((k != 34 && k != 39) && ((k > 64 && k < 91) || (k > 32 && k <= 64) || (k > 96 && k < 123) || k == 8 || k == 95 || k == 32 || (k >= 48 && k <= 57) || (crok == true && k == 13)));
}

function utf_currency(symbol) {
  return symbol.replace(/\�/ig,'&pound;').replace(/\�/ig,'&euro;');
}

function DD_currency(symbol) {  // for insertion into select options
  return symbol.replace(/\&pound\;/ig,'�').replace(/\&euro\;/ig,'�');
}

//--------------------------------------
// form prepare, initialisation & validation END
//--------------------------------------

function roundDP(num, dp, len) {
  if (dp == undefined) { dp = 0; }
  var ans = num * Math.pow(10,dp)
  ans = Math.round(ans) + ""
  while (ans.length < dp+1) {ans = "0" + ans}
  var l = ans.length
  ans = ans.substring(0,l-dp) + (dp > 0 ? "." + ans.substring(l-dp,l) : '')
  if (len != undefined) { while (ans.length < len) { ans = '0' + ans; } }
  return ans
}

function ajaxFilter(s) {
  s = s.replace(/\n/g,'~');
  s = s.replace('#','');  // firefox and chrome will truncate the data at the point of a #
  s = s.replace('&','~and~');
  s = s.replace('�','~pnd~');
  return s;
}

function addScript(script,onload,reload) {
  var headID = document.getElementsByTagName("head")[0];
  var scripts = document.getElementsByTagName("script");
  for (var i=scripts.length-1; i>=0; i--) {
    if (scripts[i].src == script) {
      if (reload === true) {
        headID.removeChild(scripts[i]);
      } else {
        return;
      }
    }
  }

  var newScript = document.createElement('script');
  newScript.async = true;
  newScript.done = false;
  if (onload && onload != undefined) {
    newScript.myload = onload;
  //  newScript.onload = function() { alert('zz'); }
  //  newScript.onload = newScript.onreadystatechange = function() { alert(this.readyState+' '+this.loaded+' '+this.complete); } //{ if (this.myload) this.myload(); this.done = true; };
    newScript.onload = newScript.onreadystatechange = function() {
        if (this.myload && !this.done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
          this.done = true;
          this.myload();
         try {
           newScript.onload = newScript.onreadystatechange = null;
           if ( headID && newScript.parentNode ) {
             headID.removeChild( newScript );
           }
         } catch(err) {}
       }
     }
  }
  newScript.src = script;
  headID.appendChild(newScript);
}

function addStyle(stylesheet, title, onload) {
  var headID = document.getElementsByTagName("head")[0];
  var newStyle = document.createElement('link');
  newStyle.type = 'text/css';
  newStyle.rel = 'stylesheet';
  newStyle.title = title;
  newStyle.onload = onload;
  newStyle.oncomplete = onload;
  newStyle.onunload = function() { };
  newStyle.href = stylesheet;
  headID.appendChild(newStyle);
  return newStyle;
}

function addCSS(css) {
// pass css as the text you would normally put in a <style></style> block to dynamically add it to the page
// eg 'body { background:red; } .div1 { border:1px solid red; }'
  var head = document.getElementsByTagName('head')[0]
  var style = document.createElement('style');
  style.type = 'text/css';
  if (style.styleSheet){
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }
  head.appendChild(style);
}

var csel_id = 0;
var cbox_id = 0;
var csel_last = '';

function cselDDClick(event, id, val) {
  var ele = document.getElementById(id);
  if (ele) {
    ele.value = val;
    selectRefresh(ele);
    if (ele.onchange) { ele.onchange(); }
  }
  popupClose('timer','');
//  document.title = document.title + 'x';
//  alert('x');

}
function selectRefresh(ele, focus) {
  // call this if selectedindex or value or the options are changed
  // ele should be the element or id of the original selector
  if (typeof(ele) == 'string') { ele = document.getElementById(ele); }
  if (ele) {
    var id = ele.id;
    var inp = document.getElementById(id+'inp');
    if (inp) {
//alert(ele.options[ele.selectedIndex].textContent);
      var i = ele.selectedIndex;
      var txt = (ele.options.length > 0 && i > -1) ? (ele.options[i].textContent ? ele.options[i].textContent : ele.options[i].text) : '';
      inp.value = txt;
      if (focus != 'draw') { selectRange(inp,0,0); }
    }
    if (ele.imgpath) {
      var img = document.getElementById(id+'img');
      if (img) {
        var bck = (ele.imgpath && txt) ? cselBackgroundUrl(ele,cselGetVal(ele.value)) : '';
        img.style.backgroundImage = bck;
      }
    }
  }
  return inp;
}


function cselDDHide() {
  var ele = document.getElementById(csel_last);
  if (ele) {
    var dd = document.getElementById(csel_last+'dd');
    if (dd) {
      dd.style.display = 'none';
//document.title = document.title + ' '+dd.id+' ';
    }
    ele.dropped = false;
    ele.closedTime = Date.parse(new Date());
  }
//  document.title = document.title + 'y';
//  alert('y');
}

function cselKD(event,id) {
  event = event || window.event;
  var key = (event.keyCode ? event.keyCode : event.which);
  var shift = (event.shiftKey ? event.shiftKey : event.shiftKey);
// 0..9 with no shift, 0-9 keypad, return-13, tab-9, del-46, backspace-8, left-37, right-39, home-36, end-35
//document.title = key+'x'+shift;
//  if ((((key >= 48) && (key <= 57)) && shift!=1) || ((key >= 96) && (key <= 105)) || (key == 13) || (key == 8) || (key == 9) || (key == 46) || (key == 39) || (key == 37) || (key == 35) || (key == 36))
  res = false;
  newval = false;
  if (key == 13) { cselDDHide(); }
  if (key == 9) { res = true;  cselDDHide(); }
  if ((((key >= 48) && (key <= 57)) && shift!=1) || ((key >= 96) && (key <= 105)) || ((key >= 65) && (key <= 90))) {
//document.title = key+'x'+shift+'x'+String.fromCharCode(key);
    var ele = document.getElementById(id);
    if (ele) {
      var i = ele.selectedIndex+1; if (i >= ele.options.length) { i = 0; }
      while (i != ele.selectedIndex && i < ele.options.length) {
//document.title = document.title  + 'z'+i+' ';
        if (ele.options[i].value && ele.options[i].text.toUpperCase().charAt(0) == String.fromCharCode(key)) {
          ele.selectedIndex = i;
          newval = true;
          break;
        }
        i++; if (i >= ele.options.length) { i = 0; }
      }
    }
  }
  if (key == 40 || key == 38) {
    var ele = document.getElementById(id);
    if (ele) {
      if (key == 38) {
        ele.selectedIndex = (ele.selectedIndex > 0 ? ele.selectedIndex-1 : ele.selectedIndex);
      }
      if (key == 40) {
        ele.selectedIndex = (ele.selectedIndex < ele.options.length-1 ? ele.selectedIndex+1 : ele.selectedIndex);
      }
      newval = true;
    }
  }
  if (key == 27) { cselDDHide(); }

  if (newval) {
    if (ele.onchange) { ele.onchange(); }
    var inp = selectRefresh(ele);
    if (inp) { selectRange(inp,0,1); }
  }
  event.returnValue = res;
  return res;
}

function selectRange(ele, iStart, iLength) {
  if (typeof(ele) == 'string') { ele = document.getElementById(ele); }
  if (ele.createTextRange) {
      var oRange = ele.createTextRange();
      oRange.moveStart("character", iStart);
      oRange.moveEnd("character", iLength - ele.value.length);
      oRange.select();
  }
  else if (ele.setSelectionRange) {
      ele.setSelectionRange(iStart, iLength);
  }
}

function cselFocus(id) {
  var ele = document.getElementById(id+'inp');
  if (ele) { selectRange(ele,1,0);   ele.focus(); }
}

function selectHide(ele) {
  // call this to hide a selector which may have a customSelector covering it
  // ele should be the element or id of the original selector
  if (typeof(ele) == 'string') { ele = document.getElementById(ele); }
  if (ele) {
    ele.hidden = true;
    if (ele.dropped == undefined) {  // ele has a cover up
      var el = document.getElementById(ele.id+'div');
      if (el) { el.style.display = 'none'; }
      else { ele.style.display = 'none'; }
    } else {
      ele.style.display = 'none';
    }
  }
}

function selectShow(ele) {
  // call this to hide a selector which may have a customSelector covering it
  // ele should be the element or id of the original selector
  if (typeof(ele) == 'string') { ele = document.getElementById(ele); }
  if (ele) {
    ele.hidden = false;
    if (ele.dropped == undefined) {  // ele has a cover up
      ele = document.getElementById(ele.id+'div');
      ele.style.display = 'block';
    } else {
      ele.style.display = 'block';
    }
  }
}

function cselBackgroundUrl(ele,val) {
  if (ele.imgfunc) {
    var value = eval(ele.imgfunc+'(ele,val,ele.imgvars)');
  } else {
    var value = ele.imgmask.replace(/\[xx_opt\]/ig,val);
  }
  return (val ? 'url('+ele.imgpath+value+ele.imgext+')' : 'none');
}

function cselGetVal(val) {
  // values might have | or ~ delimiters - assume first element is the part number
  if (val.indexOf('|') > -1) { val = val.split('|')[0]; }
  if (val.indexOf('~') > -1) { val = val.split('~')[0]; }
  return val;
}

function cselDD(id, xx) {
  var ele = document.getElementById(id);
  if (ele) {
    var dd = createDynamicPopup(id+'dd', false, 'cSelDD_'+xx);//, document.getElementById(id+'div') )
    if (dd) {
      var now = Date.parse(new Date());
      var justClosed = (Math.abs(now - ele.closedTime) < 100);
//document.title = document.title + ' '+ele.dropped+' ';
      if (ele.dropped || justClosed) {
        ele.dropped = false;
        dd.style.display = 'none';
      } else {
        csel_last = id;
        popupInit(cselDDHide,'!'+id+'dd');
        ele.dropped = true;
        dd.style.left = '-10000px';
        dd.style.display = 'block';

        var blanksOk = (ele.getAttribute('data-noblank') == null ? true : false);
        var s = '';
        s += '<table cellspacing="0" cellpadding="0" border="0"><tr>';
        s += '<td class="tl" style="background-position: 0 0;">';
        s += '<div id="'+id+'scrl" class="cSelDDScrl_'+ele.getAttribute('data-custom')+'" style="float:left;" >';
        s += '<ul id="'+id+'ul" style="overflow:hidden;" >';
        for (var i=0; i<ele.options.length; i++) {
          var val = cselGetVal(ele.options[i].value);
          if (val != '' || blanksOk || ele.options.length == 1) {
            var bck = ele.imgpath ? ' style="background-image:'+cselBackgroundUrl(ele,val)+'" ' : '';
            s += '<li '+bck+' onclick="cselDDClick(event,\''+id+'\',\''+ele.options[i].value+'\');" onmousemove="cselFocus(\''+id+'\');" >'+(ele.options[i].textContent ? ele.options[i].textContent : ele.options[i].text)+'</li>';
          }
        }
        s += '</ul>';
        s += '</div>';
        s += '</td><td class="tr" style="background-position: right 0;"><!--x--></td>';
        s += '</tr><tr>';
        s += '<td class="bot" style=" background-position: left bottom;" ><!--x--></td>';
        s += '<td class="bot" style=" background-position: right bottom;"><!--x--></td>';
        s += '</tr></table>';

        dd.innerHTML = s;

        var div = document.getElementById(id+'div');
        var d = getBounds(div);
        var dw = wborder(div);

        var bw = d.w;
        var ul = document.getElementById(id+'ul');
        var uw = wborder(ul);
        if (ul.offsetWidth - uw > bw) { bw = ul.offsetWidth - uw; }
        ul.style.width = (bw - dw - uw)+'px';

        var lis = ul.getElementsByTagName("LI");
        if (lis.length > 0) {
          var lw = wborder(lis[0]);
          for (var i=0; i<lis.length; i++) {
            lis[i].style.width = (bw - dw - uw - lw) + 'px';
          }
        }

        var ss  = screenSize();
        var spaceabove = d.y - ss.y;
        var spacebelow = ss.y + ss.h - d.y - d.h
        if (responsiveStyle == 'phone') {
          var dt = d.y + d.h;  // list below with no scrolling
        }
        else if (dd.offsetHeight <= spacebelow) {
          var dt = d.y + d.h;
        }
        else if (dd.offsetHeight <= spaceabove) {
          var dt = d.y - dd.offsetHeight;
        }
        else {
          var scrl = document.getElementById(id+'scrl');
          scrl.style.overflow = 'hidden';
          scrl.style.overflowY = 'scroll';
          scrl.style.width = (ul.offsetWidth+16-uw)+'px';
//          dd.style.width = scrl.offsetWidth;
          if (spaceabove > spacebelow) {
            scrl.style.height = (spaceabove-5-hborder(scrl))+'px';
            var dt = ss.y+1;
          } else {
            scrl.style.height = (spacebelow-5-hborder(scrl))+'px';
            var dt = d.y + d.h;
          }
        }

        setZIndex(dd);
        var xofs = ele.getAttribute('selectborder') == 'Y' ? 1 : 0;

        dd.style.left = xOnPage(d.x - xofs,dd.offsetWidth) + 'px';
        dd.style.top = dt + 'px';
      }

      selectRefresh(ele);
      var inp = document.getElementById(id+'inp');
      if (inp) { selectRange(inp,0,0);   inp.focus(); }
    }
  }
}

function doCustomSelects() {
// call a timer in case other things that create selects are also on a timer
  setTimeout(new Function("doCustomSelects_c()") ,1);
}

function doCustomSelects_c() {
  var sels = document.getElementsByTagName('select');
  if (sels) {
    for (var i = 0; i < sels.length; ++i) {
      var xx = sels[i].getAttribute('data-custom');
      if (customSelectDoAll && xx == null) { xx = ''; }
      if (xx !=  null) {
        var ele = sels[i];
        var yy = sels[i].getAttribute('data-img-path');
        sels[i].imgpath = (yy ? yy : '');
        var yy = sels[i].getAttribute('data-img-ext');
        sels[i].imgext = (yy ? yy : '.gif');
        var yy = sels[i].getAttribute('data-img-mask');
        sels[i].imgmask = (yy ? yy : '[xx_opt]');
        var yy = sels[i].getAttribute('data-img-func');
        sels[i].imgfunc = (yy ? yy : null);
        var yy = sels[i].getAttribute('data-img-vars');
        sels[i].imgvars = (yy ? yy : '');
        var yy = sels[i].getAttribute('data-responsive');
        sels[i].resp = (yy ? yy.toLowerCase() : '');
        // the code expects css to exist for cSelBack_xx and cSelInput_xx

        ele.style.display = 'none';

        if (!ele.id) { csel_id++; ele.id = 'csel_'+csel_id; }
        if (ele.id == 'title') { csel_id++; ele.id = ele.id + csel_id; }
        var id = ele.id;

        if (ele.dropped == undefined) {  // dont add another if this function is called a second time
        		var divn=document.createElement('div');
        		divn.id=id+'div';
        		divn.onclick = new Function('cselDD(\''+id+'\',\''+xx+'\'); return false;');
          divn.className = 'cSelBack_'+xx;

          var txt = (ele.options.length > 0 && ele.selectedIndex > -1) ? ele.options[ele.selectedIndex].text : '';
          var html = '';
          html += '<div class="cSelBtn_'+xx+'" id="'+id+'btn">&nbsp;</div>';
          html += '<div class="cSelImg_'+xx+'" id="'+id+'img" >';
          html += '<input type="text" class="cSelInput_'+xx+'" id="'+id+'inp" value="'+txt+'" readonly onkeydown="cselKD(event,\''+id+'\')" />';
          html += '</div>';
          html += '';
          divn.innerHTML = html;

          ele.dropped = false;
        		ele.parentNode.insertBefore(divn,ele);
        }

        var div = document.getElementById(id+'div');
        var d = getBounds(div);
        var dw = wborder(div);

        if (ele.hidden === true) {
          div.style.display = 'none';
        }

        // if the original select is not visible d.w will be 0.
        // You may need to re-call this function if it is made visible later
        if (d.w > 0) {
          var btn = document.getElementById(id+'btn');
          var b = getBounds(btn);

          var img = document.getElementById(id+'img');
          var iw = wborder(img);

          if (!sels[i].resp) {
            // error here = style problem
            img.style.width = (d.w - dw - b.w - iw-1)+'px';
          } // else css does it

          var inp = document.getElementById(id+'inp');
          var ew = wborder(inp);
          if (!sels[i].resp) {
            inp.style.width = (d.w - dw - b.w - iw - ew- 1) + 'px';
          } // else css does it
          inp.onfocus = ele.onfocus;
          inp.onblur = ele.onblur;

          var hasBorder = false;
          var par = ele.parentNode;
          if (par && par.className.match(/selectborder/ig)) { hasBorder = true; }
          if (par && par.className.match(/ddlborder/ig)) { hasBorder = true; }
          if (par && par.className.match(/std50border/ig)) { hasBorder = true; }
          if (hasBorder) {
            bgi = cssStyle(div,'backgroundImage');
            if (bgi && bgi != 'none') { // it has a background image so remove the selectborder border
              par.style.border = '0px solid red'; par.style.width = '100%'
            } else {
              ele.setAttribute('selectborder','Y');
            }
          }
        }
        selectRefresh(ele,'draw');
      }
    }
  }
}

function checkBoxRefresh(ele,wrong) {
  // call this is the original checkbox state is changed
  // ele should be the element or id of the original selector
  if (typeof(ele) == 'string') { ele = document.getElementById(ele); }
  if (ele.cb) {
    if (wrong == true) {
      ele.cb.className = ele.checked ? ele.classes[1] : ele.classes[0];
    } else {
      ele.cb.className = ele.checked ? ele.classes[0] : ele.classes[1];
    }
  }
}

function checkBoxClicked(id,wrong) {
  // onclick on the label happens before the checkbox value is changed so pass wrong = true
  ele = document.getElementById(id);
  if (wrong !== true) { ele.checked = !ele.checked; }
  checkBoxRefresh(ele, wrong);
}

function radioRefresh(ele) {
  // call this is the original checkbox state is changed
  // ele should be the element or id of the original selector
  if (typeof(ele) == 'string') { ele = document.getElementById(ele); }
  if (ele.cb) {
    var rads = document.getElementsByName(ele.name);
    if (rads) {
      for (var i=0; i<rads.length; i++) {
        var div = document.getElementById(rads[i].cbid);
        div.className = rads[i].checked ? rads[i].classes[0] : rads[i].classes[1];
      }
    }
  }
}

function radioClicked(id,wrong) {
  // onclick on the label happens before the checkbox value is changed so pass wrong = true
  ele = document.getElementById(id);
  ele.checked = true;
  radioRefresh(ele, wrong);
}

function doCustomCheckBoxAndRadio() {
// call a timer in case other things that create checkboxes are also on a timer
  setTimeout(new Function("doCustomCheckBoxAndRadio_c()") ,1);
}

function doCustomCheckBoxAndRadio_c() {
  var sels = document.getElementsByTagName('input');
  if (sels) {
    for (var i = 0; i < sels.length; ++i) {
      var ele = sels[i];
      var typ = ele.type.toLowerCase();
      if (typ == 'checkbox' || typ == 'radio') {
        var xx = ele.getAttribute('data-custom');
        if (customCheckBoxRadioDoAll && xx == null) { xx = ''; }
        if (xx !=  null && ele.dccb != true) {
          ele.dccb = true;
          // if it is disabled it wont post with the form
          // if it is hidden it will screw up on ipad
          ele.style.position = 'absolute';
          ele.style.left = '-10000px';

          if (!ele.id) { cbox_id++; ele.id = 'cbox_'+cbox_id; }
          var id = ele.id;
          var classes = xx.split('|');  // 0 = checked, 1 = unchecked

        		var divn=document.createElement('div');
        		divn.id=id+'div';
          if (typ == 'checkbox') {
            divn.onclick = new Function('checkBoxClicked(\''+id+'\');');
          } else {
          		divn.onclick = new Function('radioClicked(\''+id+'\');');
          }
        		divn.style.cursor = 'pointer';
        		ele.parentNode.insertBefore(divn,ele);

          ele.cb = divn;
          ele.cbid = divn.id;
          ele.classes = classes;

          var labs = document.getElementsByTagName('label');
          if (labs) {
            for (var j = 0; j < labs.length; ++j) {
              if (labs[j].htmlFor == ele.id) {
                // onclick on the label happens before the checkbox value is changed
                if (typ == 'checkbox') {
                  labs[j].onclick = new Function('checkBoxClicked(\''+ele.id+'\',true);');
                  ele.onclick = new Function('checkBoxRefresh(\''+ele.id+'\',false);');
                } else {
//                  labs[j].onclick = new Function('radioClicked(\''+ele.id+'\',false);');
                  ele.onclick = new Function('radioRefresh(\''+ele.id+'\',false);');
                }
              		labs[j].style.cursor = 'pointer';
              }
            }
          }
          checkBoxRefresh(ele);
        }
      }
    }
  }
}

function initTabHeights(tabs_id) {
  var ons = new Array();
  var tab = document.getElementById(tabs_id);
  var ht = 0;
  if (tab) {
    var margin = cssStyle(tab,'marginLeft');
    tab.style.marginLeft = '-10000px';
    var c = -1;
    var tabs = document.getElementsByTagName("LI");
    for (var i=0; i<tabs.length; i++) {
      if (tabs[i].className == 'tab') {
        c++;
        ons[c] = cssStyle(tabs[i],'display');
        tabs[i].style.display = 'block';
        if (tabs[i].offsetHeight > ht) { ht = tabs[i].offsetHeight; }
      }
    }
    var c = -1;
    for (var i=0; i<tabs.length; i++) {
      if (tabs[i].className == 'tab') {
        tabs[i].style.height = ht + 2 - hborder(tabs[i]) + 'px';
        c++;
        tabs[i].style.display = ons[c];
      }
    }
    tab.style.marginLeft = margin+'px';
  }
}

// dynamic pagers
var dynPagers = new Array();
// add the push line below to the template which needs the scroller.
// pager is the id of a div you added to the template to contain the page number links
// pages is the id of a div you put around the area to be paged
// itemclass is the class of an item on a page
// itemtype is the tagname of the items eg 'DIV' or 'LI'
// pager_layout_js is a json array in custom.js defining the layout and css to use for the page click buttons
// onPageChange is function(idx) which will be called when a page is changed
// everything is called dynPage..... so it should be easy to find
//dynPagers.push( { pager:'dynPager_2', pages:'dynPages_1', items_per_row:4, rows_per_page:2,
//   itemclass:'pressitem', itemtype:'div', pager_layout_js:dyn_pager_type_2, removeRightBorder:true, onPageChange:null });


function dynPagerExampleOnPageChange(idx) {
  var loc = dynPagers[idx].pages;
  location = '#'+loc+'top';  // jump to the top of the list (assuming you put the id there)
}

function dynPagerText(idx,r1,r2) {
  var dyn_pager = dynPagers[idx];
  var pageNo = dyn_pager.pageNo;
  var pageMax = dyn_pager.pageMax;
  var pager = dyn_pager.pager_layout_js;

  var sp = '';
  if (pageNo > 1 && pager.showPrevNextArrows) { sp += pager.prevButton; } else { sp += pager.noPrevButton; }
  sp = sp.replace(/\[xx[_ ]prev_page\]/ig,pageNo-1);

  var sb = '';
  if (r1 < r2) {
    for (var j=r1; j<=r2; j++) {
      if (j == pageNo) { sb += pager.thisPage; } else { sb += pager.linkPage; }
      sb = sb.replace(/\[xx[_ ]pageno\]/ig,j);
    }
  }

  var sn = '';
  if (pageNo < pageMax && pager.showPrevNextArrows) { sn += pager.nextButton; } else { sn += pager.noNextButton; }
  sn = sn.replace(/\[xx[_ ]next_page\]/ig,pageNo-0+1);

  var s = pager.layout;
  s = s.replace(/\[xx[_ ]prev\]/ig,sp);
  s = s.replace(/\[xx[_ ]pages\]/ig,sb);
  s = s.replace(/\[xx[_ ]next\]/ig,sn);
  s = s.replace(/\[xx[_ ]base\]/ig,pager.classBaseName);
  s = s.replace(/\[xx[_ ]idx\]/ig,idx);
  return s;
}

function dynShowPager(idx) {
  var tmp = dynPagerText(idx,1,dynPagers[idx].pageMax);

  var pagers = dynPagers[idx].pager.split(',');
  for (j = 0; j < pagers.length; j++) {
    var elp = document.getElementById(pagers[j]);
    if (elp) {
      elp.style.display = 'block';
      switch (dynPagers[idx].pager_layout_js.position) {
        case 'right': var styl = 'float:right; display:inline;'; break;
        case 'left': var styl = 'float:left; display:inline;'; break;
        case 'center': var styl = 'float:left; display:inline;'; break;
      }
      var tmp1 = '<div id="'+pagers[j]+'Container" style="'+styl+'">'+tmp+'</div>';
      elp.innerHTML = tmp1;
      var elc = document.getElementById(pagers[j]+'Container');
      if (elc) {
        if (dynPagers[idx].pager_layout_js.position == 'center') {
          styl = 'width:'+elc.offsetWidth+'px; margin:0 auto;';
          var tmp1 = '<div id="'+pagers[j]+'Container" style="'+styl+'">'+tmp+'</div>';
          elp.innerHTML = tmp1;
        }
      }
    }
  }

  var ps = (dynPagers[idx].pageNo-1) * dynPagers[idx].pageSize;
  var pe = ps + dynPagers[idx].pageSize-1;
  var el = document.getElementById(dynPagers[idx].pages)
  if (el) {
    var c = -1;
    var divs = el.getElementsByTagName(dynPagers[idx].itemtype);
    for (var j = 0; j < divs.length; j++) {
      if (divs[j].className == dynPagers[idx].itemclass) {
        c++;
        divs[j].style.display = (c >= ps && c <= pe ? 'block' : 'none');
      }
    }
  }
}

function dynPageTo(idx,p) {
  dynPagers[idx].pageNo = p;
  dynShowPager(idx);
  if (dynPagers[idx].onPageChange) {
    dynPagers[idx].onPageChange(idx);
  }
}

function initDynPagers() {
  for (var i = 0; i < dynPagers.length; i++) {
    var el = document.getElementById(dynPagers[i].pages)
    if (el) {
      var c = 0; var l = -1;
      var divs = el.getElementsByTagName(dynPagers[i].itemtype);
      for (var j = 0; j < divs.length; j++) {
        if (divs[j].className == dynPagers[i].itemclass) {
          c++;
          l = j;
          if (dynPagers[i].removeRightBorder && (c % dynPagers[i].items_per_row) == 0) { divs[j].style.borderRightWidth = 0; }
        }
      }
      if (dynPagers[i].removeRightBorder && (l > -1)) { divs[l].style.borderRightWidth = 0; }
      dynPagers[i].pageMax = Math.floor(c / (dynPagers[i].items_per_row * dynPagers[i].rows_per_page))+1;
      if ((dynPagers[i].pageMax-1)*dynPagers[i].items_per_row * dynPagers[i].rows_per_page == c) dynPagers[i].pageMax--;
      if (dynPagers[i].pageMax > 1) {
        dynPagers[i].pageNo = 1;
        dynPagers[i].itemCount = c;
        dynPagers[i].pageSize = dynPagers[i].items_per_row * dynPagers[i].rows_per_page;
        // hide all the divs after page 1
        for (var j = dynPagers[i].pageSize; j < divs.length; j++) { if (divs[j].className == dynPagers[i].itemclass) { divs[j].style.display = 'none'; } }
        dynShowPager(i);
      }
    }
  }
}

var popOpens = new Array();
// add the "popOpens.push" line below to your template
// turns a list of divs or p's with one class for the title and another for the main text into
// a clickable open/close list  (lolo faq page)
// container is a div around the area to be controlled
// clickclass is the class of the titles or text to be clicked
// clickedclass is the class to use when the list is opened
// openclass is the class of the items to open when the 'clickclass' above them is clicked.
//popOpens.push( { container:'popOpen1', clickclass:'faqhi', clickedclass:'faqhio', openclass:'faq', exclusiveopen:false });

function popOpenClick(thisid,idx) {
  var popOpen = popOpens[idx];

    var el = document.getElementById(popOpens[idx].container)
    if (el) {
      var open = false;
      var close = popOpens[idx].exclusiveopen;
      var nodes = el.getElementsByTagName('*');
//      var nodes = el.childNodes;
      for (var j = 0; j < nodes.length; j++) {
        if ((nodes[j].className == popOpens[idx].clickclass) || (nodes[j].className == popOpens[idx].clickedclass)) {
          if (nodes[j].id == thisid) {
            var state = nodes[j].getAttribute('data-opened');
            if (state == 'open') {
              open = false; close = true;
              nodes[j].setAttribute('data-opened','closed');
              nodes[j].className = popOpens[idx].clickclass;
            } else {
              open = true; close = false;
              nodes[j].setAttribute('data-opened','open');
              nodes[j].className = popOpens[idx].clickedclass;
            }
          } else {
            open = false;
            close = popOpens[idx].exclusiveopen;
          }
        }
        if (nodes[j].className == popOpens[idx].openclass) {
          if (open) {
            nodes[j].style.display = 'block';
            nodes[j].style.height = 'auto';
            rolldown(nodes[j],null,popOpens[idx].overflow);
//            nodes[j].style.display = 'block';
          } else if (close) {
            rollup(nodes[j]);
//            nodes[j].style.display = 'none';
          }
        }
      }
    }

}

function initPopOpens() {
  for (var i = 0; i < popOpens.length; i++) {
    var el = document.getElementById(popOpens[i].container)
    if (el) {
      var c = 0;
      var nodes = el.getElementsByTagName('*');
//      var nodes = el.childNodes;
      for (var j = 0; j < nodes.length; j++) {
        if (nodes[j].className == popOpens[i].clickclass) {
          c++;
          var nn = nodes[j].id ? nodes[j].id : 'popo'+c;
          nodes[j].id = nn;
          nodes[j].onclick = new Function('popOpenClick(\''+nn+'\',\''+i+'\'); return false;');
          nodes[j].setAttribute('data-opened','closed');
        }
        if (nodes[j].className == popOpens[i].openclass) {
          c++;
          nodes[j].style.display = 'none';
        }
      }
    }
  }
}
var ccc=0;
function showJson_c(data,ind,lev,max_nest) {
  var s = '';
ccc++;
if (ccc > 1000) return;
  for (var xx in data) {
    if (typeof(data[xx]) == 'object' && lev < max_nest) {
      s += ind + '[<b>'+xx+' object</b>]<br>';
      if (xx && xx.toLowerCase() != 'srcelement' && xx.toLowerCase() != 'view') {
        s += showJson_c(data[xx],ind+'&nbsp;&nbsp;',lev+1, max_nest);
      }
      s += ind + '[<b>/'+xx+' object</b>]<br>';
    } else {
      try { s += ind + '<b>'+ xx + '</b>='+data[xx]+'<br>'; } catch(err) { s += 'ERROR<br>'; return; }
    }
  }
  return s;
}
function showJson(data,max_nest,doalert) {
ccc = 0;
  if (typeof(data) == 'object') {
    var s = '';
    s += showJson_c(data,'',0,max_nest);
    if (doalert) { alert(s); } else { alertX(s,'',0,'','left'); }
  } else {
    alertX('<b>not json data</b> <br>'+data,'',0,'','left');
  }
}

function doDecimals(original_number, decimals) {
    var result1 = original_number * Math.pow(10, decimals)
    var result2 = Math.round(result1)
    var result3 = result2 / Math.pow(10, decimals)
    return pad_with_zeros(result3, decimals)
}

function pad_with_zeros(rounded_value, decimal_places) {
    var value_string = rounded_value.toString()
    var decimal_location = value_string.indexOf(".")
    if (decimal_location == -1) {
        decimal_part_length = 0
        value_string += decimal_places > 0 ? "." : ""
    }
    else {
        decimal_part_length = value_string.length - decimal_location - 1
    }
    var pad_total = decimal_places - decimal_part_length
    if (pad_total > 0) {
        for (var counter = 1; counter <= pad_total; counter++)
            value_string += "0"
        }
    return value_string
}

// Declaring required variables
var digits = "0123456789";
// non-digit characters which are allowed in phone numbers
var phoneNumberDelimiters = "()- ";
// characters which are allowed in international phone numbers
// (a leading + is OK)
var validWorldPhoneChars = phoneNumberDelimiters + "+";
// Minimum no of digits in an international phone no.
var minDigitsInIPhoneNumber = 8;

function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}
function innerTrim(s)
{   var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not a whitespace, append to returnString.
    for (i = 0; i < s.length; i++)
    {
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (c != " ") returnString += c;
    }
    return returnString;
}
function stripCharsInBag(s, bag)
{   var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++)
    {
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function isPhone(strPhone){
//alert(strPhone);
var bracket=3
strPhone=innerTrim(strPhone)
if(strPhone.indexOf('+')>1) return false
//alert('1');
if(strPhone.indexOf('-')!=-1) bracket=bracket+1
//alert('bracket='+bracket);
//alert('(='+strPhone.indexOf('('))
if(strPhone.indexOf('(')!=-1 && strPhone.indexOf('(')>bracket) return false
var brchr=strPhone.indexOf('(')
//alert('brchr='+brchr);
//alert(')='+strPhone.charAt(brchr+3))
if(strPhone.indexOf("(")!=-1 && strPhone.charAt(brchr+3)!=')') return false
//alert('2');
if(strPhone.indexOf("(")==-1 && strPhone.indexOf(')')!=-1) return false
//alert('3');
s=stripCharsInBag(strPhone,validWorldPhoneChars);
//alert('s='+s);
//alert(s.length >= minDigitsInIPhoneNumber);
return (isInteger(s) && s.length >= minDigitsInIPhoneNumber);
}

var ajaxax = {};
function helpPopup(file, wid, hgt, title) {
  ajaxax = { hndl: null, wid:630, hgt:0, title:'' };
  if (!pageIsLoaded) return;
  if (wid) { ajaxax.wid = wid; }
  if (hgt) { ajaxax.hgt = hgt; }
  if (title) { ajaxax.title = title; }
  loadFileAjax(file, helpPopupCallback);
  ajaxax.hndl = alertX('<div id="basketHelp">'+getTranslatedText('tt_helpPopup','Loading. Please wait...')+'<img src="'+thisUrl+'assets/images/wait.gif" alt="" ></div>');
}

function helpPopupCallback(state, status, res) {
  if (state == 4) {
    closeAlertX(ajaxax.hndl,null);
    var wid = (ajaxax.wid > 0 ? 'max-width:'+ajaxax.wid+'px; ' : '');
    var hgt = (ajaxax.hgt > 0 ? 'min-height:'+ajaxax.hgt+'px; ' : '');
    alertConfig.clickCoverToClose = true;
    ajaxax.hndl = alertX('<div id="basketHelp" style="'+wid+hgt+'">'+res+'</div>','',0,ajaxax.title);
  }
}

function setEUCookie(mode) {
  EUCookieSet = true;
  setXCookie('EUCookie','YES',730,'/','');
  if (mode != 'notify') {
    var el = document.getElementById('EUCookieMessage');
    if (el) { el.style.display = 'none'; }
  }
  if (mode != 'noloc' && mode != 'notify') { location.reload(); }
}

function initCookies() {
  EUCookieSet = getXCookie('EUCookie') == 'YES';
  if (EUCookieSet) { setEUCookie('noloc'); }

  cookiesOn = false;
  if (navigator.cookieEnabled) {
    cookiesOn = true;
  } else	if (navigator.cookieEnabled == undefined)	{
    setXCookie('xxtest','YES');
    if (getXCookie('xxtest') == 'YES') { cookiesOn = true; setXCookie('xxtest',''); }
	 }

  if (cookiesOn) {
    var ld = getXCookie('vdate') || '';
    var nv = getXCookie('vcount') || 0;

    var dt = new Date();
    var d = dt.getFullYear() + "/" + (dt.getMonth()-0 + 1) + "/" + dt.getDate();

    if (d != ld) {
      setXCookie('vdate',d,730,'/','');
      nv++;
      setXCookie('vcount',nv,730,'/','');
    }
	 }
}

function randColor(mask) {
  function randhex(m) {
    var v = (m == '1' ? Math.floor(Math.random()*200+56) : 0);
    var nv = 255^v;
    var v = v.toString(16);
    while (v.length < 2) { v = '0' + v; }
    var n = nv.toString(16);
    while (n.length < 2) { n = '0' + n; }
    return { 'h':v, 'n':n };
  }
  if (!mask) { mask = '111'; }  // mask = rgb, 1 = random value, 0 = 'ff'
  while (mask.length < 3) { mask += '0'; }
  mask = mask.split('');
  var b = '#'; var c = '#';
  for (var i=0; i<mask.length; i++) { var v = randhex(mask[i]); b+=v.h; c+=v.n; }

  return { 'b':b, 'c':c };
}
function colourDivs() {
  var els = document.getElementsByTagName('div');
  for (var i=0; i<els.length; i++) {
    var x = randColor('001');
    els[i].style.backgroundColor = x.b;
    els[i].style.color = x.c;
  }
  var els = document.getElementsByTagName('li');
  for (var i=0; i<els.length; i++) {
    var x = randColor('100');
    els[i].style.backgroundColor = x.b;
    els[i].style.color = x.c;
  }
  var els = document.getElementsByTagName('p');
  for (var i=0; i<els.length; i++) {
    var x = randColor('010');
    els[i].style.backgroundColor = x.b;
    els[i].style.color = x.c;
  }
  var els = document.getElementsByTagName('td');
  for (var i=0; i<els.length; i++) {
    var x = randColor('100');
    els[i].style.backgroundColor = x.b;
    els[i].style.color = x.c;
  }
}

function checkDebug() {
  try { // can except in a window.open popup
  var s = window.location.href.toLowerCase();
  if (s.indexOf('showdiv=') > -1) {
    var ani = (s.indexOf('showdiv=true') > -1 ? 'T' : 'F');
    setXCookie('showdiv',ani);
  } else {
    var ani = getXCookie('showdiv');
  }
  if (ani == 'T') { addLoadEvent(colourDivs,99000); }

  var s = window.location.href.toLowerCase();
  if (s.indexOf('geodebug=') > -1) {
    var ani = (s.indexOf('geodebug=true') > -1 ? 'T' : 'F');
    setXCookie('geodebug',ani);
  } else {
    var ani = getXCookie('geodebug');
  }
  if (ani == 'T') { geodebug = true; }

  if (s.indexOf('showdebug=') > -1) {
    var ani = (s.indexOf('showdebug=true') > -1 ? 'T' : 'F');
    setXCookie('showdebug',ani);
  } else {
    var ani = getXCookie('showdebug');
  }
  if (ani == 'T') { canshowdebug = true; }

  if (s.indexOf('resp=') > -1) {
    var ani = (s.indexOf('resp=true') > -1 ? 'T' : 'F');
    setXCookie('doresp',ani);
  } else {
    var ani = getXCookie('doresp');
  }
  if (ani == 'T') { isResponsiveSite = true; showResizeTip = true; }
  if (location.href.match(/elucidportal/ig)) { isResponsiveSite = true; showResizeTip = top.location == self.location; }

  } catch(err) {}
}

var swt_speed = 0, swt_jump = 10, swt_top = 0, swt_y = 0, swt_dir = 1; swt_id = null;
function scrollWindowToTimer() {
  if (swt_y == -1) {
    var el = document.getElementById(swt_id);
    if (el) {
      var b = getBounds(el);
//      var d = documentSize();
      var s = screenSize();
      swt_top = b.y;
      swt_y = s.y;
      if (swt_y < swt_top) { swt_dir = 1; } else { swt_dir = -1; }
      if (swt_top == s.y ) { return; }
    } else {
       return;
    }
  }

  swt_y = swt_y + (swt_jump * swt_dir);
  window.scrollTo(0,swt_y);
  if ((swt_y < swt_top && swt_dir == 1) || (swt_y > swt_top && swt_dir == -1)) {
    setTimeout( scrollWindowToTimer, swt_speed );
  }
}
function scrollWindowTo(id, delay, jump, initdelay) {
    swt_id = id;
    swt_y = -1;
    swt_speed = delay || 1;
    swt_jump = jump || 50;
    setTimeout( scrollWindowToTimer, initdelay || 500 );
//   document.getElementById(id).scrollIntoView();
}

function getTranslatedText(id,def,vals) {
// id = id of div holding translated text
// def = default text to use if div doesnt exist
// vals = json array of values to replace
// eg. def='between [1] and [2]' vals={'[1]':10, '[2]':2}
// PLURALS are handled by ending the id with _plural and adding a _one id to the div list
// eg. getTranslatedText('tt_test_plural','these [x] items',{ '[x]':qty })  ** NOTE only one value allowed
// <div id="tt_test_one">this item</div>
// <div id="tt_test_plural">these [x] items</div>
  if (id.indexOf('_plural') > -1 && vals) {
    for (var x in vals) { break; }
    if (vals[x]-0 == 1) { id = id.replace('_plural','_one'); }
  }
  var trans = document.getElementById(id);
  var res = (trans ? trans.innerHTML : def);
  if (!res) { res = id; }
  if (vals) {
    for (var x in vals) {
      res = res.replace(x, vals[x]);
    }
  }
  return res;
}

function deleteViewed(id,div_id,onchange) {
  var s = getXCookie('ViewedList');
  var a = s.split('#|#');

  for (var i=0; i<a.length; i++) {
    var b = a[i].split('##');
    if (b[1] == id || b[1] == id.replace(/\ /ig,'+')) {  // asp removes + from the cookie
      // found the element - kill it from the cookie and hide the div

      var el = document.getElementById(div_id);
      if (el) { el.style.display = 'none'; }

      a.splice(i,1);
      s = a.join('#|#');
      setXCookie('ViewedList',s,90,'/');

      if (onchange != undefined) { // onchange handler in case anything else needs to know
        if (typeof(onchange) == 'string') {
          eval(onchange);
        } else {
          onchange(el);
        }
      }
      break;
    }
  }
}

function showDebug(s,append) {
  var showdebug = false;
  var ss = window.location.href.toLowerCase();
  if (ss.indexOf('showdebug=') > -1) { showdebug = true; }
  if (showdebug || (window.IP && (IP.substr(0,11) == '192.168.128' || IP.substr(0,9) == '80.193.99' || IP == '127.0.0.1' || IP == '95.138.201.43'))) {
    var el = document.getElementById('debug');
    if (!el) {
      el = document.createElement('div');
      el.setAttribute('id', 'debug');
      try { document.body.appendChild(el); } catch(err) {}
    }
    el.innerHTML = ((append == undefined || !append || el.innerHTML == '') && s > '' ? '<span onclick="showDebug(\'\');">CLOSE</span><br>' : '')
                 + (append == true ? el.innerHTML : '') + s;
    el.style.display =(el.innerHTML == '' ? 'none' : 'block')
  }
}

function removeClass(s, classs) {
  s = s || '';
  var clasa  = classs.split(',');   // can be class1,class2 etc
  for (var i=0; i<clasa.length; i++) {
    var pattern = new RegExp("(^| )" + clasa[i] + "( |$)");
    s = s.replace(pattern, "$1");
  }
  s = s.replace(/ $/, "");
  return s;
};

function addClass(s, classs) {
  s = trim(removeClass(s,classs));
  s += (s.length > 0 ? ' ' : '') + classs;
  return s;
}

function expander(self, state, justdraw) {
// this does the basic job of expanding one div when another is clicked. The html will be something like:
// <div class="collapsed" data-exp="tt_collapse_text|tt_expand_text|div_to_expand|[+][-]div2|etc" onclick="expander(this);">
//  Open my div
// </div>
// <div id="div_to_expand">expanded contents</div>
//
// A class of .collapsed and .expanded must exist for the element
// If the first two parts of data-exp begin with tt_ they are assumed to be translated text
// state = 'collapse' or 'expand' or anything else to toggle the current value
// The call can control any number of divs by adding them to the data-exp array. eg |div1|div2|div3 etc.
// div2 means open and close this div, -div2 means only close this div, +div2 means only open this div
// !div4 means reverse open/close logic for this div
  if (typeof(self) == 'string') { self = document.getElementById(self); }
  if (!self) { return; }

  var l_state = self.state;
  var c_state = (self.className.indexOf('collapsed') > -1 ? 'collapsed' : 'expanded');
  if (l_state == undefined) { l_state = c_state; }
  if (state && state != undefined) { state = state.toLowerCase(); } else { state = ''; }
  if (state.indexOf('collapsed') > -1) { l_state = 'collapsed'; }
  else
  if (state.indexOf('expanded') > -1) { l_state = 'expanded'; }
  else
  { l_state = (l_state == 'collapsed' ? 'expanded' : 'collapsed'); }

  self.state = l_state;
  var el = null;
  // must use &, not &amp; in data exp - as usual IE screws it up.
  var data = self.getAttribute('data-exp').replace(/\&/ig,'xyx').split('|');
  if (data) {
    var newtext = (l_state == 'collapsed' ? data[0] : data[1]);
    if (newtext) {
//      if (newtext.indexOf('tt_') == 0) { newtext = getTranslatedText(newtext,newtext); }
      self.innerHTML = newtext.replace('xyx','&amp;');
    }
    self.className = addClass(removeClass(self.className, c_state), self.state);

    if (justdraw !== true) {
      for (var i=2; i<data.length; i++) {  // open/close the associated divs
        if ((data[i] != undefined) && data[i]) {
          var oktoopen = true, oktoclose = true, reverse = false;
          if (data[i].charAt(0) == '-') { oktoopen = false; data[i] = data[i].substr(1,100); }
          if (data[i].charAt(0) == '+') { oktoclose = false; data[i] = data[i].substr(1,100); }
          if (data[i].charAt(0) == '!') { reverse = true; data[i] = data[i].substr(1,100); }
          el = document.getElementById(data[i]);
          if (el) {
            if (reverse) {
				var displayMethod = (el.classList.contains('editbox')) ? 'inline-block' : 'block';
              	el.style.display = (l_state == 'collapsed') ? displayMethod : 'none';
            } else if (l_state == 'collapsed') {
              if (oktoclose) { el.style.display = 'none'; }
            } else {
              if (oktoopen) { el.style.display = 'block'; }
            }
          }
        }
      }
    }
  }
  return el;  // pointer to the div that was opened or null if 'justdraw'
}

function doEmail(boo,user,domain,com,extra) {
  if (boo) {
    doUnloadPopup = false;  // this is declared in custom.js but life is not perfect
    document.location = 'mailto:' + user + "@" + domain + "." + com + extra;
  } else {
    document.write(user + "@" + domain + "." + com + extra);
  }
}

function addEvent(obj, evx, func) {
// eg addEvent(window, 'resize', myOnResize)
// also now uses jquery namespace functionality. eg ev = 'blur.myname' will make sure that the onblur handler only gets added once
  var ev = evx;
  if (evx.indexOf('.') > -1) {
     ev = evx.split('.')[0];
     var ns = evx.split('.')[1];
     if (obj.getAttribute('data-'+evx) == 'y') {
       return;  // already added
     } else {
       obj.setAttribute('data-'+evx,'y');
     }
  }
  if (typeof(obj) == 'string') { obj = document.getElementById(obj); }
  if (obj == undefined) return;
		if (obj.addEventListener) obj.addEventListener(ev, func, false);
		else if (obj.attachEvent) obj.attachEvent('on'+ ev, func);
		else obj['on'+ ev] = func;
}
function removeEvent(obj, ev, func) {
// eg removeEvent(window, 'resize', myOnResize)
		if (obj.removeEventListener) obj.removeEventListener(ev, func, false);
		else if (obj.detachEvent) obj.detachEvent('on'+ ev, func);
		else obj['on'+ ev] = null;
}

function applyZoom(div) {
  div = div || 'outerContainer';
  var lm = document.getElementById(div);
  if (lm) {
    var lia = lm.getElementsByTagName("DIV");
    for (var i=0; i < lia.length; i++ ) {
      if (lia[i].className.indexOf('applyzoom') > -1) {
        lia[i].style.zoom = (100*responsiveMultiplier)+'%';
      }
    }
  }
}

function capitalize (text) {
  return text.charAt(0).toUpperCase() + text.slice(1).toLowerCase();
}

function getGeoLocation(func) {
  function plop(gres) {
    gres.ok = true; gres.errorcode = -1; gres.error = '';
    if (geodebug) { showJson(gres); }
    func(gres);
  }
  function ploperr(gres) {
    gres.ok = false;
    if (geodebug) { showJson(gres); }
    func(gres);
  }

  if (!is_mob) { // pc browser might have function but not gps
    if (geodebug) { alert('geo not mobile'); }
    return
  }
  if (navigator.appVersion.toLowerCase().indexOf('silk') > -1) { // kindle doesnt do gps
    if (geodebug) { alert('geo bail out for silk browser'); }
    return
  }

  if (geodebug) { alert('geo about to check for func'); }
  if (navigator.geolocation) {
    if (geodebug) { alert('geolocation function exists'); }
    navigator.geolocation.getCurrentPosition(plop,ploperr);
  }
  // calls func and passes a json array with coords.latitude + coords.longitude + others - see google for more
}

function removeElucidTags(res) {
  return res.replace(/<elucid_([^\>]*)>/ig,'');
}

function getResponsivePixelRatio() {
  function getCssPixelRatioMediaQuery(ratio) {
    var dpiTestStr =  "only screen and (-webkit-min-device-pixel-ratio: "+ratio+"),";
    dpiTestStr+=      "only screen and (min--moz-device-pixel-ratio: "+ratio+"),";
    dpiTestStr+=      "only screen and (-o-min-device-pixel-ratio: "+ratio+"/1),";
    dpiTestStr+=      "only screen and (min-device-pixel-ratio: "+ratio+")";
    return dpiTestStr;
  }

  if (window.matchMedia) {
    var cssRatio =2;
    for (var i = 20; i >= 1; i--) {
      var ratio = i/ 10;
      if (ratio<=1) { return 1; }
      else {
         var match = window.matchMedia(getCssPixelRatioMediaQuery(ratio));
         if (match.matches) { return ratio; }
      }
    }
  } else {
    return 1;
  }
}

function initEventHandlers() {
  addEvent(window, 'load', doLoad);
  addEvent(window, 'unload', doUnLoad);
  if (window.doBeforeUnload) { addEvent(window, 'beforeunload', doBeforeUnload); }
  addEvent(window, 'scroll', doScrollEvent);
  addEvent(window, 'resize', doResize);
}

function whoCalledMe() {
  var mycaller = arguments.callee.caller;
//  console.log(mycaller);
  if (mycaller.arguments.callee.caller == null) {
    return 'System'
  } else {
    return mycaller.arguments.callee.caller.toString().substring(0,mycaller.arguments.callee.caller.toString().indexOf('(')).replace('function ','');
  }
}

initEventHandlers();
initCookies();
addLoadEvent(initPopOpens,1);
addLoadEvent(setFocusTo,1);
addLoadEvent(postLoadImages,4001);
addLoadEvent(initDynPagers,4000);
addLoadEvent(prepFrm,1);
checkDebug();
if (is_mob) {
  mobListen();
  addCSS('body { -webkit-overflow-scrolling: touch; ms-overflow-style:scrollbar; } ');
}

//JB-467
function isAlphanumericText(e) {
  if(window.event)
  {
   var keyCode = window.event.keyCode;       // IE
  }
  else
  {
   var keyCode = (e.keyCode ? e.keyCode : e.which);
  }

  pat = def_textbox_regex
  var patt = new RegExp(pat);
  var inp = String.fromCharCode(keyCode);
  var allowedkeys = [8364,45,8,13,9,37,38,39,40];

  if (patt.test(inp) || allowedkeys.indexOf(keyCode) > -1)
  //if (patt.test(inp) || (keyCode == 8364) ||(keyCode == 45) || (keyCode == 8) || (keyCode == 13) || (keyCode == 9) || (Keycode > 36 && Keycode < 41))
  {
    return true;
  }
  else
    {
     return false
    }
}

//Allow letter, number or space in Postcode
function isAlphanumericPC(e) {

  if(window.event)
  {
   var keyCode = window.event.keyCode;       // IE
  }
  else
  {
   var keyCode = e.which;
  }

  var inp = String.fromCharCode(keyCode);
  if (/[a-zA-Z0-9 ]/.test(inp) || (keyCode == 8) || (keyCode == 13) || (keyCode == 9))
  {
    return true;
  }
  else
    {
     return false
    }
}

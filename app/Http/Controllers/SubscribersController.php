<?php

namespace App\Http\Controllers;

use App\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;

class SubscribersController extends Controller
{
    protected $subscriber;
    //build the subscription form
    public function index(){
        //load a view directly
        return view('subscribe-form');
    }

    public function store(Request $request){

        //validate $request
        $request->validate([]);
        $subscriber = new Subscriber();
        $subscriber->email = $request->input('email');
        $subscriber->subscribed = $request->input('doi');
        $this->subscriber = $subscriber;
    }
}

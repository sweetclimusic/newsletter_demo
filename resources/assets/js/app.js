
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('../../../public/js/newsletter');

document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems, []);
});

//for sidebar navigation
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, []);
});

//Observing when copyright div comes into view to bind the subscribe news letter to the bottom
// this is the target which is observed
var target = document.querySelector('copyrightTextDiv');
var uptarget = document.querySelector('homepagetext');

function isScrolledIntoView(el) {
    var rect = el.getBoundingClientRect();
    var elemTop = rect.top;
    var elemBottom = rect.bottom;

    // Only completely visible elements return true:
    var isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
    // Partially visible elements return true:
    //isVisible = elemTop < window.innerHeight && elemBottom >= 0;
    return isVisible;
}

// var scrollEventHandler = function() {
//     var subscribeform = document.getElementsByClassName('pin-form')[0];
//     if(isScrolledIntoView(target)) {
//         $('.some-text').text('Scolled completely into view');
//         $(subscribeform).addClass('fix-form');
//         $(subscribeform).removeClass('pin-form');
//         //unbindScrollEventHandler();
//     }
//     if(isScrolledIntoView(uptarget)) {
//         $(subscribeform).removeClass('fix-form');
//         $(subscribeform).addClass('pin-form');
//     }
//}
(function() {
    window.jQuery = window.$ = require('jquery');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});
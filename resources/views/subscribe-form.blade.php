@section("title","Confirm Newsletter Subscription")
@section("header","Subscribe to our newsletter for 10% off!")
<!-- sticky floating notification -->
<div class="fixed-mail-btn hide-on-small-only">
    <div class="fixed-notification-badge red center-align z-depth-1">1</div>
    <a class="waves-effect waves-light modal-trigger" href="#modal1"><i class="large material-icons mail-floating">mail</i></a>
</div>
<!-- sign up form -->
<div class="row pin-form">
    <a class="waves-effect waves-light modal-trigger" href="#modal1" >
    <div id="newsletter-signup" class="col s12 offset-s1 offset-m2">
        <div class="row d-block" style="margin-bottom: 0;padding-top: 0.2rem">
            <div class="col s8 padding-off">
                <input id="newsletter-email" class="input-field sub-field" type="email" name="subscriber-email"
                       placeholder="Enter email for 10% off your first order">
            </div>
            <div class="col s4 padding-off">
                <button class="btn-sub waves-effect waves-light z-depth-0 modal-trigger"  data-target="modal1" name="signup"
                        value="Sign Up">Sign Up<i class="material-icons right hide-on-small-only">send</i>
                </button>
            </div>
        </div>
    </div>
    </a>
</div>

<!-- Modal Structure -->
<div id="modal1" class="modal">
    <div class="modal-header">
        <img src="https://1039206484.rsc.cdn77.org/assets/images/jb-logo-@2x.png" class="img-fluid mx-auto" alt="Joe Browns logo">
    </div>
    <div class="mmodal-content">
        <h4>Subscribe now for 10% off your first order</h4>
        @include("errors")
        <form id="newsletter-signup" class="col s12 offset-s1 offset-m2" method="POST" action="{{route('subscribers.store')}}">
            @csrf
            <div id="doi" class="row center-align">
                <div class="col s12">
                    <label for="newsletter-doi-check">Yes, I want to subscribe!
                        <input id="newsletter-doi-check" name="newsletter-doi-check" type="checkbox" class="filled-in" required><span style="color: rgb(164, 50, 38)">*</span>
                    </label>
                </div>
            </div>
            <div class="row d-block">
                <div class="col s8 offset-s1 padding-off">
                    <input id="newsletter-email" onchange="this.setCustomValidity(validity.valueMissing ? 'Enter a valid email address': '');" class="input-field sub-field validate" required type="email" name="subscriber-email"
                           placeholder="Enter email for 10% off your first order">
                </div>
                <div class="col s3 padding-off">
                    <button class="btn-sub waves-effect waves-light z-depth-0 flow-text" type="submit" name="signup"
                            value="Sign Up">Sign Up<i class="material-icons right hide-on-small-only">send</i>
                    </button>
                </div>
            </div>
            <div class="row d-block">
                <div class="col s12 offset-s1 padding-off">
                    <span class="helper-text" data-error="Please enter a valid email" data-success=""/>
                </div>
            </div>

            <div id="tacs" class="row d-block">
                <span class="col s10 offset-s1 padding-off">By clicking 'Sign Up' you will be subscribed to our mailing list and accept our
                <a href="https://www.joebrowns.co.uk/pu+terms-and-conditions+terms+1" target="_blank">terms and conditions</a> and
                <a href="https://www.joebrowns.co.uk/pu+privacy+privacy+1" target="_blank">privacy policy.</a>
                </span>
            </div>
        </form>
    </div>
    <div class="mmodal-footer">
        <a href="#!" class="modal-close waves-effect waves-green btn-flat"><i class="material-icons">close</i></a>
    </div>
</div>

@extends("layout");
@section("title","Our Privacy Policy Promise")
@section("header","Our Privacy Policy Promise")
@section("content")
    <h3>Our Privacy Promise</h3>
    <ul>
        <li>We collect, use and store your personal data so that we can offer you the best products, services and website experience possible.</li>
        <li>Your personal data is exactly what it says, it is yours and as such we are respectful of how we use it and how long we keep it.</li>
        <li>We will always be transparent with you about what we do with your personal data, and we will always ensure that it is secure.</li>
        <li>When we share your data with our service providers, we will always make sure they match our high levels of security.</li>
        <li>From time to time you will receive marketing communications from us which keep you up to date with all our latest products, services and offers. We believe this is a legitimate part of the Joe Browns customer experience.</li>
        <li>We will always offer you a clear and simple means of changing your communication preferences whenever you want to.</li>
    </ul>
@endsection
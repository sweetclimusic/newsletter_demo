@extends("layout");
@section("title","Email Newsletter Policy")
@section("header","Email Newsletter Policy")
@section("content")
    <ul>
        <li>Win £100 Email Newsletter Competition</li>
        <li>One winner will be selected from new email newsletter signups each calendar month.</li>
        <li>The prize awarded will be £100 in Joe Browns vouchers to be used at joebrowns.co.uk or via telephone (0113 270 6655).</li>
        <li>Prize cannot be exchanged for monetary equivalent.</li>
        <li>Winners will be contacted within two working days of draws being made.</li>
        <li>This competition is not open to employees of Joe Browns or family members of employees.</li>
        <li>Our decision is final.</li>
    </ul>
@endsection
